# PySide based visualization toolkit, warpx_analysis.py #


![picture](sample_image.png)

The toolkit provides various easy-to-use functions for data analysis of Warp/WarpX simulations.
Figure shows a snapshot of the existing prototype GUI widget toolkit, which includes several widget tools 
in the left side and multi-plot panels in the right side.
This GUI is still under development to include several functionalities.


(1) You need python 2.7:
We recommend to install python from Anaconda
http://docs.continuum.io/anaconda/install.
If you dont have such python libraries,

(1) h5py
```
conda install h5py
```
(2) matplotlib
```
conda install matplotlib
```
(3) numpy
```
pip install numpy
```
(4) yt
```
pip install yt==3.4 --user
```
For detail infomation of yt, http://yt-project.org

(5) Numba
```
conda install numba
```

To open Warp simulation data, the GUI uses openpmd viewer.

(6) PySide
```
conda install -c conda-forge pyside
``` 

If you have such an error:
"import PySide.QtCore.... Reason: image not found",
try the following:
```
conda uninstall qt
```
```
conda uninstall pyside
```
```
conda install -c conda-forge pyside
```


****************************************************
For cluster users such as NERSC (Edison, Cori), ALCF (Cooley)
****************************************************

(1) download miniconda2
```
#!shell
wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
```
(2) install miniconda2
```
#!shell
bash Miniconda2-latest-Linux-x86_64.sh
```
(3) activate miniconda2
```
#!shell
source miniconda2/bin/activate
```
(4) finally, install "pyside" using conda,
```
#!shell
conda install -c conda-forge pyside
```
************************************************
If you are using ALCF clusters, you can install PySide on Cooley,
and you may need to comment out the path of miniconda2 from .bashrc file,
************************************************
```
#!shell
#export PATH=�/home/jaehong/miniconda2/bin:$PATH�
```
due to version collision between python 2.6 and 2.7.

If you have installed all the libraries, git clone the GUI source files.
```
#!shell
git clone https://jaehong2013@bitbucket.org/jaehong2013/warpx_pyside.git
```

(7) VisIt
https://wci.llnl.gov/simulation/computer-codes/visit/
Download one VisiT excutable file for your system (Window/Mac)
Or module load visit on clusters,
```
module load visit
```
Set enviornment variables:
For OsX Mac users. Change username and VisiT version 2.xxx correctly.
```
export PATH="/Users/username/anaconda2/bin:$PATH"
export PATH="/Applications/VisIt.app/Contents/Resources/bin:$PATH"
export DYLD_LIBRARY_PATH=/Users/username/anaconda2/lib/python2.7/site-packages/PySide:/Applications/VisIt.app/Contents/Resources/2.XXX/darwin-x86_64/lib
export PYTHONPATH=$PYTHONPATH:/Applications/VisIt.app/Contents/Resources/2.XXX/darwin-x86_64/lib/site-packages
alias xml2cmake="/Applications/VisIt.app/Contents/Resources/bin/xml2cmake"
alias visit="/Applications/VisIt.app/Contents/Resources/bin/visit"
```

Go to the folder where your simulation data files are located, and then

```
#!shell
 python ~/warpx_pyside/warpx_analysis.py
```

The python package may be located at $HOME/warpx_pyside/ depending on your download folder.

