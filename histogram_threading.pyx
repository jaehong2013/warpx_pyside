# Implement Cloud-In-Cell deposition (i.e. linear weights) for particle histogram
# original source: Remi Rehe
# https://github.com/openPMD/openPMD-viewer/pull/185
cimport cython
import numpy as np
from libc.math cimport floor
from cython.parallel cimport prange
	
	
@cython.boundscheck(False)
@cython.wraparound(False)
def particle_energy(
	float[:] q1, float[:] q2, float[:] q3):
    
	cdef long n_ptcl = len(q1), i
	cdef float ene
	
	cdef float[:] energy = np.zeros((n_ptcl), dtype=np.float32)
	with nogil:
		for i in prange(n_ptcl, schedule='static'):
			
			ene = 1.+q1[i]**2+q2[i]**2+q3[i]**2
			ene = ene**.5
			ene = ene-1.
			energy[i]=ene
			
	return energy
	
@cython.boundscheck(False)
@cython.wraparound(False)
def histogram_cic_2d(
	float[:] q1, float[:] q2, float[:] w,
	int nbins_1, double bins_start_1, double bins_end_1,
	int nbins_2, double bins_start_2, double bins_end_2 ):

	# Define various scalars
	cdef double bin_spacing_1 = (bins_end_1-bins_start_1)/nbins_1
	cdef double inv_spacing_1 = 1./bin_spacing_1
	cdef double bin_spacing_2 = (bins_end_2-bins_start_2)/nbins_2
	cdef double inv_spacing_2 = 1./bin_spacing_2
	cdef long n_ptcl = len(w), i
	cdef int i1_low_bin, i2_low_bin
	cdef double q1_cell, q2_cell
	cdef double S1_low, S2_low
	
	cdef float[:,:] hist_data = np.zeros((nbins_1, nbins_2), dtype=np.float32)
	
	#hist_data = np.zeros( (nbins_1, nbins_2), dtype=np.float64 )
	with nogil:
		for i in prange(n_ptcl, schedule='static'):
			# Calculate the index of lower bin to which this particle contributes
			q1_cell = (q1[i] - bins_start_1) * inv_spacing_1
			q2_cell = (q2[i] - bins_start_2) * inv_spacing_2
			i1_low_bin = int(floor( q1_cell ))
			i2_low_bin = int(floor( q2_cell ))
		
			# Calculate corresponding CIC shape and deposit the weight
			S1_low = 1. - (q1_cell - i1_low_bin)
			S2_low = 1. - (q2_cell - i2_low_bin)
			if (i1_low_bin >= 0) and (i1_low_bin < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					hist_data[ i1_low_bin, i2_low_bin ] += w[i]*S1_low*S2_low
				if (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					hist_data[ i1_low_bin, i2_low_bin+1 ] += w[i]*S1_low*(1.-S2_low)
			if (i1_low_bin+1 >= 0) and (i1_low_bin+1 < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					hist_data[ i1_low_bin+1, i2_low_bin ] += w[i]*(1.-S1_low)*S2_low
				if (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					hist_data[ i1_low_bin+1, i2_low_bin+1 ] += w[i]*(1.-S1_low)*(1.-S2_low)
	
	
	return hist_data
	

@cython.boundscheck(False)
@cython.wraparound(False)
def histogram_cic_3d(
	float[:] q1, float[:] q2, float[:] q3, float[:] w,
	int nbins_1, double bins_start_1, double bins_end_1,
	int nbins_2, double bins_start_2, double bins_end_2,
	int nbins_3, double bins_start_3, double bins_end_3 ):

	# Define various scalars
	cdef double bin_spacing_1 = (bins_end_1-bins_start_1)/nbins_1
	cdef double inv_spacing_1 = 1./bin_spacing_1
	cdef double bin_spacing_2 = (bins_end_2-bins_start_2)/nbins_2
	cdef double inv_spacing_2 = 1./bin_spacing_2
	cdef double bin_spacing_3 = (bins_end_3-bins_start_3)/nbins_3
	cdef double inv_spacing_3 = 1./bin_spacing_3
	cdef long n_ptcl = len(w), i
	cdef int i1_low_bin, i2_low_bin, i3_low_bin
	cdef double q1_cell, q2_cell, q3_cell
	cdef double S1_low, S2_low, S3_low
	
	cdef float[:,:,:] hist_data = np.zeros((nbins_1, nbins_2, nbins_3), dtype=np.float32)
	
	#hist_data = np.zeros( (nbins_1, nbins_2), dtype=np.float64 )
	with nogil:
		for i in prange(n_ptcl, schedule='static'):
			# Calculate the index of lower bin to which this particle contributes
			q1_cell = (q1[i] - bins_start_1) * inv_spacing_1
			q2_cell = (q2[i] - bins_start_2) * inv_spacing_2
			q3_cell = (q3[i] - bins_start_3) * inv_spacing_3
			i1_low_bin = int(floor( q1_cell ))
			i2_low_bin = int(floor( q2_cell ))
			i3_low_bin = int(floor( q3_cell ))
		
			# Calculate corresponding CIC shape and deposit the weight
			S1_low = 1. - (q1_cell - i1_low_bin)
			S2_low = 1. - (q2_cell - i2_low_bin)
			S3_low = 1. - (q3_cell - i3_low_bin)
			if (i1_low_bin >= 0) and (i1_low_bin < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*S2_low*S3_low
					elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*S2_low*(1.-S3_low)
				elif (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*(1.-S2_low)*S3_low
					elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*(1.-S2_low)*(1.-S3_low)
			elif (i1_low_bin+1 >= 0) and (i1_low_bin+1 < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*S2_low*S3_low
					elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*S2_low*(1.-S3_low)
				elif (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*(1.-S2_low)*S3_low
					elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*(1.-S2_low)*(1.-S3_low)
	
	
	return hist_data
	
@cython.boundscheck(False)
@cython.wraparound(False)
def energy_deposit_2d(
	float[:] q1, float[:] q2,
	float[:] p1, float[:] p2, float[:] p3, float[:] w,
	int nbins_1, double bins_start_1, double bins_end_1,
	int nbins_2, double bins_start_2, double bins_end_2):

	# Define various scalars
	cdef double bin_spacing_1 = (bins_end_1-bins_start_1)/nbins_1
	cdef double inv_spacing_1 = 1./bin_spacing_1
	cdef double bin_spacing_2 = (bins_end_2-bins_start_2)/nbins_2
	cdef double inv_spacing_2 = 1./bin_spacing_2
	cdef long n_ptcl = len(w), i
	cdef int i1_low_bin, i2_low_bin,
	cdef double q1_cell, q2_cell
	cdef double S1_low, S2_low
	cdef float ene

	cdef float[:,:] hist_data = np.zeros((nbins_1, nbins_2), dtype=np.float32)

	#hist_data = np.zeros( (nbins_1, nbins_2), dtype=np.float64 )
	with nogil:
		for i in prange(n_ptcl, schedule='static'):
			# Calculate the index of lower bin to which this particle contributes
			q1_cell = (q1[i] - bins_start_1) * inv_spacing_1
			q2_cell = (q2[i] - bins_start_2) * inv_spacing_2
			i1_low_bin = int(floor( q1_cell ))
			i2_low_bin = int(floor( q2_cell ))
	
			# Calculate corresponding CIC shape and deposit the weight
			S1_low = 1. - (q1_cell - i1_low_bin)
			S2_low = 1. - (q2_cell - i2_low_bin)
		
			ene = 1.+p1[i]**2+p2[i]**2+p3[i]**2
			ene = ene**.5
			ene = ene-1.
			
			if (i1_low_bin >= 0) and (i1_low_bin < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					hist_data[ i1_low_bin, i2_low_bin ] += w[i]*S1_low*S2_low*ene
				if (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					hist_data[ i1_low_bin, i2_low_bin+1 ] += w[i]*S1_low*(1.-S2_low)*ene
			if (i1_low_bin+1 >= 0) and (i1_low_bin+1 < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					hist_data[ i1_low_bin+1, i2_low_bin ] += w[i]*(1.-S1_low)*S2_low*ene
				if (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					hist_data[ i1_low_bin+1, i2_low_bin+1 ] += w[i]*(1.-S1_low)*(1.-S2_low)*ene
		
	return hist_data	

@cython.boundscheck(False)
@cython.wraparound(False) 	
def histogram_cic_3d_serial(
	float[:] q1, float[:] q2, float[:] q3, float[:] w,
	int nbins_1, double bins_start_1, double bins_end_1,
	int nbins_2, double bins_start_2, double bins_end_2,
	int nbins_3, double bins_start_3, double bins_end_3 ):

	# Define various scalars
	cdef double bin_spacing_1 = (bins_end_1-bins_start_1)/nbins_1
	cdef double inv_spacing_1 = 1./bin_spacing_1
	cdef double bin_spacing_2 = (bins_end_2-bins_start_2)/nbins_2
	cdef double inv_spacing_2 = 1./bin_spacing_2
	cdef double bin_spacing_3 = (bins_end_3-bins_start_3)/nbins_3
	cdef double inv_spacing_3 = 1./bin_spacing_3
	cdef long n_ptcl = len(w), i
	cdef int i1_low_bin, i2_low_bin, i3_low_bin
	cdef double q1_cell, q2_cell, q3_cell
	cdef double S1_low, S2_low, S3_low

	cdef float[:,:,:] hist_data = np.zeros((nbins_1, nbins_2, nbins_3), dtype=np.float32)

	#hist_data = np.zeros( (nbins_1, nbins_2), dtype=np.float64 )
	for i in range(n_ptcl):
		# Calculate the index of lower bin to which this particle contributes
		q1_cell = (q1[i] - bins_start_1) * inv_spacing_1
		q2_cell = (q2[i] - bins_start_2) * inv_spacing_2
		q3_cell = (q3[i] - bins_start_3) * inv_spacing_3
		i1_low_bin = int(floor( q1_cell ))
		i2_low_bin = int(floor( q2_cell ))
		i3_low_bin = int(floor( q3_cell ))

		# Calculate corresponding CIC shape and deposit the weight
		S1_low = 1. - (q1_cell - i1_low_bin)
		S2_low = 1. - (q2_cell - i2_low_bin)
		S3_low = 1. - (q3_cell - i3_low_bin)
		if (i1_low_bin >= 0) and (i1_low_bin < nbins_1):
			if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
				if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
					hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*S2_low*S3_low
				elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
					hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*S2_low*(1.-S3_low)
			elif (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
				if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
					hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*(1.-S2_low)*S3_low
				elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
					hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*(1.-S2_low)*(1.-S3_low)
		elif (i1_low_bin+1 >= 0) and (i1_low_bin+1 < nbins_1):
			if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
				if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
					hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*S2_low*S3_low
				elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
					hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*S2_low*(1.-S3_low)
			elif (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
				if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
					hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*(1.-S2_low)*S3_low
				elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
					hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*(1.-S2_low)*(1.-S3_low)


	return hist_data
	
@cython.boundscheck(False)
@cython.wraparound(False)
def energy_deposit_2d(
	float[:] q1, float[:] q2,
	float[:] p1, float[:] p2, float[:] p3, float[:] w,
	int nbins_1, double bins_start_1, double bins_end_1,
	int nbins_2, double bins_start_2, double bins_end_2):

	# Define various scalars
	cdef double bin_spacing_1 = (bins_end_1-bins_start_1)/nbins_1
	cdef double inv_spacing_1 = 1./bin_spacing_1
	cdef double bin_spacing_2 = (bins_end_2-bins_start_2)/nbins_2
	cdef double inv_spacing_2 = 1./bin_spacing_2
	cdef long n_ptcl = len(w), i
	cdef int i1_low_bin, i2_low_bin,
	cdef double q1_cell, q2_cell
	cdef double S1_low, S2_low
	cdef float ene

	cdef float[:,:] hist_data = np.zeros((nbins_1, nbins_2), dtype=np.float32)

	#hist_data = np.zeros( (nbins_1, nbins_2), dtype=np.float64 )
	with nogil:
		for i in prange(n_ptcl, schedule='static'):
			# Calculate the index of lower bin to which this particle contributes
			q1_cell = (q1[i] - bins_start_1) * inv_spacing_1
			q2_cell = (q2[i] - bins_start_2) * inv_spacing_2
			i1_low_bin = int(floor( q1_cell ))
			i2_low_bin = int(floor( q2_cell ))

			# Calculate corresponding CIC shape and deposit the weight
			S1_low = 1. - (q1_cell - i1_low_bin)
			S2_low = 1. - (q2_cell - i2_low_bin)
	
			ene = 1.+p1[i]**2+p2[i]**2+p3[i]**2
			ene = ene**.5
			ene = ene-1.
		
			if (i1_low_bin >= 0) and (i1_low_bin < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					hist_data[ i1_low_bin, i2_low_bin ] += w[i]*S1_low*S2_low*ene
				if (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					hist_data[ i1_low_bin, i2_low_bin+1 ] += w[i]*S1_low*(1.-S2_low)*ene
			if (i1_low_bin+1 >= 0) and (i1_low_bin+1 < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					hist_data[ i1_low_bin+1, i2_low_bin ] += w[i]*(1.-S1_low)*S2_low*ene
				if (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					hist_data[ i1_low_bin+1, i2_low_bin+1 ] += w[i]*(1.-S1_low)*(1.-S2_low)*ene
	
	return hist_data	
	
	
@cython.boundscheck(False)
@cython.wraparound(False)
def energy_deposit_3d(
	float[:] q1, float[:] q2, float[:] q3, 
	float[:] p1, float[:] p2, float[:] p3, float[:] w,
	int nbins_1, double bins_start_1, double bins_end_1,
	int nbins_2, double bins_start_2, double bins_end_2,
	int nbins_3, double bins_start_3, double bins_end_3 ):

	# Define various scalars
	cdef double bin_spacing_1 = (bins_end_1-bins_start_1)/nbins_1
	cdef double inv_spacing_1 = 1./bin_spacing_1
	cdef double bin_spacing_2 = (bins_end_2-bins_start_2)/nbins_2
	cdef double inv_spacing_2 = 1./bin_spacing_2
	cdef double bin_spacing_3 = (bins_end_3-bins_start_3)/nbins_3
	cdef double inv_spacing_3 = 1./bin_spacing_3
	cdef long n_ptcl = len(w), i
	cdef int i1_low_bin, i2_low_bin, i3_low_bin
	cdef double q1_cell, q2_cell, q3_cell
	cdef double S1_low, S2_low, S3_low
	cdef float ene
	
	cdef float[:,:,:] hist_data = np.zeros((nbins_1, nbins_2, nbins_3), dtype=np.float32)
	
	#hist_data = np.zeros( (nbins_1, nbins_2), dtype=np.float64 )
	with nogil:
		for i in prange(n_ptcl, schedule='static'):
			# Calculate the index of lower bin to which this particle contributes
			q1_cell = (q1[i] - bins_start_1) * inv_spacing_1
			q2_cell = (q2[i] - bins_start_2) * inv_spacing_2
			q3_cell = (q3[i] - bins_start_3) * inv_spacing_3
			i1_low_bin = int(floor( q1_cell ))
			i2_low_bin = int(floor( q2_cell ))
			i3_low_bin = int(floor( q3_cell ))
		
			# Calculate corresponding CIC shape and deposit the weight
			S1_low = 1. - (q1_cell - i1_low_bin)
			S2_low = 1. - (q2_cell - i2_low_bin)
			S3_low = 1. - (q3_cell - i3_low_bin)
			
			ene = 1.+p1[i]**2+p2[i]**2+p3[i]**2
			ene = ene**.5
			ene = ene-1.
			
			if (i1_low_bin >= 0) and (i1_low_bin < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*S2_low*S3_low*ene
					elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*S2_low*(1.-S3_low)*ene
				elif (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*(1.-S2_low)*S3_low*ene
					elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*S1_low*(1.-S2_low)*(1.-S3_low)*ene
			elif (i1_low_bin+1 >= 0) and (i1_low_bin+1 < nbins_1):
				if (i2_low_bin >= 0) and (i2_low_bin < nbins_2):
					if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*S2_low*S3_low*ene
					elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*S2_low*(1.-S3_low)*ene
				elif (i2_low_bin+1 >= 0) and (i2_low_bin+1 < nbins_2):
					if (i3_low_bin >= 0) and (i3_low_bin < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*(1.-S2_low)*S3_low*ene
					elif (i3_low_bin+1 >= 0) and (i3_low_bin+1 < nbins_3):
						hist_data[ i1_low_bin, i2_low_bin, i3_low_bin ] += w[i]*(1.-S1_low)*(1.-S2_low)*(1.-S3_low)*ene
	
	
	return hist_data

	
	
	
	
	
	
	
	
