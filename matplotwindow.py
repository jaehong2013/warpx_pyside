import numpy as np
import time as tm

import matplotlib
matplotlib.rcParams['backend.qt4']='PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Import numba functions
from cic_histogram import histogram_cic_1d, particle_energy
try:
    #Use cython openmp multi-thread
    from histogram_threading import histogram_cic_2d
    print("use cython parallel")
except ImportError:
    from cic_histogram import histogram_cic_2d

debug = False

class MatplotlibWidget(Canvas):

    def __init__(self, parent=None):		
        super(MatplotlibWidget, self).__init__(Figure())
        self.setParent(parent)
        self.figure = Figure(dpi=60)
        self.canvas = Canvas(self.figure)
        self.figure.set_facecolor('0.85')
  
    def field(self, warpx, dim, coord_system, mass_dic, amass_dic, time, taxis, 
                tstep, fdata, field, oned, aspect, localphase, count,
                x1min0, x1max0, x2min0, x2max0, x1axis, x2axis, amrlevel,
                x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                xL_loc, xR_loc, yL_loc, yR_loc, lineselect, rectselect, contrast,
                p1min, p1max, p2min, p2max, sliceplane, animation, saveimg):
                
        self.figure.clear()
        
        if saveimg ==1 or saveimg == 2 or saveimg ==3 :
            matplotlib.rc('xtick', labelsize=18) 
            matplotlib.rc('ytick', labelsize=18)
        else:
            matplotlib.rc('xtick', labelsize=16) 
            matplotlib.rc('ytick', labelsize=16)
        
        #sliceplane = kwargs['sliceplane']    
        
        # zoom-in/out
        x1min = x1min0+(x1max0-x1min0)*x1min_zoom/100  
        x1max = x1min0+(x1max0-x1min0)*x1max_zoom/100  
        x2min = x2min0+(x2max0-x2min0)*x2min_zoom/100  
        x2max = x2min0+(x2max0-x2min0)*x2max_zoom/100
        
        nx = len(x1axis); ny = len(x2axis)
                
        # zoom-in/out indices
        iL = np.where(x1axis >= x1min)[0]
        if len(iL) != 0: iL=iL[0]
        else: iL = 0
        iR = np.where(x1axis >= x1max)[0]
        if len(iR) != 0: iR=iR[0]
        else: iR = nx-1
        jL = np.where(x2axis >= x2min)[0]
        if len(jL) != 0: jL=jL[0]
        else: jL = 0
        jR = np.where(x2axis >= x2max)[0]
        if len(jR) != 0: jR=jR[0]
        else: jR = ny-1

        # multply the AMR level factor
        iL = iL*2**amrlevel; iR = iR*2**amrlevel
        jL = jL*2**amrlevel; jR = jR*2**amrlevel

        # local zone coordinaes (xi,yi) and (xf, yf)
        xi = x1min+(x1max-x1min)*xL_loc
        xf = x1min+(x1max-x1min)*xR_loc
        yi = x2min+(x2max-x2min)*yL_loc
        yf = x2min+(x2max-x2min)*yR_loc
        
        #if amrlevel !=0 and amrgrid:
        #    xleftedge0 = amredges[0]
        #    xrightedge0 = amredges[1]
        #    yleftedge0 = amredges[2]
        #    yrightedge0 = amredges[3]
            
        
        # Main plot
        if count == 0 or not(lineselect or rectselect):
            self.plot = self.figure.add_subplot(111)
        else:
            self.plot = self.figure.add_subplot(121)
            
        nbin = 200
        fontsize0 = 15; fontsize1 = 15
        if saveimg ==1 or saveimg == 2 or saveimg == 3:
            fontsize0 = 17; fontsize1 = 17
        cbarwidth = 0.16


        
        if sliceplane == 'xy':
            xtitle = 'x ($\mu$m)'; ytitle = 'y ($\mu$m)'
            kxtitle = 'kx (1/$\mu$m)'; kytitle = 'ky (1/$\mu$m)'
        if sliceplane == 'xz':
            xtitle = 'z ($\mu$m)'; ytitle = 'x ($\mu$m)'
            kxtitle = 'kz (1/$\mu$m)'; kytitle = 'kx (1/$\mu$m)'
        if sliceplane == 'yz':
            xtitle = 'z ($\mu$m)'; ytitle = 'y ($\mu$m)'
            kxtitle = 'kz (1/$\mu$m)'; kytitle = 'ky (1/$\mu$m)'
        

        #if oned:        
        #    jm=int((jL+jR)/2.)
        #    self.plot.cla()
        #    self.plot.plot(x1axis, fdata[jm,:])
        #    self.plot.set_title(field, fontsize=fontsize0)
        #    self.plot.set_xlabel(xtitle, fontsize=fontsize1)
        #    self.plot.set_aspect('auto')
        #    self.plot.axes.set_xlim([x1min,x1max])
        #    self.draw()
            
        #else:
        
        vmin = np.min(fdata[jL:jR,iL:iR])
        vmax = np.max(fdata[jL:jR,iL:iR])
        self.plot.cla()
        #logthresh = 1
        im =  self.plot.imshow(fdata[jL:jR,iL:iR], interpolation='nearest', cmap='jet',
                origin='lower', extent=[ x1min,x1max,x2min,x2max ], aspect=aspect, 
                vmin=1.*contrast/100*vmin, vmax=1.*contrast/100*vmax)
                #norm=matplotlib.colors.SymLogNorm(10**-logthresh) )
        self.plot.axes.set_xlim([x1min,x1max])
        self.plot.axes.set_ylim([x2min,x2max])
        ax = self.figure.gca()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size=cbarwidth, pad=0.)
        cb = self.figure.colorbar(im, cax=cax)
        if saveimg == 0:
            self.plot.set_title(field, fontsize=fontsize0)
        else:
            self.plot.set_title(field+'   t=%6.1f fs' % time, fontsize=fontsize0)
        self.plot.set_xlabel(xtitle, fontsize=fontsize1)
        self.plot.set_ylabel(ytitle, fontsize=fontsize1)
        #if amrlevel !=0 and amrgrid:
        #    self.plot.plot([xleftedge0,xleftedge0],[yleftedge0,yrightedge0],':', color='black')
        #    self.plot.plot([xleftedge0,xrightedge0],[yleftedge0,yleftedge0],':', color='black')
        #    self.plot.plot([xleftedge0,xrightedge0],[yrightedge0,yrightedge0],':', color='black')
        #    self.plot.plot([xrightedge0,xrightedge0],[yleftedge0,yrightedge0],':', color='black')
        if count == 1:
            self.plot.plot([xi, xi],[yi, yi], 'o', color='blue')
        if count == 2:
            if localphase == 'intensity' or localphase == 'energy':        
                if lineselect:
                    self.plot.plot([xi, xi], [x1min0,x2max0], ':', color='black')
                    #self.plot.plot([xf, xf], [xmin0,xmax0], ':', color='black')
                elif rectselect:
                    self.plot.plot([xi, xi], [yi, yi], '.', color='blue')
                    self.plot.plot([xi, xf], [yi, yi], ':', color='black')
                    self.plot.plot([xi, xf], [yf, yf], ':', color='black')
                    self.plot.plot([xi, xi], [yi, yf], ':', color='black')
                    self.plot.plot([xf, xf], [yi, yf], ':', color='black')
                    self.plot.plot([xf, xf], [yf, yf], '.', color='red')
            else:
                if lineselect:
                    self.plot.plot([xi, xi], [yi, yi], '.', color='blue')
                    self.plot.plot([xi, xf], [yi, yf], ':', color='black')
                    self.plot.plot([xf, xf], [yf, yf], '.', color='red')
                elif rectselect:
                    self.plot.plot([xi, xi], [yi, yi], '.', color='blue')
                    self.plot.plot([xi, xf], [yi, yi], ':', color='black')
                    self.plot.plot([xi, xf], [yf, yf], ':', color='black')
                    self.plot.plot([xi, xi], [yi, yf], ':', color='black')
                    self.plot.plot([xf, xf], [yi, yf], ':', color='black')
                    self.plot.plot([xf, xf], [yf, yf], '.', color='red')
        self.plot.axes.get_figure().tight_layout()
        self.draw()
            
        #############################################
        # Local field plot
        #############################################
        
        if count == 2 and (lineselect or rectselect):
        
            if lineselect:
                iLloc = np.where(x1axis >= xi)[0];iLloc = iLloc[0]
                iRloc = np.where(x1axis >= xf)[0];iRloc = iRloc[0]
                jLloc = np.where(x2axis >= yi)[0];jLloc = jLloc[0]
                jRloc = np.where(x2axis >= yf)[0];jRloc = jRloc[0]
                
                iLloc = iLloc*2**amrlevel; iRloc = iRloc*2**amrlevel
                jLloc = jLloc*2**amrlevel; jRloc = jRloc*2**amrlevel
            elif rectselect:
                xi1 = np.min([xi,xf])
                xf1 = np.max([xi,xf])
                yi1 = np.min([yi,yf])
                yf1=  np.max([yi,yf])
                # local zoomn coorinates indcies
                iLloc = np.where(x1axis >= xi1)[0];iLloc = iLloc[0]
                iRloc = np.where(x1axis >= xf1)[0];iRloc = iRloc[0]
                jLloc = np.where(x2axis >= yi1)[0];jLloc = jLloc[0]
                jRloc = np.where(x2axis >= yf1)[0];jRloc = jRloc[0]
            
                iLloc = iLloc*2**amrlevel; iRloc = iRloc*2**amrlevel
                jLloc = jLloc*2**amrlevel; jRloc = jRloc*2**amrlevel
        
            
            self.plot = self.figure.add_subplot(122)
            self.plot.cla()
            
                    
            if localphase == 'density' or localphase == 'FFT':
                
                if lineselect:
                    laxis = []
                    ldata = []
                    if np.abs(jRloc-jLloc) < np.abs(iRloc-iLloc):
                        m = 1.*(jRloc-jLloc)/(iRloc-iLloc)
                        if iLloc > iRloc:
                            step = -1
                        else:
                            step = 1
                        for i in np.arange(iLloc, iRloc, step):
                            j = m*(i-iLloc) + jLloc
                            j = int(j)
                            ldata.append(fdata[j,i])
                            length = (x1axis[i]-x1axis[iLloc])**2+(x2axis[j]-x2axis[jLloc])**2
                            length = length**.5
                            laxis.append(length)
                    else:
                        m = 1.*(iRloc-iLloc)/(jRloc-jLloc)
                        if jLloc > jRloc:
                            step = -1
                        else:
                            step = 1
                        for j in np.arange(jLloc, jRloc, step):
                            i = m*(j-jLloc) + iLloc
                            i = int(i)
                            ldata.append(fdata[j,i])
                            length = (x1axis[i]-x1axis[iLloc])**2+(x2axis[j]-x2axis[jLloc])**2
                            length = length**.5
                            laxis.append(length)
                    
                    nfl = len(laxis)-1
                    
                    if localphase == 'density': 
                        self.plot.cla() 
                        self.plot.plot(laxis, ldata)
                        self.plot.plot([laxis[0], laxis[0]], [ldata[0], ldata[0]], '.', color='blue')
                        self.plot.plot([laxis[nfl], laxis[nfl]], [ldata[nfl], ldata[nfl]], '.', color='red')
                        if saveimg == 0:
                            self.plot.set_title(field, fontsize=fontsize0)
                        else:
                            self.plot.set_title(field+'   t=%6.1f fs' % time, fontsize=fontsize0)
                        self.plot.set_xlabel('$l$ ($\mu$m)', fontsize=fontsize1)
                        self.plot.set_aspect('auto')
                        self.plot.axes.get_figure().tight_layout()
                        self.draw()
                
                    if localphase == 'FFT':
                        nl = len(laxis)
                        lmin1 = laxis[0]
                        lmax1 = laxis[nl-1]
                        lmin = lmin1 - (lmin1+lmax1)*.5
                        lmax = lmax1 - (lmin1+lmax1)*.5 
                        fft = np.fft.fft(ldata)
                        fft = np.abs(np.fft.fftshift(fft))
                        kmin = np.pi/lmin
                        kmax = np.pi/lmax            
                    
                        kmin = (kmax-kmin)*p1min/99+kmin
                        kmax = (kmax-kmin)*p1max/99+kmin  
                        kaxis = np.arange(nl)*(kmax-kmin)/nl+kmin
                    
                        self.plot.cla()
                        self.plot.plot(kaxis, fft)
                        if saveimg == 0:
                            self.plot.set_title(field+' FFT', fontsize=fontsize0)
                        else:
                            self.plot.set_title(field+' FFT'+'   t=%6.1f fs' % time, fontsize=fontsize0)
                        self.plot.set_xlabel('k (1/$\mu$m)', fontsize=fontsize1)
                        self.plot.set_xlim([kmin,kmax])
                        self.plot.set_aspect('auto')
                        self.plot.axes.get_figure().tight_layout()
                        self.draw()
                    
                elif rectselect: 
            
                    if localphase == 'density':
                        self.plot.cla()
                        im = self.plot.imshow(fdata[jLloc:jRloc,iLloc:iRloc], interpolation='nearest', cmap='jet',
                            origin='lower', extent=[ xi1,xf1,yi1,yf1 ], aspect=aspect)
                        ax = self.figure.gca()
                        divider = make_axes_locatable(ax)
                        cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                        cb = self.figure.colorbar(im, cax=cax)
                        if saveimg == 0:
                            self.plot.set_title(field, fontsize=fontsize0)
                        else:
                            self.plot.set_title(field+'   t=%6.1f fs' % time, fontsize=fontsize0)
                        self.plot.set_xlabel(xtitle, fontsize=fontsize1)
                        self.plot.set_ylabel(ytitle, fontsize=fontsize1)
                        self.plot.axes.set_xlim([xi1,xf1])
                        self.plot.axes.set_ylim([yi1,yf1])
                        self.plot.axes.get_figure().tight_layout()
                        self.draw()
                
                    if localphase == 'FFT':
                
                        fft2d = np.fft.fft2(fdata[jLloc:jRloc,iLloc:iRloc])
                        fft2d = np.fft.fftshift(fft2d)
                        fft2d = np.abs(fft2d)
            
                        kxmin = np.pi/(xi1-(xi1+xf1)*.5)
                        kxmax = np.pi/(xf1-(xi1+xf1)*.5)
                        kymin = np.pi/(yi1-(yi1+yf1)*.5)
                        kymax = np.pi/(yf1-(yi1+yf1)*.5)
                        nx, ny= fft2d.shape
                        kxaxis = np.arange(nx)*(kxmax-kxmin)/(nx-1)+kxmin
                        kyaxis = np.arange(ny)*(kymax-kymin)/(ny-1)+kymin
            
                        kxmin = (kxmax-kxmin)*p1min/99+kxmin
                        kxmax = (kxmax-kxmin)*p1max/99+kxmin
                        kymin = (kymax-kymin)*p2min/99+kymin
                        kymax = (kymax-kymin)*p2max/99+kymin
            
                    
                        self.plot.cla()
                        im = self.plot.imshow(fft2d, interpolation='spline16', cmap='jet',
                            origin='lower', extent=[ kxmin,kxmax,kymin,kymax ], aspect=aspect)
                        ax = self.figure.gca()
                        divider = make_axes_locatable(ax)
                        cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                        cb = self.figure.colorbar(im, cax=cax)
                        self.plot.set_title(field+' FFT', fontsize=fontsize0)
                        self.plot.axes.set_xlim([kxmin,kxmax])
                        self.plot.axes.set_ylim([kymin,kymax])
                        self.plot.set_xlabel(kxtitle, fontsize=fontsize1)
                        self.plot.set_ylabel(kytitle, fontsize=fontsize1)    
                        self.plot.axes.get_figure().tight_layout()
                        self.draw()
        
        
        if saveimg == 1 or saveimg == 2:
            if warpx == True:
                if   count == 0:
                    image_dir = '.'
                    filename = field
                else:
                    image_dir = '.'
                    filename =  field+'_'+localphase
            else:
                if count == 0:
                    image_dir = '..'
                    filename = field
                else:
                    image_dir = '..'
                    filename =  field+'_'+localphase
        
            if saveimg == 1:
                self.figure.savefig(image_dir+'/'+filename+'%3.3d.png' %tstep, format='png')  
                print(image_dir+'/'+filename+'%3.3d.png saved' %tstep)
            if saveimg == 2:
                self.figure.savefig(image_dir+'/'+filename+'%3.3d.eps' %tstep, format='eps', dpi=60)
                print(image_dir+'/'+filename+'%3.3d.png saved' %tstep)
                        
        if saveimg == 3:
                        
            if warpx == True:
                if count == 0:
                    image_dir = './movie/'
                    filename = field
                else:
                    image_dir = './movie/'
                    filename =  field+'_'+localphase
            else:
                if count == 0:
                    image_dir = '../movie/'
                    filename = field
                else:
                    image_dir = '../movie/'
                    filename =  field+'_'+localphase            
            
            self.figure.savefig(image_dir+'/'+filename+'%3.3d.png' %tstep, format='png')
            print(image_dir+'/'+filename+'%3.3d.png saved' %tstep)
  
    
    def particle(self, warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                tstep, xdata, ydata, zdata, uxdata, uydata, uzdata, wtdata,
                species, phase, aspect, localphase, count, 
                x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                xL_loc, xR_loc, yL_loc, yR_loc, lineselect, rectselect, contrast, 
                tempfit, temp, norm,
                p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson):

        self.figure.clear()

        if saveimg ==1 or saveimg == 2 or saveimg ==3:
            matplotlib.rc('xtick', labelsize=17) 
            matplotlib.rc('ytick', labelsize=17)
        else:
            matplotlib.rc('xtick', labelsize=16) 
            matplotlib.rc('ytick', labelsize=16)
        
        # zoom-in/out
        x1min = x1min0+(x1max0-x1min0)*x1min_zoom/100  
        x1max = x1min0+(x1max0-x1min0)*x1max_zoom/100  
        x2min = x2min0+(x2max0-x2min0)*x2min_zoom/100  
        x2max = x2min0+(x2max0-x2min0)*x2max_zoom/100
        
        # local zone coordinaes (xi,yi) and (xf, yf)
        xi = x1min+(x1max-x1min)*xL_loc
        xf = x1min+(x1max-x1min)*xR_loc
        yi = x2min+(x2max-x2min)*yL_loc
        yf = x2min+(x2max-x2min)*yR_loc
        
        
        # Main plot
        if count == 0 or not(lineselect or rectselect):
            self.plot = self.figure.add_subplot(111)
        else:
            self.plot = self.figure.add_subplot(121)
            
        fontsize0 = 15; fontsize1 = 15
        if saveimg ==1 or saveimg == 2 or saveimg == 3:
            fontsize0 = 17; fontsize1 = 17
        cbarwidth = 0.16
    
    
        if sliceplane == 'xy':
            #xdata = xdata
            #ydata = ydata
            xtitle = 'x ($\mu$m)';ytitle = 'y ($\mu$m)'
        if sliceplane == 'xz':
            ydata = xdata
            xdata = zdata
            xtitle = 'z ($\mu$m)';ytitle = 'x ($\mu$m)'
        if sliceplane == 'yz':
            xdata = zdata
            #ydata = ydata
            xtitle = 'z ($\mu$m)';ytitle = 'y ($\mu$m)'

        ######################################################################
        # particle selection in a zoomed zone takes more computation time 
        # in the statement: xdata = xdata[intersect], etc.
        # So i skip it.
        #######################################################################
        #if x1min_zoom !=0 or x1max_zoom != 100 or x2min_zoom != 0 or x2max_zoom != 100:
        #    start_time1=  tm.time()
        #    if (x1min_zoom !=0 or x1max_zoom != 100) and (x2min_zoom == 0 and x2max_zoom == 100):
        #        intersect, = np.where((xdata > x1min) & (xdata < x1max))
        #    elif (x1min_zoom ==0 and x1max_zoom == 100) and (x2min_zoom != 0 or x2max_zoom != 100):
        #        intersect, = np.where((ydata > x2min) & (ydata < x2max))
        #    else:
        #        intersect, = np.where((xdata > x1min) & (xdata < x1max) & (ydata > x2min) & (ydata < x2max))
        #    print("---np.where  particle select %s seconds ---" % (tm.time() - start_time1))
        #    nselect = len(intersect)                
        #    start_time3 =  tm.time()
        #    xdata = xdata[intersect]
        #    ydata = ydata[intersect]
        #    uxdata = uxdata[intersect]
        #    uydata = uydata[intersect]
        #    uzdata = uzdata[intersect]
        #    wtdata = wtdata[intersect]        
  
        nbin = 400
        
        if debug: start_time4 = tm.time()
                    
        if phase == 'density':

            self.plot.cla()
            #if coord_system == 'cylindrical':
            #    im = self.plot.hist2d(xdata, ydata, bins=nbin, norm=LogNorm()) #, weights = weight)
            #else:
            #    im = self.plot.hist2d(xdata, ydata, bins=nbin, norm=LogNorm(), weights=wtdata)    
            if debug: start_time5 = tm.time()
            
            if lowreson:
                histogram = histogram_cic_2d( xdata, ydata, wtdata, nx1, x1min, x1max, nx2, x2min, x2max)
            else:
                histogram = histogram_cic_2d( xdata, ydata, wtdata, nbin, x1min, x1max, nbin, x2min, x2max)
            if debug: print("---histogram_cic_2d time %s seconds ---" % (tm.time() - start_time5))
            #logthresh = -4
            
            vmax=np.max(histogram)
            vmin = vmax*1.e-4
            vmax *= contrast/100.
            vmin *= 100./contrast
            logthresh=-np.log10(vmin)
            
            im = self.plot.imshow( histogram.T, 
                            origin='lower', extent=[ x1min,x1max,x2min,x2max ], 
                            aspect=aspect, cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh))
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            #cb = self.figure.colorbar(im[3], cax=cax)
            self.plot.axes.set_xlim([x1min,x1max])
            self.plot.axes.set_ylim([x2min,x2max])
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel(xtitle, fontsize=fontsize1)
            self.plot.set_ylabel(ytitle, fontsize=fontsize1)
            self.plot.axes.get_figure().tight_layout()
            if count == 1:
                self.plot.plot([xi, xi],[yi, yi], 'o', color='blue')
            if count == 2:
                if lineselect:
                    self.plot.plot([xi, xi], [x2min,x2max], ':', color='black')
                    self.plot.plot([xf, xf], [x2min,x2max], ':', color='black')
                elif rectselect:
                    self.plot.plot([xi, xi], [yi, yi], '.', color='blue')
                    self.plot.plot([xi, xf], [yi, yi], ':', color='black')
                    self.plot.plot([xi, xf], [yf, yf], ':', color='black')
                    self.plot.plot([xi, xi], [yi, yf], ':', color='black')
                    self.plot.plot([xf, xf], [yi, yf], ':', color='black')
                    self.plot.plot([xf, xf], [yf, yf], '.', color='red')
            self.draw()
            
            
        if phase == 'fE-loglin':    
            nbin = 200
            if debug: start_time7 = tm.time()
            #ene = np.sqrt(1.+uxdata**2+uydata**2+uzdata**2)-1.	# energy
            ene = particle_energy(uxdata, uydata, uzdata)
            if debug: print("---ene calculation %s seconds ---" % (tm.time() - start_time7))
            mass = mass_dic[species]
            amass = amass_dic[species]
            if mass > 1:
                ene = ene * mass * 0.511/amass
            else:
                ene = ene * 0.511
            
            e1min = ene.min()
            e1max = ene.max()
            eaxis = e1min+np.arange(nbin)*(e1max-e1min)/nbin
        
            self.plot.cla()
            if debug: start_time8 = tm.time()
            histogram_1d=histogram_cic_1d( ene, wtdata, nbin, e1min , e1max )
            self.plot.semilogy(eaxis, histogram_1d)
            #self.plot.hist(ene, bins=nbin, log=True, histtype='step', normed=True, weights= wtdata)
            if debug: print("---plot.hist %s seconds ---" % (tm.time() - start_time8))
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            #if (window == 1 and numfracCheck1) or (window == 2 and numfracCheck2):
            #    self.plot.plot([bins[e1loc],bins[e1loc]],[vmin,vmax],':', color='red')
            #    self.plot.plot([bins[e2loc],bins[e2loc]],[vmin,vmax],':', color='red')
            self.plot.set_xlabel('E (MeV)', fontsize=fontsize1)
            self.plot.set_ylabel('f(E)', fontsize=fontsize1)
            self.plot.set_aspect('auto')
            self.plot.axes.set_xlim([e1min,e1max])
            self.plot.axes.get_figure().tight_layout()
            self.draw()
 
            
        if phase == 'fE-loglog':
            nbin = 200
            #ene = np.sqrt(1.+uxdata**2+uydata**2+uzdata**2)-1.	# energy
            ene = particle_energy(uxdata, uydata, uzdata)
            mass = mass_dic[species]
            amass = amass_dic[species]
            if mass > 1:
                ene = ene * mass * 0.511/amass
            else:
                ene = ene * 0.511
            
            values, bins = np.histogram(ene, bins=nbin, normed=True, weights= wtdata)
            vmin = np.min(values); vmax = np.max(values)   
            
            e1min = ene.min()
            e1max = ene.max()
            self.plot.cla()
            self.plot.loglog(bins[:-1], values)
            #self.plot.semilogy(bins[:-1], values)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel('E (MeV)', fontsize=fontsize1)
            self.plot.set_ylabel('f(E)', fontsize=fontsize1)
            self.plot.set_aspect('auto')
            self.plot.axes.set_xlim([1.e-4,e1max])
            #self.plot.axes.set_ylim([vmin,vmax])
            self.plot.axes.get_figure().tight_layout()
            self.draw()
        
        if warpx:
            phasename = 'px-x'
        else:  
            phasename = 'px-z'   
        if phase == phasename:
            self.plot.cla()
            uxmin = np.min(uxdata)
            uxmax = np.max(uxdata)
            
            if lowreson:
                histogram = histogram_cic_2d( xdata, uxdata, wtdata, nx1, x1min, x1max, nbin, uxmin, uxmax)
            else:
                histogram = histogram_cic_2d( xdata, uxdata, wtdata, nbin, x1min, x1max, nbin, uxmin, uxmax)
                   
            vmax=np.max(histogram)
            vmin = vmax*1.e-4
            vmax *= contrast/100.
            vmin *= 100./contrast
            logthresh=-np.log10(vmin)
            
            im = self.plot.imshow( histogram.T, 
                            origin='lower', extent=[ x1min,x1max,uxmin,uxmax ], aspect='auto', cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
            #im = self.plot.hist2d(xdata, uxdata, bins=nbin, norm=LogNorm(), weights= wtdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            #cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel(xtitle, fontsize=fontsize1)
            self.plot.set_ylabel('$p_x$ (c)', fontsize=fontsize1)
            self.plot.axes.set_xlim([x1min,x1max])
            self.plot.axes.get_figure().tight_layout()
            self.draw()    
                
        if warpx:
            phasename = 'py-x'
        else:  
            phasename = 'py-z'   
        if phase == phasename:
            self.plot.cla()
            uymin = np.min(uydata)
            uymax = np.max(uydata)
            
            if lowreson:
                histogram = histogram_cic_2d( xdata, uydata, wtdata, nx1, x1min, x1max, nbin, uymin, uymax)
            else:
                histogram = histogram_cic_2d( xdata, uydata, wtdata, nbin, x1min, x1max, nbin, uymin, uymax)
                        
            vmax=np.max(histogram)
            vmax *= contrast/100.
            vmin = vmax*1.e-4
            logthresh=-np.log10(vmin)
            
            im = self.plot.imshow( histogram.T, 
                            origin='lower', extent=[ x1min,x1max,uymin,uymax ], aspect='auto', cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
            #im = self.plot.hist2d(xdata, uydata, bins=nbin, norm=LogNorm(), weights= wtdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            #cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel(xtitle, fontsize=fontsize1)
            self.plot.set_ylabel('$p_y$ (c)', fontsize=fontsize1)
            self.plot.axes.set_xlim([x1min,x1max])
            self.plot.axes.get_figure().tight_layout()
            self.draw()
                
        if warpx:
            phasename = 'pz-x'
        else:  
            phasename = 'pz-z' 
        if phase == phasename:
            self.plot.cla()
            uzmin = np.min(uzdata)
            uzmax = np.max(uzdata)
            
            if lowreson:
                histogram = histogram_cic_2d( xdata, uzdata, wtdata, nx1, x1min, x1max, nbin, uzmin, uzmax)
            else:
                histogram = histogram_cic_2d( xdata, uzdata, wtdata, nbin, x1min, x1max, nbin, uzmin, uzmax)
             
            vmax=np.max(histogram)
            vmin = vmax*1.e-4
            vmax *= contrast/100.
            vmin *= 100./contrast
            logthresh=-np.log10(vmin)
            
            im = self.plot.imshow( histogram.T, 
                            origin='lower', extent=[ x1min,x1max,uzmin,uzmax ], aspect='auto', cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh))
            #im = self.plot.hist2d(xdata, uzdata, bins=nbin, norm=LogNorm(), weights= wtdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            #cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel(xtitle, fontsize=fontsize1)
            self.plot.set_ylabel('$p_z$ (c)', fontsize=fontsize1)
            self.plot.axes.set_xlim([x1min,x1max])
            self.plot.axes.get_figure().tight_layout()
            self.draw()
                            
        
        if warpx:
            phasename = 'ene-x'
        else:  
            phasename = 'ene-z'
        if phase == phasename:    
            #ene = np.sqrt(1.+uxdata**2+uydata**2+uzdata**2)-1.	# energy
            ene = particle_energy(uxdata, uydata, uzdata)
            mass = mass_dic[species]
            amass = amass_dic[species]
            if mass > 1:
                ene = ene * mass * 0.511/amass
            else:
                ene = ene * 0.511
                
            self.plot.cla()
            e1min = ene.min()
            e1max = ene.max()
            
            if lowreson:
                histogram = histogram_cic_2d( xdata, ene, wtdata, nx1, x1min, x1max, nbin, e1min, e1max)
            else:
                histogram = histogram_cic_2d( xdata, ene, wtdata, nbin, x1min, x1max, nbin, e1min, e1max)
            
            vmax=np.max(histogram)
            vmin = vmax*1.e-4
            vmax *= contrast/100.
            vmin *= 100./contrast
            logthresh=-np.log10(vmin)
            
            im = self.plot.imshow( histogram.T, 
                            origin='lower', extent=[ x1min,x1max,e1min,e1max ], aspect='auto', cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
            #im = self.plot.hist2d(xdata, ene, bins=nbin, norm=LogNorm(), weights = wtdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            #cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel(xtitle, fontsize=fontsize1)
            self.plot.set_ylabel('E (MeV)', fontsize=fontsize1)
            self.plot.axes.set_xlim([x1min,x1max])
            self.plot.axes.get_figure().tight_layout()
            self.draw()   
            
        if warpx:
            phasename = 'py-px'
            pxtitle = '$p_x$ (c)'
            pytitle = '$p_y$ (c)'
        else:  
            phasename = 'px-pz'
            pxtitle = '$p_z$ (c)'
            pytitle = '$p_x$ (c)'
        if phase == phasename:
            self.plot.cla()
            if warpx:
                uxmin = np.min(uxdata); uxmax = np.max(uxdata)
                uymin = np.min(uydata); uymax = np.max(uydata)
                histogram = histogram_cic_2d( uxdata, uydata, wtdata, nbin, uxmin, uxmax, nbin, uymin, uymax)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)
                
                im = self.plot.imshow( histogram.T, 
                            origin='lower', extent=[ uxmin,uxmax,uymin,uymax ], aspect=aspect, cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(uxdata, uydata, bins=nbin, norm=LogNorm(), weights = wtdata)
            else:
                uxmin = np.min(uxdata); uxmax = np.max(uxdata)
                uzmin = np.min(uzdata); uzmax = np.max(uzdata)
                histogram = histogram_cic_2d( uzdata, uxdata, wtdata, nbin, uzmin, uzmax, nbin, uxmin, uxmax)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)
                
                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ uzmin,uzmax,uxmin,uxmax ], aspect=aspect, cmap='viridis',
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(uzdata, uxdata, bins=nbin, norm=LogNorm(), weights = wtdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            #cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel(pxtitle, fontsize=fontsize1)
            self.plot.set_ylabel(pytitle, fontsize=fontsize1)
            self.plot.axes.get_figure().tight_layout()
            self.draw() 
            
        if warpx:
            phasename = 'pz-px'
            pxtitle = '$p_x$ (c)'
            pytitle = '$p_z$ (c)'
        else:  
            phasename = 'py-pz'
            pxtitle = '$p_z$ (c)'
            pytitle = '$p_y$ (c)'
        if phase == phasename:
            self.plot.cla()
            if warpx:
                uxmin = np.min(uxdata); uxmax = np.max(uxdata)
                uzmin = np.min(uzdata); uzmax = np.max(uzdata)
                histogram = histogram_cic_2d( uxdata, uzdata, wtdata, nbin, uxmin, uxmax, nbin, uzmin, uzmax)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)
                
                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ uxmin,uxmax,uzmin,uzmax ], aspect=aspect, cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(uxdata, uzdata, bins=nbin, norm=LogNorm(), weights = wtdata)
            else:
                uymin = np.min(uydata); uymax = np.max(uydata)
                uzmin = np.min(uzdata); uzmax = np.max(uzdata)
                histogram = histogram_cic_2d( uzdata, uydata, wtdata, nbin, uzmin, uzmax, nbin, uymin, uymax)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)
                
                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ uzmin,uzmax,uymin,uymax ], aspect=aspect, cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(uzdata, uydata, bins=nbin, norm=LogNorm(), weights = wtdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            #cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel(pxtitle, fontsize=fontsize1)
            self.plot.set_ylabel(pytitle, fontsize=fontsize1)
            self.plot.axes.get_figure().tight_layout()
            self.draw()
            
        if warpx:
            phasename = 'pz-py'
            pxtitle = '$p_y$ (c)'
            pytitle = '$p_z$ (c)'
        else:  
            phasename = 'py-px'
            pxtitle = '$p_x$ (c)'
            pytitle = '$p_y$ (c)'
        if phase == phasename:
            self.plot.cla()
            if warpx:
                uymin = np.min(uydata); uymax = np.max(uydata)
                uzmin = np.min(uzdata); uzmax = np.max(uzdata)
                histogram = histogram_cic_2d( uydata, uzdata, wtdata, nbin, uymin, uymax, nbin, uzmin, uzmax)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)
                
                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ uymin,uymax,uzmin,uzmax ], aspect=aspect, cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(uydata, uzdata, bins=nbin, norm=LogNorm(), weights = wtdata)
            else:
                uxmin = np.min(uxdata); uxmax = np.max(uxdata)
                uymin = np.min(uydata); uymax = np.max(uydata)
                histogram = histogram_cic_2d( uxdata, uydata, wtdata, nbin, uxmin, uxmax, nbin, uymin, uymax)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)
                
                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ uxmin,uxmax,uymin,uymax ], aspect=aspect, cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(uxdata, uydata, bins=nbin, norm=LogNorm(), weights = wtdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            #cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel(pxtitle, fontsize=fontsize1)
            self.plot.set_ylabel(pytitle, fontsize=fontsize1)
            self.plot.axes.get_figure().tight_layout()
            self.draw()
                
        if phase == 'ene-theta':
            thetadata = np.arctan(uxdata/np.abs(uzdata))*180/np.pi	# angle
            thetadata= np.nan_to_num(thetadata)       
            #ene = np.sqrt(1.+uxdata**2+uydata**2+uzdata**2)-1.	# energy
            ene = particle_energy(uxdata, uydata, uzdata)
            mass = mass_dic[species]
            amass = amass_dic[species]
            if mass > 1:
                ene = ene * mass * 0.511/amass
            else:
                ene = ene * 0.511
            e1min = ene.min()
            e1max = ene.max()
            self.plot.cla()
            histogram = histogram_cic_2d( thetadata, ene, wtdata, nbin, -90, 90, nbin, e1min, e1max)
            
            vmax=np.max(histogram)
            vmin = vmax*1.e-4
            vmax *= contrast/100.
            vmin *= 100./contrast
            logthresh=-np.log10(vmin)
            
            im = self.plot.imshow( histogram.T, 
                            origin='lower', extent=[ -90,90,e1min,e1max ], aspect='auto', cmap='viridis',
                            vmin=vmin, vmax=vmax,
                            norm=matplotlib.colors.LogNorm(10**-logthresh) )
            #im = self.plot.hist2d(thetadata, ene, bins=nbin, norm=LogNorm(), weights = wtdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            #cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                self.plot.set_title(species+' '+phase+'   t=%6.1f fs' % time, fontsize=fontsize0)
            self.plot.set_xlabel('$\\theta$', fontsize=fontsize1)
            self.plot.set_ylabel('E (MeV)', fontsize=fontsize1)
            self.plot.axes.get_figure().tight_layout()
            self.draw()
        
        
        if debug: print("---matplot imshow time %s seconds ---" % (tm.time() - start_time4))
        
        #############################################
        # Local plot
        #############################################
        if count == 2 and (lineselect or rectselect):
            
            xi1 = np.min([xi,xf])
            xf1 = np.max([xi,xf])
            if lineselect:
                yi1 = x2min
                yf1 = x2max
            else:
                yi1 = np.min([yi,yf])
                yf1=  np.max([yi,yf])

            self.plot = self.figure.add_subplot(122)
            self.plot.cla()

            if debug: start_time1=tm.time()
            if lineselect:
                intersect, = np.where((xdata > xi1) & (xdata < xf1))
            if rectselect:
                intersect, = np.where((xdata > xi1) & (xdata < xf1) & (ydata > yi1) & (ydata < yf1))
            if debug: print("---np.where in local plot time %s seconds ---" % (tm.time() - start_time1))
            nselect = len(intersect)
            if debug: start_time2=tm.time()
            xld = xdata[intersect]
            yld = ydata[intersect]
            uxld = uxdata[intersect]
            uyld = uydata[intersect]
            uzld = uzdata[intersect]
            wt = wtdata[intersect]
            if debug: print("---xdata[intersect] in local plot time %s seconds ---" % (tm.time() - start_time2))
            nthres=10
            if localphase == 'density':
                self.plot.cla()
                if nselect > nthres:
                    #if coord_system == 'cylindrical':
                    #    im = self.plot.hist2d(xld, yld, bins=nbin, norm=LogNorm()) #, weights = wt)
                    #else:
                    #    im = self.plot.hist2d(xld, yld, bins=nbin, norm=LogNorm(), weights = wt)
                    if lowreson:
                        histogram = histogram_cic_2d( xld, yld, wt, nx1, xi1, xf1, nx2, yi1,yf1)
                    else:
                        histogram = histogram_cic_2d( xld, yld, wt, nbin, xi1, xf1, nbin, yi1,yf1)
                    if debug: print("---histogram_cic_2d time %s seconds ---" % (tm.time() - start_time5))
                    
                    vmax=np.max(histogram)
                    vmin = vmax*1.e-4
                    vmax *= contrast/100.
                    vmin *= 100./contrast
                    logthresh=-np.log10(vmin)
                    
                    im = self.plot.imshow( histogram.T, 
                                    origin='lower', extent=[ xi1,xf1, yi1,yf1 ], aspect=aspect,
                            vmin=vmin, vmax=vmax,
                                    norm=matplotlib.colors.LogNorm(10**-logthresh) )
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                if nselect > nthres:
                    cb = self.figure.colorbar(im, cax=cax)
                    #cb = self.figure.colorbar(im[3], cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel(xtitle, fontsize=fontsize1)
                self.plot.set_ylabel(ytitle, fontsize=fontsize1)
                self.plot.axes.set_xlim([xi1,xf1])
                if lineselect == False:
                    self.plot.axes.set_ylim([yi1,yf1])     
                self.plot.set_aspect(aspect)
                self.plot.axes.get_figure().tight_layout()
                self.draw()
        
            if localphase == '1d-dens' and nselect > nthres:
    
                values, bins = np.histogram(xld, bins=100, weights = wt)
                vmin = np.min(values); vmax = np.max(values)
        
                self.plot.cla()
                self.plot.plot(bins[:-1], values)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel(xtitle, fontsize=fontsize1)
                self.plot.set_ylabel(localphase, fontsize=fontsize1)
                self.plot.axes.set_xlim([xi1,xf1])
                self.plot.axes.set_ylim([vmin,vmax])
                self.plot.set_aspect('auto')
                self.plot.axes.get_figure().tight_layout()
                self.draw()
            
            if localphase == 'fE-loglin' and nselect > nthres:
            
                #global maxE_final1
                nbin = 400
                #global emin, emax   
                #ene = np.sqrt(1.+uxld**2+uyld**2+uzld**2)-1.	# energy
                ene = particle_energy(uxld, uyld, uzld)
                mass = mass_dic[species]
                amass = amass_dic[species]
                if mass > 1:
                    ene = ene * mass * 0.511/amass
                else:
                    ene = ene * 0.511
            
                #if animation == False:
                emin = np.min(ene); emax = np.max(ene)
                
                #if animation == False:
                #    maxE_final1 = np.max(ene)
            
                #if numfracCheck1 or numfracCheck2:
                #    values, bins = np.histogram(ene, bins=nbin, normed=True, weights= wt)
                #    vmin = np.min(values); vmax = np.max(values)
                #    e1loc = np.where(bins >= eLeft)[0]
                #    e1loc = e1loc[0]
                #    e2loc = np.where(bins >= eRight)[0]
                #    e2loc = e2loc[0]
                #if window == 1 and numfracCheck1:
                #    numfrac1 = np.sum(values[e1loc:e2loc]*np.diff(bins[e1loc:e2loc+1]))
                #    #print(np.sum(values*np.diff(bins)))
                #if window == 2 and numfracCheck2:
                #    numfrac2 = np.sum(values[e1loc:e2loc]*np.diff(bins[e1loc:e2loc+1]))
            
                self.plot.cla()
                self.plot.hist(ene, bins=nbin, log=True, histtype='step', normed=True, weights=wt)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('E (MeV)', fontsize=fontsize1)
                self.plot.set_ylabel('f(E)', fontsize=fontsize1)
                #if animation:
                #    self.plot.axes.set_xlim([0,maxE_final1])
                self.plot.set_aspect('auto')
                #if (window == 1 and numfracCheck1) or (window == 2 and numfracCheck2):
                #    self.plot.plot([bins[e1loc],bins[e1loc]],[vmin,vmax],':', color='red')
                #    self.plot.plot([bins[e2loc],bins[e2loc]],[vmin,vmax],':', color='red')
            
                if tempfit != 0:
                    eaxis = 1.*np.arange(400)*(emax-emin)/(nbin-1)+emin
                    gaxis = eaxis/(mass*0.511) + 1  # energy Lorentz factor
                    if tempfit == 1:
                        fmax = norm*(gaxis+1)*gaxis*np.exp(-(gaxis-1.)/(temp/(0.511*mass)))
                    else:
                        fmax = norm*(gaxis+1)*gaxis*np.sqrt(gaxis**2-1)*np.exp(-(gaxis-1.)/(temp/(0.511*mass)))
                    self.plot.plot(eaxis, fmax, label='T=%4.1f MeV' %temp)
                    self.plot.legend(fontsize=fontsize1)
                self.plot.axes.get_figure().tight_layout()
                self.draw()
            
            if localphase == 'fE-loglog' and nselect > nthres:
                #global emin, emax
                #ene = np.sqrt(1.+uxld**2+uyld**2+uzld**2)-1.	# energy
                ene = particle_energy(uxld, uyld, uzld)
                mass = mass_dic[species]
                amass = amass_dic[species]
                if mass > 1:
                    ene = ene * mass * 0.511/amass
                else:
                    ene = ene * 0.511
            
                #if animation == False:
                emin = np.min(ene); emax = np.max(ene)
                values, bins = np.histogram(ene, bins=nbin , normed=True, weights =wt)
                vmin = np.min(values); vmax = np.max(values)
            
                #if animation == False:
                self.plot.cla()
                self.plot.loglog(bins[:-1], values, '-*')
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('E (MeV)', fontsize=fontsize1)
                self.plot.set_ylabel('f(E)', fontsize=fontsize1)
                self.plot.set_aspect('auto')
                self.plot.axes.set_xlim([1.e-4,emax])
                self.plot.axes.set_ylim([vmin,vmax])
                #if tempfit != 0:
                #    eaxis = 1.*np.arange(nbin)*(emax-emin)/(nbin-1)+emin
                #    gaxis = eaxis/(mass*0.511) + 1  # energy Lorentz factor
                #    if tempfit == 1:
                #        fmax = norm*(gaxis+1)*gaxis*np.exp(-(gaxis-1.)/(temp/(0.511*mass)))
                #    else:
                #        fmax = norm*(gaxis+1)*gaxis*np.sqrt(gaxis**2-1)*np.exp(-(gaxis-1.)/(temp/(0.511*mass)))
                #    self.plot.plot(eaxis, fmax, label='T=%4.1f' %temp)
                #    self.plot.legend()
                self.plot.axes.get_figure().tight_layout()
                self.draw()                
            
            if warpx:
                localphasename = 'px-x'
            else:  
                localphasename = 'px-z'
            if localphase == localphasename and nselect > nthres:
                self.plot.cla()
                uxldmin = uxld.min(); uxldmax = uxld.max()
                
                if lowreson:
                    histogram = histogram_cic_2d( xld, uxld, wt, nx1, xi1, xf1, nbin, uxldmin,uxldmax)
                else:
                    histogram = histogram_cic_2d( xld, uxld, wt, nbin, xi1, xf1, nbin, uxldmin,uxldmax)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)

                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ xi1,xf1, uxldmin,uxldmax ], aspect='auto',
                                vmin=vmin, vmax=vmax,
                                norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(xld, uxld, bins=nbin, norm=LogNorm(), weights = wt)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im, cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel(xtitle, fontsize=fontsize1)
                self.plot.set_ylabel('$p_x$ (c)', fontsize=fontsize1)
                self.plot.axes.set_xlim([xi1,xf1])
                self.plot.axes.set_ylim([uxldmin,uxldmax])
                self.plot.axes.get_figure().tight_layout()
                self.draw()
            
            if warpx:
                localphasename = 'py-x'
            else:  
                localphasename = 'py-z' 
            if localphase == localphasename and nselect > nthres:
                self.plot.cla()
                uyldmin = uyld.min(); uyldmax = uyld.max()
                if lowreson:
                    histogram = histogram_cic_2d( xld, uxld, wt, nx1, xi1, xf1, nbin, uyldmin,uyldmaxx)
                else:
                    histogram = histogram_cic_2d( xld, uxld, wt, nbin, xi1, xf1, nbin, uyldmin,uyldmax)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)

                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ xi1,xf1, uyldmin,uyldmax ], aspect='auto',
                                vmin=vmin, vmax=vmax,
                                norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(xld, uyld, bins=nbin, norm=LogNorm(), weights = wt)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im, cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel(xtitle+' ($\mu$m)', fontsize=fontsize1)
                self.plot.set_ylabel('$p_y$ (c)', fontsize=fontsize1)
                self.plot.axes.set_xlim([xi1,xf1])
                self.plot.axes.get_figure().tight_layout()
                self.draw()
            
            if warpx:
                localphasename = 'pz-x'
            else:  
                localphasename = 'pz-z'
            if localphase == localphasename and nselect > nthres:
                self.plot.cla()
                uzldmin = uzld.min(); uzldmax = uzld.max()
                if lowreson:
                    histogram = histogram_cic_2d( xld, uzld,  wt, nx1, xi1, xf1, nbin, uzldmin,uzldmax)
                else:
                    histogram = histogram_cic_2d( xld, uzld,  wt, nbin, xi1, xf1, nbin, uzldmin,uzldmax)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)

                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ xi1,xf1, uzldmin,uzldmax ], aspect='auto',
                                vmin=vmin, vmax=vmax,
                                norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(xld, uzld, bins=nbin, norm=LogNorm(), weights = wt)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im, cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel(xtitle, fontsize=fontsize1)
                self.plot.set_ylabel('$p_z$ (c)', fontsize=fontsize1)
                self.plot.axes.set_xlim([xi1,xf1])
                self.plot.axes.get_figure().tight_layout()
                self.draw()
            
            if warpx:
                localphasename = 'ene-x'
            else:  
                localphasename = 'ene-z'
            if localphase == localphasename and nselect > nthres: 
                #ene = np.sqrt(1.+uxld**2+uyld**2+uzld**2)-1.	# energy
                ene = particle_energy(uxld, uyld, uzld)
                mass = mass_dic[species]
                amass = amass_dic[species]
                if mass > 1:
                    ene = ene * mass * 0.511/amass
                else:
                    ene = ene * 0.511
            
                enemin1 = ene.min(); enemax1 = ene.max()
                self.plot.cla()
                if lowreson:
                    histogram = histogram_cic_2d( xld, ene,  wt, nx1, xi1, xf1, nbin, enemin1, enemax1)
                else:
                    histogram = histogram_cic_2d( xld, ene,  wt, nbin, xi1, xf1, nbin, enemin1, enemax1)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)         

                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ xi1,xf1, enemin1, enemax1 ], aspect='auto',
                                vmin=vmin, vmax=vmax,
                                norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(xld, ene, bins=nbin, norm=LogNorm(), weights = wt)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im, cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel(xtitle, fontsize=fontsize1)
                self.plot.axes.set_xlim([xi1,xf1])
                self.plot.axes.get_figure().tight_layout()
                self.draw()    
            
            if warpx:
                localphasename = 'py-px'
                pxtitle = '$p_x$ (c)'
                pytitle = '$p_y$ (c)'
            else:  
                localphasename = 'px-pz'
                pxtitle = '$p_z$ (c)'
                pytitle = '$p_x$ (c)'
            if localphase == localphasename and nselect > nthres:
                self.plot.cla()
                if warpx:
                    uxldmin = uxld.min(); uxldmax = uxld.max()
                    uyldmin = uyld.min(); uyldmax = uyld.max()
                    histogram = histogram_cic_2d( uxld, uyld,  wt, nbin, uxldmin, uxldmax, nbin, uyldmin, uyldmax)
                    
                    vmax=np.max(histogram)
                    vmin = vmax*1.e-4
                    vmax *= contrast/100.
                    vmin *= 100./contrast
                    logthresh=-np.log10(vmin)

                    im = self.plot.imshow( histogram.T, 
                                    origin='lower', extent=[ uxldmin, uxldmax, uyldmin, uyldmax ], aspect=aspect,
                                    vmin=vmin, vmax=vmax,
                                    norm=matplotlib.colors.LogNorm(10**-logthresh) )
                    #im = self.plot.hist2d(uxld, uyld, bins=nbin, norm=LogNorm(), weights = wt)
                else:
                    uzldmin = uzld.min(); uzldmax = uzld.max()
                    uxldmin = uxld.min(); uxldmax = uxld.max()
                    histogram = histogram_cic_2d( uzld, uxld,  wt, nbin, uzldmin, uzldmax, nbin, uxldmin, uxldmax)
                    
                    vmax=np.max(histogram)
                    vmin = vmax*1.e-4
                    vmax *= contrast/100.
                    vmin *= 100./contrast
                    logthresh=-np.log10(vmin)

                    im = self.plot.imshow( histogram.T, 
                                    origin='lower', extent=[ uzldmin, uzldmax, uxldmin, uxldmax ], aspect=aspect,
                                    vmin=vmin, vmax=vmax,
                                    norm=matplotlib.colors.LogNorm(10**-logthresh) )
                    #im = self.plot.hist2d(uzld, uxld, bins=nbin, norm=LogNorm(), weights = wt)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im, cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel(pxtitle, fontsize=fontsize1)
                self.plot.set_ylabel(pytitle, fontsize=fontsize1)
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                
            if warpx:
                localphasename = 'pz-px'
                pxtitle = '$p_x$ (c)'
                pytitle = '$p_z$ (c)'
            else:  
                localphasename = 'py-pz'
                pxtitle = '$p_z$ (c)'
                pytitle = '$p_y$ (c)'
            if localphase == localphasename and nselect > nthres:
                self.plot.cla()
                if warpx:
                    uxldmin = uxld.min(); uxldmax = uxld.max()
                    uzldmin = uzld.min(); uzldmax = uyld.max()
                    histogram = histogram_cic_2d( uxld, uzld,  wt, nbin, uxldmin, uxldmax, nbin, uzldmin, uzldmax)
                    
                    vmax=np.max(histogram)
                    vmin = vmax*1.e-4
                    vmax *= contrast/100.
                    vmin *= 100./contrast
                    logthresh=-np.log10(vmin)

                    im = self.plot.imshow( histogram.T, 
                                    origin='lower', extent=[ uxldmin, uxldmax, uzldmin, uzldmax ], aspect=aspect,
                                    vmin=vmin, vmax=vmax,
                                    norm=matplotlib.colors.LogNorm(10**-logthresh) )
                    #im = self.plot.hist2d(uxld, uzld, bins=nbin, norm=LogNorm(), weights = wt)
                else:
                    uzldmin = uzld.min(); uzldmax = uzld.max()
                    uyldmin = uyld.min(); uyldmax = uyld.max()
                    histogram = histogram_cic_2d( uzld, uxld,  wt, nbin, uzldmin, uzldmax, nbin, uyldmin, uyldmax)
                    
                    vmax=np.max(histogram)
                    vmin = vmax*1.e-4
                    vmax *= contrast/100.
                    vmin *= 100./contrast
                    logthresh=-np.log10(vmin)

                    im = self.plot.imshow( histogram.T, 
                                    origin='lower', extent=[ uzldmin, uzldmax, uyldmin, uyldmax ], aspect=aspect,
                                    vmin=vmin, vmax=vmax,
                                    norm=matplotlib.colors.LogNorm(10**-logthresh) )
                    #im = self.plot.hist2d(uzld, uyld, bins=nbin, norm=LogNorm(), weights = wt)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im, cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel(pxtitle, fontsize=fontsize1)
                self.plot.set_ylabel(pytitle, fontsize=fontsize1)
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                
            if warpx:
                localphasename = 'pz-py'
                pxtitle = '$p_y$ (c)'
                pytitle = '$p_z$ (c)'
            else:  
                localphasename = 'py-px'
                pxtitle = '$p_x$ (c)'
                pytitle = '$p_y$ (c)'
            if localphase == localphasename and nselect > nthres:
                self.plot.cla()
                if warpx:
                    uyldmin = uyld.min(); uyldmax = uyld.max()
                    uzldmin = uzld.min(); uzldmax = uzld.max()
                    histogram = histogram_cic_2d( uyld, uzld,  wt, nbin, uyldmin, uyldmax, nbin, uzldmin, uzldmax)
                    
                    vmax=np.max(histogram)
                    vmin = vmax*1.e-4
                    vmax *= contrast/100.
                    vmin *= 100./contrast
                    logthresh=-np.log10(vmin)

                    im = self.plot.imshow( histogram.T, 
                                    origin='lower', extent=[ uyldmin, uyldmax, uzldmin, uzldmax ], aspect=aspect,
                                    vmin=vmin, vmax=vmax,
                                    norm=matplotlib.colors.LogNorm(10**-logthresh) )
                    #im = self.plot.hist2d(uyld, uzld, bins=nbin, norm=LogNorm(), weights = wt)
                else:
                    uxldmin = uxld.min(); uxldmax = uxld.max()
                    uyldmin = uyld.min(); uyldmax = uyld.max()
                    histogram = histogram_cic_2d( uxld, uyld,  wt, nbin, uxldmin, uxldmax, nbin, uyldmin, uyldmax)
                    
                    vmax=np.max(histogram)
                    vmin = vmax*1.e-4
                    vmax *= contrast/100.
                    vmin *= 100./contrast
                    logthresh=-np.log10(vmin)

                    im = self.plot.imshow( histogram.T, 
                                    origin='lower', extent=[ uxldmin, uxldmax, uyldmin, uyldmax ], aspect=aspect,
                                    vmin=vmin, vmax=vmax,
                                    norm=matplotlib.colors.LogNorm(10**-logthresh) )
                    #im = self.plot.hist2d(uxld, uyld, bins=nbin, norm=LogNorm(), weights = wt)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im, cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel(pxtitle, fontsize=fontsize1)
                self.plot.set_ylabel(pytitle, fontsize=fontsize1)
                self.plot.axes.get_figure().tight_layout()
                self.draw()
            
            if localphase == 'ene-theta' and nselect > nthres:
        
                theta = np.arctan(uxld/np.abs(uzld))*180/np.pi	# angle
                theta= np.nan_to_num(theta)       
                #ene = np.sqrt(1.+uxld**2+uyld**2+uzld**2)-1.	# energy
                ene = particle_energy(uxld, uyld, uzld)
                mass = mass_dic[species]
                amass = amass_dic[species]
                if mass > 1:
                    ene = ene * mass * 0.511/amass
                else:
                    ene = ene * 0.511
        
                self.plot.cla()
                thetamin1 = theta.min(); thetamax1 = theta.max()
                enemin1 = ene.min(); enemax1 = ene.max()
                histogram = histogram_cic_2d( theta, ene,  wt, nbin, thetamin1, thetamax1, nbin, enemin1, enemax1)
                
                vmax=np.max(histogram)
                vmin = vmax*1.e-4
                vmax *= contrast/100.
                vmin *= 100./contrast
                logthresh=-np.log10(vmin)

                im = self.plot.imshow( histogram.T, 
                                origin='lower', extent=[ thetamin1, thetamax1, enemin1, enemax1 ], aspect='auto',
                                vmin=vmin, vmax=vmax,
                                norm=matplotlib.colors.LogNorm(10**-logthresh) )
                #im = self.plot.hist2d(theta, ene, bins=nbin, norm=LogNorm(), weights = wt)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im, cax=cax)
                self.plot.axes.set_xlim([-90.,90.])
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('$\\theta$', fontsize=fontsize1)
                self.plot.set_ylabel('E (MeV)', fontsize=fontsize1)
                self.plot.axes.get_figure().tight_layout()
                self.draw()
            """"
            if localphase == 'MaxE':
            
                global maxene_arr1, maxene_arr2
                global t_arr1, t_arr2
                global maxE_final2, maxE_final3
            
                if nselect == 0:  
                    ene = [0]
                else:
                    ene = np.sqrt(1.+uxld**2+uyld**2+uzld**2)-1.	# energy
                    mass = mass_dic[species]
                    amass = amass_dic[species]
                    ene = ene * mass * 0.511/amass

            
                # initialize arrays
                if animation == False:
                    tmax_current = tstep
                    if window == 1:
                        t_arr1 = []
                        maxene_arr1 = []
                        maxE_final2 = np.max(ene)
                    if window == 2:
                        t_arr2 = []
                        maxene_arr2 = []
                        maxE_final3 = np.max(ene)
            
                if animation == False:
                
                    self.plot.cla()
                    self.plot.plot([time, time], [np.max(ene), np.max(ene)], 'o', color='blue')
                    self.plot.axes.set_xlim([0,1.2*np.max(taxis)])
                    self.plot.axes.set_ylim([0,1.2*np.max(ene)])
                    self.plot.set_title(localphase, fontsize=fontsize0)
                    self.plot.set_xlabel('t (fs)', fontsize=fontsize1)
                    self.plot.set_ylabel('E (MeV)', fontsize=fontsize1)
                    self.plot.axes.get_figure().tight_layout()
                    self.draw()
            
                if animation: 
                
                    if window == 1:
                        t_arr1.append(time)
                        maxene_arr1.append(np.max(ene))
                    if window == 2:
                        t_arr2.append(time)
                        maxene_arr2.append(np.max(ene)) 
                    
                    self.plot.cla()
                    if window == 1:
                        self.plot.plot(t_arr1, maxene_arr1, 'o-', color='blue')
                        self.plot.axes.set_ylim([0,1.2*maxE_final2])
                    if window == 2:
                        self.plot.plot(t_arr2, maxene_arr2, 'o-', color='blue')    
                        self.plot.axes.set_ylim([0,1.2*maxE_final3])
                    self.plot.axes.set_xlim([0,1.2*np.max(taxis)])
                    self.plot.set_title(localphase, fontsize=fontsize0)
                    self.plot.set_xlabel('t (fs)', fontsize=fontsize1)
                    self.plot.set_ylabel('E (MeV)', fontsize=fontsize1)
                    self.plot.axes.get_figure().tight_layout()
                    self.draw()
                
                    # re-initialize arrays after animation has been done
                    if tstep == len(taxis):
                        if window == 1:
                            t_arr1 = []
                            maxene_arr1 = []
                        else:
                            t_arr2 = []
                            maxene_arr2 = []
                        
            if localphase == 'totalE':
            
                global t3_arr1, t3_arr2
                global totalene_arr1, totalene_arr2
                global maxE_final4, maxE_final5
            
                if(nselect == 0):
                    totalene = 0
                else:
                    ene = np.sqrt(1.+uxld**2+uyld**2+uzld**2)-1.	# energy
                    ene = ene*wt
                    totalene = np.sum(ene)
                    mass = mass_dic[species]
                    amass = amass_dic[species]
                    totalene = totalene * mass * 0.511/amass

                
                # initialize arrays
                if animation == False:
                    if window == 1:
                        t3_arr1 = []
                        totalene_arr1 = []
                    if window == 2:
                        t3_arr2 = []
                        totalene_arr2 = []
            
                if animation == False:
                
                    self.plot.cla()
                    self.plot.plot([time, time], [totalene, totalene], 'o', color='blue')
                    self.plot.axes.set_xlim([0,1.2*time])
                    self.plot.axes.set_ylim([0,1.2*totalene])
                    self.plot.set_title(localphase, fontsize=fontsize0)
                    self.plot.set_xlabel('t (fs)', fontsize=fontsize1)
                    self.plot.set_ylabel('E (MeV)', fontsize=fontsize1)
                    self.plot.axes.get_figure().tight_layout()
                    self.draw()
            
                if animation == True: 
                
                    if window == 1:
                        t3_arr1.append(time)
                        totalene_arr1.append(totalene)
                    if window == 2:
                        t3_arr2.append(time)
                        totalene_arr2.append(totalene)
                    
                    self.plot.cla()
                    if window == 1:
                        self.plot.plot(t3_arr1, totalene_arr1, 'o-', color='blue')
                        self.plot.axes.set_ylim([0,1.2*maxE_final4])
                    if window == 2:
                        self.plot.plot(t3_arr2, totalene_arr2, 'o-', color='blue')    
                        self.plot.axes.set_ylim([0,1.2*maxE_final5])
                    self.plot.axes.set_xlim([0,1.2*np.max(taxis)])
                    self.plot.set_title(localphase, fontsize=fontsize0)
                    self.plot.set_xlabel('t (fs)', fontsize=fontsize1)
                    self.plot.set_ylabel('E (MeV)', fontsize=fontsize1)
                    self.plot.axes.get_figure().tight_layout()
                    self.draw()
                    
                    # re-initialize arrays after animation has been done
                    if tstep == len(taxis):
                        if window == 1:
                            t3_arr1 = []
                            totalene_arr1 = []
                        else:
                            t3_arr2 = []
                            totalene_arr2 = []
            
            if localphase == 'data':             
                c = 3.e8
                gamma = np.sqrt(1.+uxld**2+uyld**2+uzld**2) # Lorentz gamma
                vx = ux/gamma*c
                vy = uy/gamma*c
                vz = uz/gamma*c  
                
                file_name = species+"_t%d_n%d.txt"%(iteration,nselect)
                file = open("../"+file_name, "w")
                for i in np.arange(nselect):
                    file.write(str(z[i]*1.e-6)+"    "+str(x[i]*1.e-6)+"    "+str(vx[i])+"    "+str(vy[i])+"    "+str(vz[i])+"\n")
                file.close()
                print("wrote "+file_name)
            
        #stop = timeit.default_timer()           
        #print('particle plotting time = ',stop - start2)            
        #print('particle total time = ',stop - start)                   

        """ 
      
        if saveimg == 1 or saveimg == 2:
            if warpx == True:
                if count == 0:
                    image_dir = '.'
                    filename = species+'_'+phase
                else:
                    image_dir = '.'
                    filename =  species+'_'+phase+'_'+localphase
            else:
                if count == 0:
                    image_dir = '..'
                    filename = species+'_'+phase
                else:
                    image_dir = '..'
                    filename =  species+'_'+phase+'_'+localphase
   
    
            if saveimg == 1:
                self.figure.savefig(image_dir+'/'+filename+'%3.3d.png' %tstep, format='png')  
                print(image_dir+'/'+filename+'%3.3d.png saved' %tstep)
            if saveimg == 2:
                self.figure.savefig(image_dir+'/'+filename+'%3.3d.eps' %tstep, format='eps', dpi=60)
                print(image_dir+'/'+filename+'%3.3d.png saved' %tstep)
                    
        if saveimg == 3:
                    
            if warpx == True:
                if count == 0:
                    image_dir = './movie'
                    filename = species+'_'+phase
                else:
                    image_dir = './movie'
                    filename =  species+'_'+phase+'_'+localphase
            else:
                if count == 0:
                    image_dir = '../movie'
                    filename = species+'_'+phase
                else:
                    image_dir = '../movie'
                    filename =  species+'_'+phase+'_'+localphase    
        
            self.figure.savefig(image_dir+'/'+filename+'%3.3d.png' %tstep, format='png')
            print(image_dir+'/'+filename+'%3.3d.png saved' %tstep)
           

    def yt_volume(self, tstep, yt_volumedata, field):
        
        self.figure.clear()
        
        self.plot = self.figure.add_subplot(111)
        nbin = 200
        fontsize0 = 14; fontsize1 = 14
        cbarwidth = 0.16
        
        
        self.plot.cla()
        im =  self.plot.imshow(yt_volumedata)
        self.plot.axes.get_xaxis().set_visible(False)
        self.plot.axes.get_yaxis().set_visible(False)
        self.plot.set_title(field, fontsize=fontsize0)
        self.plot.axes.get_figure().tight_layout()
        self.draw()
        
                    
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
                
    
        