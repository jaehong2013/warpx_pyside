import os
from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
from Cython.Distutils import build_ext
 
os.system("rm -rf *.c *.so build/") 

setup(
  name = "histogram_threading",
  cmdclass = {"build_ext": build_ext},
  ext_modules =
  [
    Extension("histogram_threading",
              ["histogram_threading.pyx"],
              extra_compile_args = ["-O0", "-fopenmp"],
              extra_link_args=['-fopenmp']
              )
  ]
)

