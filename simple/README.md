## simple test toolkit ##

This is a simple test version to learn basic pyside tools and how to 
load the Warp/Warpx data files.
You can make .png or .eps image files, and also multiple png images for a movie.

This code imports simple.py which provides the basic framework for the GUI.

Go to the folder where your simulation data files are located and type in the command line,

```
#!shell

 python ~/warpx_pyside/simple/simple_analysis.py
```