"""
simple_analysis.py

by Jaehong Park (jaehongpark@lbl.gov)

This code provides a simple PySide based visualization tool
for the Warp and WarpX data analysis.
This is a simple test version to learn basic pyside tools and how to 
load the Warp/Warpx data files.
You can make .png or .eps image files, and also multiple png images for a movie.

This code imports simple.py which provides the basic framework for the GUI.

To run it, go to the folder where your data files are located and type in the command line
>> python ~/warpx_pyside/simple_analysis.py
*** The paython codes may be located at $HOME/warpx_pyside.

Last updates:
7/2017

"""
import sys
import time as tm
from simple import *
import matplotlib
matplotlib.rcParams['backend.qt4']='PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure

import yt, glob
from yt.units import volt
import h5py
import numpy as np
from opmd_viewer import OpenPMDTimeSeries
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
import threading

# Define path way for Warp results and find iterations
file_path = '.'
file_list = glob.glob(file_path + '/plt?????')
if len(file_list) != 0:
    iterations = [ int(file_name[len(file_name)-5:]) for file_name in file_list ]
    warpx = True
else:
    file_list = glob.glob(file_path + '/data????????.h5')
    iterations = [ int(file_name[len(file_name)-11:len(file_name)-3]) for file_name in file_list ]
    warpx = False

file_list.sort();
iterations.sort();
tnum = len(iterations)
tstep = tnum
iteration = iterations[tstep-1]

# list of field variables
field_list = []
field_list.append('Ex')
field_list.append('Ey')
field_list.append('Ez')
field_list.append('Bx')
field_list.append('By')
field_list.append('Bz')
field_list.append('Jx')
field_list.append('Jy')
field_list.append('Jz')

# get parameters
if warpx:
    ds = yt.load(file_path + '/plt%05d/'%(iteration));
    zmin=ds.domain_left_edge[0].d*1.e6
    xmin=ds.domain_left_edge[1].d*1.e6
    zmax=ds.domain_right_edge[0].d*1.e6
    xmax=ds.domain_right_edge[1].d*1.e6
    time=ds.current_time.d*1.e15
    taxis = (np.arange(tnum)+1)*time/tnum
    nz = ds.domain_dimensions[0]
    nx = ds.domain_dimensions[1] 
    zaxis=1.*np.arange(nz)*(zmax-zmin)/(nz-1)+zmin
    xaxis=1.*np.arange(nx)*(xmax-xmin)/(nx-1)+xmin
else:
    ts = OpenPMDTimeSeries(file_path)
    ###  get infomation of space and time
    info = ts.get_field( iteration=iterations[0],  field='E', coord='x')[1]
    taxis = ts.t*1.e15; time = taxis[tnum-1]
    zmin = info.zmin*1.e6; zmax = info.zmax*1.e6
    xmin = info.xmin*1.e6; xmax = info.xmax*1.e6
    zaxis = info.z*1.e6; xaxis = info.x*1.e6
    nz = len(zaxis); nx = len(xaxis)       

nzL = 0;nzR = nz-1	# left (right) end cell of the zaxis
nxL = 0;nxR = nx-1	# left (right) end cell of the xaxis

animation = False
field1 = 'Ex'; field2 = 'Ey'; field3 = 'Ez'

# default the mainwindow size set in visualization.py
xsize0=826; ysize0=626
x1 = 300; x2 = 10
y1 = 30; y2 = 50
# default margins for the plot panels
#<----        xsize0            ------->
#---------------------------------------|
#                    y1                 |
#               |---------------|       |
#               |               |       | ysize0
#< ---  x1 ---> |               |<- x2->|	
#               |   plot panels |       |	
#               |               |       |	
#               |---------------|       |
#                      y2               |
#---------------------------------------|
# ratio of the main plot width to the local plot width
ratio = 1.
# size of each main plot panel
mainwidth = (xsize0-(x1+x2))*ratio
mainheight = 1.*(ysize0-(y1+y2))

class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyForm, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # set mainwindow title as the current directory
        self.setWindowTitle(os.getcwd())
        # emit signal to open directory
        self.connect(self.ui.actionOpen, QtCore.SIGNAL('triggered()' ), self.opendir)
        self.connect(self.ui.actionClose, QtCore.SIGNAL('triggered()' ), self.close)	
        # (x,y) coordinate labeli
        self.ui.coordLabel.setText(str('(0,0)'))

        # time label
        self.ui.tstepLabel.setText("%d" %tstep)
        self.ui.timeLabel.setText("%6.1f /fs" % time)

        # timestep stride spin box
        self.ui.stepSpinBox.setValue(1)
        self.ui.stepSpinBox.setMinimum(1)

        # backward time button
        self.ui.backwardtimeButton.clicked.connect(self.backwardtimebutton)
        # foward time button
        self.ui.forwardtimeButton.clicked.connect(self.forwardtimebutton)
        
        # timestep slider
        self.ui.timeSlider.setRange(1,tnum)
        self.ui.timeSlider.setSingleStep(1)
        self.ui.timeSlider.setValue(tnum)
        self.ui.timeSlider.valueChanged.connect(self.timeslider)
        
        # plot PushButton
        self.ui.plotButton.clicked.connect(self.plotbutton)
        
        # animation PushButton
        self.ui.animationButton.clicked.connect(self.animationbutton)
        self.ui.tiniSpinBox.setMinimum(1)
        self.ui.tiniSpinBox.setMaximum(tnum)
        self.ui.tiniSpinBox.setValue(1)
        self.ui.tmaxSpinBox.setMinimum(1)
        self.ui.tmaxSpinBox.setMaximum(tnum)
        self.ui.tmaxSpinBox.setValue(tnum)
        
        # select image CheckBox
        self.ui.pngCheckBox.setChecked(False)
        self.ui.pngCheckBox.clicked.connect(self.pngcheckbox)
        self.ui.epsCheckBox.setChecked(False)
        self.ui.epsCheckBox.clicked.connect(self.epscheckbox)
        self.ui.movieCheckBox.setChecked(False)
        self.ui.movieCheckBox.clicked.connect(self.moviecheckbox)
        
        # Matplotlib widgets in the 1st panel
        self.ui.widget1 = MatplotlibWidget(self.ui.centralwidget)
        self.ui.widget1.setGeometry(QtCore.QRect(x1, y1, mainwidth, mainheight))
        self.ui.widget1.setObjectName("widget1")
         
        self.plotbutton()

    def resizeEvent(self,  event):
        width = event.size().width()
        height = event.size().height()
        mainwidth = (width-(x1+x2))*ratio
        mainheight = 1.*(height-(y1+y2))
        
        self.ui.widget1.setGeometry(QtCore.QRect(x1, y1, mainwidth, mainheight))
 
	# open a new directory and load files for basic information
    def opendir(self):	
        pass
        
    def close(self):
        sys.exit(0)
              
    def backwardtimebutton(self):
        step = self.ui.stepSpinBox.value()
        tstep=self.ui.timeSlider.value()
        tstep = tstep - step
        if tstep < 1:
            tstep = tstep + step
        self.ui.tstepLabel.setText("%d" %tstep)
        time = taxis[tstep-1]    
        self.ui.timeLabel.setText("%6.1f fs" % time)
        self.ui.timeSlider.setValue(tstep)
        self.plotbutton()
        
    def forwardtimebutton(self):
        step = self.ui.stepSpinBox.value()
        tstep=self.ui.timeSlider.value()
        tstep = tstep + step
        if tstep > tnum:
            tstep = tstep - step
        self.ui.tstepLabel.setText("%d" %tstep)
        time = taxis[tstep-1]    
        self.ui.timeLabel.setText("%6.1f fs" % time)
        self.ui.timeSlider.setValue(tstep)
        self.plotbutton()
       
    def timeslider(self):
        tstep=self.ui.timeSlider.value()
        time = taxis[tstep-1]
        self.ui.tstepLabel.setText("%d" %tstep)
        self.ui.timeLabel.setText("%6.1f fs" % time)

    def pngcheckbox(self):
        if self.ui.pngCheckBox.isChecked():
            self.ui.epsCheckBox.setChecked(False)
            self.ui.movieCheckBox.setChecked(False)
    def epscheckbox(self):
        if self.ui.epsCheckBox.isChecked():
            self.ui.pngCheckBox.setChecked(False)
            self.ui.movieCheckBox.setChecked(False)
    def moviecheckbox(self):
        if self.ui.movieCheckBox.isChecked():
            self.ui.pngCheckBox.setChecked(False)
            self.ui.epsCheckBox.setChecked(False)
            if(warpx == True):
                os.system('mkdir ./movie')
            else:
                os.system('mkdir ../movie')
                
    def plotbutton(self):
        if animation == False:
            tstep=self.ui.timeSlider.value()
            saveimg = 0
            if self.ui.pngCheckBox.isChecked(): saveimg = 1
            if self.ui.epsCheckBox.isChecked(): saveimg = 2
            if self.ui.movieCheckBox.isChecked(): saveimg = 3
            self.ui.widget1.plotfield(tstep, field_list, saveimg)
        else:
            pass
        
    def myEvenListener(self):
        global animation
        saveimg = 1
        # While animation is running, animation = True
        animation = True
        saveimg = 0
        if self.ui.pngCheckBox.isChecked(): saveimg = 1
        if self.ui.epsCheckBox.isChecked(): saveimg = 2
        if self.ui.movieCheckBox.isChecked(): saveimg = 3
        tini = self.ui.tiniSpinBox.value()
        tmax = self.ui.tmaxSpinBox.value()
        step = self.ui.stepSpinBox.value()
        # number of iterations in animation
        num = (tmax-tini)/step+1
        if num<0: num=1
        tarr = np.zeros(num, dtype=np.int)
        t = tmax
        for i in np.arange(num)[::-1]:
            tarr[i] = t
            t = t - step
        for tstep in tarr:
            tm.sleep(0.005)
            self.ui.tstepLabel.setText("%d" %tstep)
            self.ui.timeLabel.setText("%6.1f fs" % time)
            self.ui.timeSlider.setValue(tstep)
            self.ui.widget1.plotfield(tstep, field_list, saveimg)
            tstep = tstep + step
        # When animation is done, animation = False
        animation = False
        
    def animationbutton(self):
        # while animation is running, this function is skipped.
        if animation == False:
            self.c_thread=threading.Thread(target=self.myEvenListener)
            self.c_thread.start()
        else:
            pass    
        
class MatplotlibWidget(Canvas):
    
    def __init__(self, parent=None):		
        super(MatplotlibWidget, self).__init__(Figure())
        
        self.setParent(parent)
        self.figure = Figure(dpi=60)
        self.canvas = Canvas(self.figure)
        self.figure.set_facecolor('0.85')
        
    def plotfield(self, tstep, field_list, saveimg):
        
        self.figure.clear()
        
        iteration = iterations[tstep-1]
        # read data files
        if warpx:
            ds = yt.load(file_path + '/plt%05d/'%(iteration));
            zmin=ds.domain_left_edge[0].d*1.e6
            xmin=ds.domain_left_edge[1].d*1.e6
            zmax=ds.domain_right_edge[0].d*1.e6
            xmax=ds.domain_right_edge[1].d*1.e6
            time=ds.current_time.d*1.e15
            nz = ds.domain_dimensions[0]
            nx = ds.domain_dimensions[1] 
            zaxis=1.*np.arange(nz)*(zmax-zmin)/(nz-1)+zmin
            xaxis=1.*np.arange(nx)*(xmax-xmin)/(nx-1)+xmin
            
            all_data_level = ds.covering_grid(level=0,
                left_edge=ds.domain_left_edge, dims=ds.domain_dimensions)
            fdata1 = all_data_level[field_list[0]][:, :, 0]
            fdata1 = np.array(fdata1)
            fdata2 = all_data_level[field_list[1]][:, :, 0]
            fdata2 = np.array(fdata2)
            fdata3 = all_data_level[field_list[2]][:, :, 0]
            fdata3 = np.array(fdata3)
        else:
            ts = OpenPMDTimeSeries(file_path)            
            info = ts.get_field( iteration=iteration,  field='E', coord='x')[1]
            taxis = ts.t*1.e15; time = taxis[tnum-1]
            zmin = info.zmin*1.e6; zmax = info.zmax*1.e6
            xmin = info.xmin*1.e6; xmax = info.xmax*1.e6
            zaxis = info.z*1.e6; xaxis = info.x*1.e6
            nz = len(zaxis); nx = len(xaxis)
  
            field = field_list[0]
            fdata1 = ts.get_field(iteration=iteration, field=field[0], coord=field[1])[0]
            field = field_list[1]
            fdata2 = ts.get_field(iteration=iteration, field=field[0], coord=field[1])[0]
            field = field_list[2]
            fdata3 = ts.get_field(iteration=iteration, field=field[0], coord=field[1])[0]
            
        nzL = 0;nzR = nz-1	# left (right) end cell of the zaxis
        nxL = 0;nxR = nx-1	# left (right) end cell of the xaxis
	          
        nbin = 200
        fontsize0 = 13;fontsize1 = 13
        cbarwidth = 0.16
        
        global vmin1, vmin2, vmin3
        global vmax1, vmax2, vmax3
        
        if animation == False:
            vmin1 = np.min(fdata1);vmax1 = np.max(fdata1)
            vmin2 = np.min(fdata2);vmax2 = np.max(fdata2)
            vmin3 = np.min(fdata3);vmax3 = np.max(fdata3)
             
        self.plot = self.figure.add_subplot(311)
        self.plot.cla()
        im =  self.plot.imshow(fdata1, interpolation='nearest', cmap='jet',
            origin='lower', extent=[ zmin,zmax,xmin,xmax ], aspect='auto', vmin=vmin1, vmax=vmax1)
        ax = self.figure.gca()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size=cbarwidth, pad=0.)
        cb = self.figure.colorbar(im, cax=cax)
        #self.plot.set_title(field1, fontsize=fontsize0)
        #self.plot.set_xlabel('x ($\mu$m)', fontsize=fontsize1)
        self.plot.set_ylabel('x ($\mu$m)', fontsize=fontsize1)
        self.plot.axes.set_xlim([zmin,zmax])
        self.plot.axes.set_ylim([xmin,xmax])
        self.plot.axes.get_figure().tight_layout()
        self.draw()
        
        self.plot = self.figure.add_subplot(312)
        self.plot.cla() 
        im =  self.plot.imshow(fdata2, interpolation='nearest', cmap='jet',
            origin='lower', extent=[ zmin,zmax,xmin,xmax ], aspect='auto', vmin=vmin2, vmax=vmax2)
        ax = self.figure.gca()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size=cbarwidth, pad=0.)
        cb = self.figure.colorbar(im, cax=cax)
        #self.plot.set_title(field2, fontsize=fontsize0)
        #self.plot.set_xlabel('x ($\mu$m)', fontsize=fontsize1)
        self.plot.set_ylabel('x ($\mu$m)', fontsize=fontsize1)
        self.plot.axes.set_xlim([zmin,zmax])
        self.plot.axes.set_ylim([xmin,xmax])
        self.plot.axes.get_figure().tight_layout()
        self.draw()
        
        self.plot = self.figure.add_subplot(313)
        self.plot.cla()
        im =  self.plot.imshow(fdata3, interpolation='nearest', cmap='jet',
            origin='lower', extent=[ zmin,zmax,xmin,xmax ], aspect='auto', vmin=vmin3, vmax=vmax3)
        ax = self.figure.gca()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size=cbarwidth, pad=0.)
        cb = self.figure.colorbar(im, cax=cax)
        #self.plot.set_title(field3, fontsize=fontsize0)
        self.plot.set_xlabel('z ($\mu$m)', fontsize=fontsize1)
        self.plot.set_ylabel('x ($\mu$m)', fontsize=fontsize1)
        self.plot.axes.set_xlim([zmin,zmax])
        self.plot.axes.set_ylim([xmin,xmax])
        self.plot.axes.get_figure().tight_layout()
        self.draw()
        
        if saveimg !=0:
            if warpx == True:
                image_dir = '.'
            else:
                image_dir = '..'
        
            if saveimg == 1:
                self.figure.savefig(image_dir+'/image%3.3d.png' %tstep, format='png')    
            if saveimg == 2:
                self.figure.savefig(image_dir+'/image%3.3d.eps' %tstep, format='eps', dpi=60)
            if saveimg == 3 and animation == True:
                self.figure.savefig(image_dir+'/movie/image%3.3d.png' %tstep, format='png')
            
            
if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	myapp = MyForm()
	myapp.show()
	sys.exit(app.exec_())
