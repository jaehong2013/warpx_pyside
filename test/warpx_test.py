#!/usr/bin/env python2.7
import sys, glob

import time as tm
import basewindow
from base import *

from window_test import *

import h5py
import numpy as np
from scipy import ndimage
#from opmd_viewer import OpenPMDTimeSeries
import os
import threading

import matplotlib
matplotlib.rcParams['backend.qt4']='PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as Canvas
   
 
# SI unit
C = 2.99792458e8 # light speed
me = 9.10938291e-31 # electron mass
qe = 1.60217657e-19 # electron charge

# check if the data file is from WarpX or Warp
file_path = '.'

file_list = glob.glob(file_path + '/data????????.h5')
iterations = [ int(file_name[len(file_name)-11:len(file_name)-3]) for file_name in file_list ]
warpx = False
#ts = OpenPMDTimeSeries(file_path,check_all_files=False )

file_list.sort()
iterations.sort()
tnum = len(iterations)
tstep = tnum
iteration = iterations[tstep-1]

mass_dic = {}   # mass dictionary
charge_dic = {} # charge dictionary
amass_dic = {} # atomic mass number dictionary
# find particle species

fi = h5py.File(file_list[tstep-1], 'r')
dset= fi['data/'+str(iteration)+'/particles']
species_list = dset.keys()
nspecies = len(species_list)
    
for species in species_list:
    q = dset[species+'/charge'].attrs['value']
    m = dset[species+'/mass'].attrs['value']
    mass_dic[species] = m/me     # mass ratio
    charge_dic[species] = q/qe   # charge ratio
    if q > 0: amass_dic[species] = m/(me*1836)  # atomic mass number
    else: amass_dic[species] = 1.

print('e-ion mass ratio = ', mass_dic)
print('charge ratio (atomic number) = ', charge_dic)
print('atomic mass number = ', amass_dic)    

# get times
taxis=np.zeros(tnum)

s=0
for files in file_list:
    fi = h5py.File(files,'r')
    dset=fi['/data/'+str(iterations[s])]
    time=dset.attrs['time']*1.e15
    taxis[s]=time
    s+=1

# find if the coordinate system is 'Cartesian or cylindrical

item = fi['data/'+str(iteration)+'/fields/E'].attrs['geometry']
if item == 'cartesian':
    coord_system = 'cartesian'
else:
    coord_system = 'cylindrical'
        
item = fi['data/'+str(iteration)+'/fields/E'].attrs['axisLabels']
dim = len(item)
            
# find fields
field_list = []
   
item = fi['data/'+str(iteration)+'/fields']
fields_group = item.items()
nfields = len(fields_group)
    
item = fi['data/'+str(iteration)+'/fields/E']
coord_group = item.items()
ncoord = len(coord_group)
coord_list = []
for i in np.arange(ncoord):
    coord_list.append(coord_group[i][0])
        
for i in np.arange(nfields):
    for j in np.arange(ncoord):
        if fields_group[i][0] != 'rho':
            field_list.append(fields_group[i][0]+coord_list[j])
        else:
            field_list.append(fields_group[i][0])
            break

# list phase space variables
phase_list = []

phase_list.append('density')
phase_list.append('fE-loglin')
phase_list.append('fE-loglog')
phase_list.append('px-z')
phase_list.append('py-z')
phase_list.append('pz-z')
phase_list.append('ene-z')
phase_list.append('px-pz')
phase_list.append('py-pz')
phase_list.append('py-px')
phase_list.append('ene-theta')    

x1min_zoom = 0; x1max_zoom = 100
x2min_zoom = 0; x2max_zoom = 100
# default values of global variables
#if check_field:
#    field = field_list[0]
#species = species_list[0]
#phase = phase_list[0]
#localfield= localfldphase_list[0]
#localparticle = localprtlphase_list[0]


sliceplane = 'xz'
xsliceloc = 50
ysliceloc = 50
zsliceloc = 50

nrows = 1; ncolumns = 1
#select panel
selectpanel = 1
numpanel = nrows*ncolumns
numrowpanel = 1; numcolumnpanel =1

# field data dictionary
# eg. fdata = {'panel_1': [....], 'panel_2': [...], ...}
# The dictionary keyword is a data name in each window panel
global fdata
fdata = {}
# field name dictionary
global field_dic
field_dic = {}
# particle data dictionary
global xdata, ydata, zdata, uxdata, uydata, uzdata, wtdata
xdata = {}; ydata = {}; zdata = {}
uxdata = {}; uydata = {}; uzdata = {}; wtdata = {}
# species name dictionary
global species_dic
species_dic = {}
# particle phase name dictionary
global phase_dic
phase_dic = {}


xsize0 = 550 #804
ysize0 = 350 #396
xmargin1 = 10; xmargin2 = 10
ymargin1 = 5; ymargin2 = 6


# This QmainWindow embeds matplotlib widgets
class MyPlot(QtGui.QMainWindow):
    def __init__(self, parent=None):
                
        super(MyPlot, self).__init__(parent)
        self.ui = basewindow.Ui_MainWindow()
        self.ui.setupUi(self)
        
        # set mainwindow title as the current directory
        self.setWindowTitle(os.getcwd())
        
        plotwidget = self.geometry()
        # original window panel size
        #xsize0 = plotwidget.width()
        #ysize0 = plotwidget.height()
        
        # resize the window panel
        xsize1 = ncolumns*(xsize0/ncolumns**0.)
        ysize1 = nrows*(ysize0/nrows**0.)
        self.resize(xsize1, ysize1)

        # default margins for the plot panels
        #<---       xsize0       ------->
        #-------------------------------| ^
        #            y1                 | |
        #       |---------------|       | |
        #       |               |       |
        #<-x1-> |               |<- x2->|ysize0	
        #       |   plot panels |       |	
        #       |               |       | |	
        #       |---------------|       | |
        #               y2              | |
        #-------------------------------| v
        # size of each main plot panel
        mainwidth = xsize1/ncolumns-(xmargin1+xmargin2)
        mainheight = ysize1/nrows-(ymargin1+ymargin2)
        npanels = nrows*ncolumns
        panel = 0
        xpos = np.mod(panel,ncolumns)
        ypos = panel/ncolumns

        self.widget0 = MatplotlibWidget(self.ui.centralwidget)
        self.widget0.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
        self.widget0.setObjectName("widget")
             
    def resizeEvent(self,  event):
        
        width = event.size().width()
        height = event.size().height()
        mainwidth = width/ncolumns-(xmargin1+xmargin2)
        mainheight = height/nrows-(ymargin1+ymargin2)

        panel = 0
        xpos = np.mod(panel,ncolumns)
        ypos = panel/ncolumns
        self.widget0.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
                  
# This QmainWindow is for the control panel which contains function keys
class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyForm, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.setWindowTitle('Control panel')
        
        widget = self.geometry()
        width = widget.width()
        height = widget.height()
        # re-position the control panel
        self.setGeometry(200, 100, width, height)
        
        # show the plot window
        self.myplot = MyPlot()
        self.myplot.show()
        
        # display simulation info to labels
        self.ui.simuLabel.setText('%dD Warp'%dim)
         
        # emit signal to open directory
        self.connect(self.ui.actionOpen, QtCore.SIGNAL('triggered()' ), self.opendir)
        self.connect(self.ui.actionClose, QtCore.SIGNAL('triggered()' ), self.close)	
        self.ui.quitpushButton.clicked.connect(self.quitpushbutton)

        # time label
        self.ui.tstepLabel.setText("%d" %tstep)
        self.ui.timeLabel.setText("%6.1f fs" % time)

        # timestep stride spin box
        self.ui.stepSpinBox.setValue(1)
        self.ui.stepSpinBox.setMinimum(1)
        
        # backward time button
        self.ui.backwardtimeButton.clicked.connect(self.backwardtimebutton)
        # foward time button
        self.ui.forwardtimeButton.clicked.connect(self.forwardtimebutton)
        
        # timestep slider
        self.ui.timeSlider.setRange(1,tnum)
        self.ui.timeSlider.setSingleStep(1)
        self.ui.timeSlider.setValue(tnum)
        self.ui.timeSlider.valueChanged.connect(self.timeslider)
        
        self.ui.x1min.setText("zmin")
        self.ui.x1minLabel.setText(str("%d" %0 )+"%")
        self.ui.x1minSlider.setRange(0,100)
        self.ui.x1maxSlider.setValue(0)
        self.ui.x1minSlider.valueChanged.connect(self.x1minslider)
        self.ui.x1max.setText("zmax")
        self.ui.x1maxLabel.setText(str("%d" %100)+"%")
        self.ui.x1maxSlider.setRange(0,100)
        self.ui.x1maxSlider.setValue(100)
        self.ui.x1maxSlider.valueChanged.connect(self.x1maxslider)
        
        self.ui.x2min.setText("xmin")
        self.ui.x2minLabel.setText(str("%d" %0)+"%")
        self.ui.x2minSlider.setRange(0,100)
        self.ui.x2maxSlider.setValue(0)
        self.ui.x2max.setText("xmax")
        self.ui.x2minSlider.valueChanged.connect(self.x2minslider)
        self.ui.x2maxLabel.setText(str("%d" %100)+"%")
        self.ui.x2maxSlider.setRange(0,100)
        self.ui.x2maxSlider.setValue(100)
        self.ui.x2maxSlider.valueChanged.connect(self.x2maxslider)

        # field and particle select buttons for three plot panels
        self.ui.fieldButton.setChecked(True)
        QtCore.QObject.connect(self.ui.fieldButton, QtCore.SIGNAL('clicked()'),self.fieldbutton)
        QtCore.QObject.connect(self.ui.particleButton, QtCore.SIGNAL('clicked()'),self.particlebutton)
                
        # fieldcombo-boxes
        for i in np.arange(len(field_list)):
            self.ui.fieldsComboBox.addItem(field_list[i], i)
        self.ui.fieldsComboBox.setCurrentIndex(0)
        self.ui.fieldsComboBox.currentIndexChanged.connect(self.fieldcombobox)
        self.ui.fieldsComboBox.activated.connect(self.fieldcombobox)
		# particle species combo-boxes
        for i in np.arange(len(species_list)):
            self.ui.particlesComboBox.addItem(species_list[i], i)
        self.ui.particlesComboBox.currentIndexChanged.connect(self.particlecombobox)
  	
        # particle phase space combo-boxes
        for i in np.arange(len(phase_list)):
            self.ui.phaseComboBox.addItem(phase_list[i], i)
        self.ui.phaseComboBox.currentIndexChanged.connect(self.phasecombobox)
        
        ## plot PushButton
        self.ui.plotButton.clicked.connect(self.plotbutton)
        
        self.load_parameters()
        
      
    def opendir(self):	
        pass
        
    def close(self):
        sys.exit(0)
        self.myplot.close()
        #if mlab in sys.modules:
        #    mlab.close()
        #    self.ui.close()
    
    def quitpushbutton(self):
        sys.exit(0)
        self.myplot.close()
                    
    def plotbutton(self):
            
        tstep = self.ui.timeSlider.value()
        #if tstep == tstep_previous: # and subsample == subsample2:
        #    self.plotwindow()
        #else:
            #subsample = subsample2
        self.load_parameters()
        
    def load_parameters(self):
        global tstep, time
        global xmin0, xmax0, ymin0, ymax0, zmin0, zmax0   # full simulation domain
        global nx, ny, nz
        global xaxis, yaxis, zaxis
        global tstep_previous
        
        tstep = self.ui.timeSlider.value()
        tstep_previous = tstep
        iteration = iterations[tstep-1]      
       
        fi = h5py.File(file_list[tstep-1], 'r')
        dset=fi['/data/'+str(iteration)]
        time=dset.attrs['time']*1.e15

        dset=fi['/data/'+str(iteration)+'/fields/E']
        dx,dz=dset.attrs["gridSpacing"]
        xmin0,zmin0 = dset.attrs['gridGlobalOffset']
        if coord_system == 'cartesian':
            dset=fi['/data/'+str(iteration)+'/fields/E/x']
            nx,nz = dset.shape
        else:
            dset=fi['/data/'+str(iteration)+'/fields/E/r']
            nt,nx,nz = dset.shape
        xmax0=nx*dx+xmin0
        zmax0=nz*dz+zmin0
        xmin0*=1.e6;xmax0*=1.e6
        zmin0*=1.e6;zmax0*=1.e6
    
        xaxis=1.*np.arange(nx)*(xmax0-xmin0)/nx+xmin0
        zaxis=1.*np.arange(nz)*(zmax0-zmin0)/nz+zmin0

        self.loaddata()
        
    def loaddata(self):  
        selectpanel = 1
        if self.ui.fieldButton.isChecked():
            index=self.ui.fieldsComboBox.currentIndex()
            field = field_list[index]
            field_dic["fld{0}".format(selectpanel)] = field
                             
            fdata["fdata{0}".format(selectpanel)] = self.loadfield(field)

        else: 
            index=self.ui.particlesComboBox.currentIndex()
            species = species_list[index]
            species_dic["spec{0}".format(selectpanel)] = species
            
            xdata["xdata{0}".format(selectpanel)], ydata["ydata{0}".format(selectpanel)], zdata["zdata{0}".format(selectpanel)], \
                uxdata["uxdata{0}".format(selectpanel)], uydata["uydata{0}".format(selectpanel)], \
                uzdata["uzdata{0}".format(selectpanel)], wtdata["wtdata{0}".format(selectpanel)] = self.loadparticle(species, amrlevel)
            fieldpanel_dic["fieldpanel{0}".format(selectpanel)] = False
                        
            index=self.ui.phaseComboBox.currentIndex()
            phase = phase_list[index]
            phase_dic["phase{0}".format(selectpanel)] = phase
            
        
                            
        self.plotwindow()
                
    def loadfield(self, field):
        
        iteration = iterations[tstep-1]
            
        # read data files
        fi = h5py.File(file_list[tstep-1], 'r')
        nx1 = nx
        nz1 = nz
        dset = fi['/data/'+str(iteration)+'/fields/'+field[0]+'/'+field[1]]
        
        
        if coord_system == 'cartesian':
            tempdata = dset[()]
        else:
            tempdata = dset[1,:,:]
           
        return tempdata
        
    def loadparticle(self, species):
        
        iteration = iterations[tstep-1]  
        # read data files
         
        
        fi = h5py.File(file_list[tstep-1], 'r')
        m = mass_dic[species]*me
            
        tempz = fi['data/'+str(iteration)+'/particles/'+species+'/position/z'][()]*1.e6
        tempx = fi['data/'+str(iteration)+'/particles/'+species+'/position/x'][()]*1.e6
        tempux = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/x'][()]/(m*C)
        tempuy = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/y'][()]/(m*C)
        tempuz = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/z'][()]/(m*C)
        tempwt = fi['data/'+str(iteration)+'/particles/'+species+'/weighting'][()]    
        tempy = [0]
        tempx=np.float32(tempx)
        tempz=np.float32(tempz)
        tempwt=np.float32(tempwt)
        tempux=np.float32(tempux)
        tempuy=np.float32(tempuy)
        tempuz=np.float32(tempuz)
             
            
        return tempx, tempy, tempz, tempux, tempuy, tempuz, tempwt
           
    def plotwindow(self):
        
        global x1min0, x2min0, x1max0, x2max0
        global x1axis, x2axis
        global nx1, nx2
        

        if sliceplane == 'xy':
            x1min0 = xmin0; x1max0 = xmax0
            x2min0 = ymin0; x2max0 = ymax0
            x1axis = xaxis; x2axis = yaxis
            nx1 = nx; nx2 = ny
        if sliceplane == 'xz':
        
            x1min0 = zmin0; x1max0 = zmax0
            x2min0 = xmin0; x2max0 = xmax0
            x1axis = zaxis; x2axis = xaxis
            nx1 = nz; nx2 = nx
        if sliceplane == 'yz':
            
            x1min0 = zmin0; x1max0 = zmax0
            x2min0 = ymin0; x2max0 = ymax0
            x1axis = zaxis; x2axis = yaxis
            nx1 = nz; nx2 = ny
       
                 
        ind = 1
        #if fieldpanel_dic['fieldpanel'+str(ind)]:
        self.myplot.widget0.field(dim, coord_system, mass_dic, amass_dic, time, taxis,
                    tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)],
                    x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                    x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom)
        #else:
        #                        
        #   self.myplot.widget0.particle(dim, coord_system, mass_dic, amass_dic, time, taxis,
        #            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
        #            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
        #            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)],
        #            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
        #            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom)
                          
        
if __name__ == "__main__":
    
    try:
        app = QtGui.QApplication(sys.argv)
    except RuntimeError:
        app = QtCore.QCoreApplication.instance()
        
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())
