import numpy as np

import matplotlib
matplotlib.rcParams['backend.qt4']='PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Import numba functions
from cic_histogram import histogram_cic_1d, particle_energy
try:
    #Use cython openmp multi-thread
    from histogram_threading import histogram_cic_2d
    print("use cython parallel")
except ImportError:
    from cic_histogram import histogram_cic_2d

class MatplotlibWidget(Canvas):

    def __init__(self, parent=None):		
        super(MatplotlibWidget, self).__init__(Figure())
        self.setParent(parent)
        self.figure = Figure(dpi=60)
        self.canvas = Canvas(self.figure)
        self.figure.set_facecolor('0.85')
        
    def field(self, dim, coord_system, mass_dic, amass_dic, time, taxis, 
                tstep, fdata, field,
                x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom):
                
        self.figure.clear()
        
        matplotlib.rc('xtick', labelsize=16) 
        matplotlib.rc('ytick', labelsize=16)

        cbarwidth = 0.16
        nbin = 200
        fontsize0 = 15; fontsize1 = 15
        
        #sliceplane = kwargs['sliceplane']    
        
        # zoom-in/out
        x1min = x1min0+(x1max0-x1min0)*x1min_zoom/100  
        x1max = x1min0+(x1max0-x1min0)*x1max_zoom/100  
        x2min = x2min0+(x2max0-x2min0)*x2min_zoom/100  
        x2max = x2min0+(x2max0-x2min0)*x2max_zoom/100
        
        nx = len(x1axis); ny = len(x2axis)
                
        # zoom-in/out indices
        iL = np.where(x1axis >= x1min)[0]
        if len(iL) != 0: iL=iL[0]
        else: iL = 0
        iR = np.where(x1axis >= x1max)[0]
        if len(iR) != 0: iR=iR[0]
        else: iR = nx-1
        jL = np.where(x2axis >= x2min)[0]
        if len(jL) != 0: jL=jL[0]
        else: jL = 0
        jR = np.where(x2axis >= x2max)[0]
        if len(jR) != 0: jR=jR[0]
        else: jR = ny-1
        amrlevel =0
        # multply the AMR level factor
        iL = iL*2**amrlevel; iR = iR*2**amrlevel
        jL = jL*2**amrlevel; jR = jR*2**amrlevel

        
        #if amrlevel !=0 and amrgrid:
        #    xleftedge0 = amredges[0]
        #    xrightedge0 = amredges[1]
        #    yleftedge0 = amredges[2]
        #    yrightedge0 = amredges[3]

        self.plot = self.figure.add_subplot(111)
            
        sliceplane = 'xz'
        
        if sliceplane == 'xy':
            xtitle = 'x ($\mu$m)'; ytitle = 'y ($\mu$m)'
            kxtitle = 'kx (1/$\mu$m)'; kytitle = 'ky (1/$\mu$m)'
        if sliceplane == 'xz':
            xtitle = 'z ($\mu$m)'; ytitle = 'x ($\mu$m)'
            kxtitle = 'kz (1/$\mu$m)'; kytitle = 'kx (1/$\mu$m)'
        if sliceplane == 'yz':
            xtitle = 'z ($\mu$m)'; ytitle = 'y ($\mu$m)'
            kxtitle = 'kz (1/$\mu$m)'; kytitle = 'ky (1/$\mu$m)'
        
        
        vmin = np.min(fdata[jL:jR,iL:iR])
        vmax = np.max(fdata[jL:jR,iL:iR])
        self.plot.cla()
        #logthresh = 1
        im =  self.plot.imshow(fdata[jL:jR,iL:iR], interpolation='nearest', cmap='jet',
                origin='lower', extent=[ x1min,x1max,x2min,x2max ])
                #norm=matplotlib.colors.SymLogNorm(10**-logthresh) )
        self.plot.axes.set_xlim([x1min,x1max])
        self.plot.axes.set_ylim([x2min,x2max])
        ax = self.figure.gca()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size=cbarwidth, pad=0.)
        cb = self.figure.colorbar(im, cax=cax)
        self.plot.set_title(field, fontsize=fontsize0)
       
        self.plot.set_xlabel(xtitle, fontsize=fontsize1)
        self.plot.set_ylabel(ytitle, fontsize=fontsize1)
             
        self.plot.axes.get_figure().tight_layout()
        self.draw()