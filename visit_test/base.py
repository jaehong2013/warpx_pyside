# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'base.ui'
#
# Created: Tue Feb  6 00:06:42 2018
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.openpushButton = QtGui.QPushButton(self.centralwidget)
        self.openpushButton.setGeometry(QtCore.QRect(160, 80, 110, 32))
        self.openpushButton.setObjectName("openpushButton")
        self.closepushButton = QtGui.QPushButton(self.centralwidget)
        self.closepushButton.setGeometry(QtCore.QRect(160, 120, 110, 32))
        self.closepushButton.setObjectName("closepushButton")
        self.paraopenpush = QtGui.QPushButton(self.centralwidget)
        self.paraopenpush.setGeometry(QtCore.QRect(280, 80, 110, 32))
        self.paraopenpush.setObjectName("paraopenpush")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar()
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.openpushButton.setText(QtGui.QApplication.translate("MainWindow", "Open", None, QtGui.QApplication.UnicodeUTF8))
        self.closepushButton.setText(QtGui.QApplication.translate("MainWindow", "Close", None, QtGui.QApplication.UnicodeUTF8))
        self.paraopenpush.setText(QtGui.QApplication.translate("MainWindow", "parallel open", None, QtGui.QApplication.UnicodeUTF8))

