import sys
import os

from base import *
#sys.path.append('/Applications/VisIt.app/Contents/Resources/2.11.0/darwin-x86_64/lib/site-packages'
from visit import *

class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyForm, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.setWindowTitle('Control panel')
        
        widget = self.geometry()
        width = widget.width()
        height = widget.height()

        self.ui.openpushButton.clicked.connect(self.openpushbutton)
        self.ui.closepushButton.clicked.connect(self.closepushbutton)
        self.ui.paraopenpush.clicked.connect(self.paraopenpush)
        
        
    def openpushbutton(self):
        
        try:
            Launch()
        except VisItException:
            DeleteAllPlots()
        
        dir = './'
        #dir = '/Users/jaehongpark/running_warpx/cho_data/'
        iteration=200
        field = 'Ez'
        #OpenDatabase(dir+"plt"+str("%5.5d"%iteration)+"/Header")
    
        OpenDatabase(dir+"plt"+str("%5.5d"%iteration)+"/Header")

        AddPlot("Volume", field)
        c = GaussianControlPointList()
        p = GaussianControlPoint()
        
        factor = 6
        p.x      = 0.1
        p.height = 0.1*factor
        p.width  = 0.05
        c.AddControlPoints(p)
        p.x      = 0.9
        p.height = 0.1*factor
        p.width  = 0.05
        c.AddControlPoints(p)
        p.x      = 0.3
        p.height = 0.1*factor
        p.width  = 0.05
        c.AddControlPoints(p)
        p.x      = 0.7
        p.height = 0.1*factor
        p.width  = 0.05
        c.AddControlPoints(p)
        p.x      = 0.5
        p.height = 0.005*factor
        p.width  = 0.1
        c.AddControlPoints(p)

        v=VolumeAttributes()
        v.opacityAttenuation=0.2
        v.opacityMode = v.GaussianMode
        v.SetOpacityControlPoints(c)
        SetPlotOptions(v)
        DrawPlots()

    

        """
        AddPlot("Volume", field)
        c = GaussianControlPointList()
        p = GaussianControlPoint()
        p.x      = 0.3
        p.height = 0.4
        p.width  = 0.1
        c.AddControlPoints(p)
        p.x      = 0.7
        p.height = 0.8
        p.width  = 0.2
        c.AddControlPoints(p)
        v=VolumeAttributes()
        v.opacityMode = v.GaussianMode
        v.SetOpacityControlPoints(c)
        InvertBackgroundColor()
        SetPlotOptions(v)
        
        DrawPlots()
        """
        
        """
        AddPlot("Pseudocolor", "Ez")
        iso_atts = IsosurfaceAttributes()
        iso_atts.contourMethod = iso_atts.Value
        iso_atts.variable = "Ez"
        AddOperator("Isosurface")
        DrawPlots()
        """
        
    def paraopenpush(self):
        
        try:
            Launch()
        except VisItException:
            pass
        
        dir = './'
        #dir = '/Users/jaehongpark/running_warpx/cho_data/'
        iteration=7500
        field = 'Ez'
        #OpenDatabase(dir+"plt"+str("%5.5d"%iteration)+"/Header")
        fname=dir+"plt"+str("%5.5d"%iteration)+"/Header"
        AddPlot("Volume", field)
        
        file = open('visit_parallel.py','w') 

        file.write('# python script\n')
        file.write('OpenDatabase("'+fname+'")\n') 
        file.write('AddPlot("Volume", '+'"'+field+'")\n')
        file.write('c = GaussianControlPointList()\n')
        file.write('p = GaussianControlPoint()\n')
        file.write('p.x      = 0.3\n')
        file.write('p.height = 0.4\n')
        file.write('p.width  = 0.1\n')
        file.write('c.AddControlPoints(p)\n')
        file.write('p.x      = 0.7\n')
        file.write('p.height = 0.8\n')
        file.write('p.width  = 0.2\n')
        file.write('c.AddControlPoints(p)\n')
        file.write('v=VolumeAttributes()\n')
        file.write('v.opacityMode = v.GaussianMode\n')
        file.write('v.SetOpacityControlPoints(c)\n')
        file.write('SetPlotOptions(v)\n')
        file.write('DrawPlots()\n')
        #file.write('AddPlot("Pseudocolor", "Ez")\n')
        #file.write('iso_atts = IsosurfaceAttributes()\n') 
        #file.write('iso_atts.contourMethod = iso_atts.Value\n') 
        #file.write('iso_atts.variable = "temp"\n')
        #file.write('AddOperator("Isosurface")\n')
        #file.write('DrawPlots()\n')

        file.close()
        
        os.system('visit -np 4 -cli  -s visit_parallel.py')
            
        
    def closepushbutton(self):
        
        DeleteAllPlots()
        
if __name__ == "__main__":
    
    try:
        app = QtGui.QApplication(sys.argv)
    except RuntimeError:
        app = QtCore.QCoreApplication.instance()
        
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())
