# python script
OpenDatabase("plt07500/Header")
AddPlot("Pseudocolor", "Ez")
iso_atts = IsosurfaceAttributes()
iso_atts.contourMethod = iso_atts.Value
iso_atts.variable = "temp"
AddOperator("Isosurface")
DrawPlots()
