#!/usr/bin/env python2.7
"""
warpx_analysis.py

by Jaehong Park (JaehongPark@lbl.gov)

This code provides a PySide based visualization tool
for the Warp/WarpX data analysis.

##########################################
###### Install python libraries     ######
##########################################
(1) You should have python 2.7:
We recommend python from Anaconda (http://docs.continuum.io/anaconda/install)
If you do not have such python libraries, install
use conda install or pip install:
 >> conda install h5py
 >> conda install matplotlib
 >> pip install numpy

(2) Install yt
 >> conda install -c conda-forge yt
http://yt-project.org 

##########################################
###### Install PySide               ######
##########################################
(3) Install PySdie via conda install
 >> conda install -c conda-forge pyside

*** For Nersc or other cluster users *** You may install PySide in the following way:
 download miniconda2
>> wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
 install miniconda2
>> bash Miniconda2-latest-Linux-x86_64.sh
 activate miniconda2
>> source miniconda2/bin/activate
 finally, install PySide using conda,
>> conda install -c conda-forge pyside

###############################################
###### Download warpx_analysis         ########
###############################################
Download the warpx_analysis packages from the git repository,
(8) git clone https://jaehong2013@bitbucket.org/jaehong2013/warpx_pyside.git
and go to the folder where your simulation data files are saved, and type
(9) python ~/warpx_pyside/warpx_analysis.py


###############################################
###### Compile Cython parallel
###############################################
This is for open-mp parallel particle reduction (histogram, energy, etc..)
Go to the folder, warpx_pyside/
and run setup.py file
>> python setup.py build_ext --inplace


###############################################
###### Install VisIt for 3D volume     ########
###############################################
install visit:
https://wci.llnl.gov/simulation/computer-codes/visit/

This is warpx boxlib data only, for warp openpmd data, install OpenPMD plugin for VisIt:
https://bitbucket.org/remilehe/openpmd-visit-plugin

For OsX Mac users, set in your .bash_profile:
export PATH="/Users/jaehongpark/anaconda2/bin:$PATH"
export PATH="/Applications/VisIt.app/Contents/Resources/bin:$PATH"
export DYLD_LIBRARY_PATH=/Users/jaehongpark/anaconda2/lib/python2.7/site-packages/PySide:/Applications/VisIt.app/Contents/Resources/2.12.3/darwin-x86_64/lib
export PYTHONPATH=$PYTHONPATH:/Applications/VisIt.app/Contents/Resources/2.12.3/darwin-x86_64/lib/site-packages
alias xml2cmake="/Applications/VisIt.app/Contents/Resources/bin/xml2cmake"
alias visit="/Applications/VisIt.app/Contents/Resources/bin/visit"


Last updates:
9/2017
8/2017: 
multi-window panels
add yt volume renering
3/2018
add visit
cython openmp particle data reduction
"""
import sys, glob

import time as tm
import basewindow
from base import *
try:
    import visit
    visitloaded = True
except ImportError:
    visitloaded = False
from matplotwindow import *
try:
    import mpi4py
    size = mpi4py.MPI.COMM_WORLD.Get_size()
    mpiparallel = True
except AttributeError:
    mpiparallel = False
except ImportError:
    mpiparallel = False

try:
    import yt
    from yt.funcs import mylog
    mylog.setLevel(0)
except:
    print('no yt installed and need yt for WarpX visulatiztion')
import h5py
import numpy as np
from scipy import ndimage
#from opmd_viewer import OpenPMDTimeSeries
import os
import threading

import read_raw_data

import matplotlib
matplotlib.rcParams['backend.qt4']='PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as Canvas
   
if mpiparallel:  
    from mpi4py import MPI
    size = MPI.COMM_WORLD.Get_size()
    rank = MPI.COMM_WORLD.Get_rank()

debug = False
warpx = False
labwarpx = False
warp = False
 
# SI unit
C = 2.99792458e8 # light speed
me = 9.10938291e-31 # electron mass
qe = 1.60217657e-19 # electron charge

# check if the data file is from WarpX or Warp
file_path = '.'
file_list = glob.glob(file_path + '/plt?????')
if len(file_list) != 0:
    iterations = [ int(file_name[len(file_name)-5:]) for file_name in file_list ]
    warpx = True
    lab_list = glob.glob(file_path + '/lab_frame_data/snapshot?????')
    if len(lab_list) != 0:
        lab_list.sort()
        tnum2 = len(lab_list)
        labwarpx = True
else:
    file_list = glob.glob(file_path + '/data????????.h5')
    iterations = [ int(file_name[len(file_name)-11:len(file_name)-3]) for file_name in file_list ]
    warp = True
    #ts = OpenPMDTimeSeries(file_path,check_all_files=False )

file_list.sort()
iterations.sort()
tnum = len(iterations)
tstep = tnum
iteration = iterations[tstep-1]

mass_dic = {}   # mass dictionary
charge_dic = {} # charge dictionary
amass_dic = {} # atomic mass number dictionary
# find particle species

if debug: start_time = tm.time()

if warpx:
    ds = yt.load(file_path + '/plt%05d/'%(iteration))
    ds.index
    species_list_all=ds.particle_types
    species_list = []
    for species in species_list_all:
        if species[0] != 'a':
            try:
                q = ds.parameters[species+'_charge'].d
                m = ds.parameters[species+'_mass'].d
            except AttributeError:
                q = ds.parameters[species+'_charge']
                m = ds.parameters[species+'_mass']
            if m<1:
                species_list.append(species)
                mass_dic[species] = m/me     # mass ratio
                charge_dic[species] = q/qe   # charge ratio
                if q > 0: amass_dic[species] = m/(me*1836)  # atomic mass number
                else: amass_dic[species] = 1.
    nspecies = len(species_list)

elif warp: # warp
    fi = h5py.File(file_list[tstep-1], 'r')
    dset= fi['data/'+str(iteration)+'/particles']
    species_list = dset.keys()
    nspecies = len(species_list)
     
    for species in species_list:
        q = dset[species+'/charge'].attrs['value']
        m = dset[species+'/mass'].attrs['value']
        mass_dic[species] = m/me     # mass ratio
        charge_dic[species] = q/qe   # charge ratio
        if q > 0: amass_dic[species] = m/(me*1836)  # atomic mass number
        else: amass_dic[species] = 1.

print('e-ion mass ratio = ', mass_dic)
print('charge ratio (atomic number) = ', charge_dic)
print('atomic mass number = ', amass_dic)    

if debug: print("---load particle species %s seconds ---" % (tm.time() - start_time))

# get times
if warpx:
    taxis=np.zeros(tnum)
    s=0
    for files in file_list:
        ds = yt.load(files)
        time=ds.current_time.d*1.e15
        taxis[s]=time
        s+=1
            
    if labwarpx:
        s=0
        taxis2=np.zeros(tnum2)
        for files in lab_list:
            header = file_path + '/lab_frame_data/Header'
            dummy, info = read_raw_data.read_lab_snapshot(files, header)
            taxis2[s] = info['t_snapshot']
            s+=1

elif warp:
    taxis=np.zeros(tnum)
    s=0
    for files in file_list:
        fi = h5py.File(files,'r')
        dset=fi['/data/'+str(iterations[s])]
        time=dset.attrs['time']*1.e15
        taxis[s]=time
        s+=1

lowreson = False
stride = 20

# find if the coordinate system is 'Cartesian or cylindrical
if warpx:
    if ds.parameters['geometry.coord_sys'] == '0':
        coord_system = 'cartesian'
    else:
        coord_system = 'cylindrical'
elif warp:
    item = fi['data/'+str(iteration)+'/fields/E'].attrs['geometry']
    if item == 'cartesian':
        coord_system = 'cartesian'
    else:
        coord_system = 'cylindrical'
        
# check the dimensionality 
if warpx:
    if ds.domain_dimensions[2] > 1:
        dim = 3
    elif ds.domain_dimensions[1] == 1:
        dim = 1
    else: 
        dim = 2
elif warp:
    item = fi['data/'+str(iteration)+'/fields/E'].attrs['axisLabels']
    dim = len(item)
            
# find fields
field_list = []
if warpx:
    field_list_all = ds.field_list
    nfields_all = len(field_list_all)
    for i in np.arange(nfields_all):
        if field_list_all[i][0] == 'boxlib':
            field_list.append(field_list_all[i][1])
    
elif warp:                
    item = fi['data/'+str(iteration)+'/fields']
    fields_group = item.items()
    nfields = len(fields_group)
        
    item = fi['data/'+str(iteration)+'/fields/E']
    coord_group = item.items()
    ncoord = len(coord_group)
    coord_list = []
    for i in np.arange(ncoord):
        coord_list.append(coord_group[i][0])
            
    for i in np.arange(nfields):
        for j in np.arange(ncoord):
            if fields_group[i][0] != 'rho':
                field_list.append(fields_group[i][0]+coord_list[j])
            else:
                field_list.append(fields_group[i][0])
                break

# list phase space variables
phase_list = []
if warpx:
    phase_list.append('density')
    phase_list.append('fE-loglin')
    phase_list.append('fE-loglog')
    phase_list.append('px-x')
    phase_list.append('py-x')
    phase_list.append('pz-x')
    phase_list.append('ene-x')
    phase_list.append('py-px')
    phase_list.append('pz-px')
    phase_list.append('pz-py')
    phase_list.append('ene-theta')
elif warp:
    phase_list.append('density')
    phase_list.append('fE-loglin')
    phase_list.append('fE-loglog')
    phase_list.append('px-z')
    phase_list.append('py-z')
    phase_list.append('pz-z')
    phase_list.append('ene-z')
    phase_list.append('px-pz')
    phase_list.append('py-pz')
    phase_list.append('py-px')
    phase_list.append('ene-theta')    

# list local phase space variables
localfldphase_list = []
localfldphase_list.append('density')
localfldphase_list.append('FFT')
localfldphase_list.append('intensity')
localfldphase_list.append('energy')

localprtlphase_list = []
localprtlphase_list.append('density')
localprtlphase_list.append('1d-dens')
localprtlphase_list.append('fE-loglin')
localprtlphase_list.append('fE-loglog')
if warpx:
    localprtlphase_list.append('px-x')
    localprtlphase_list.append('py-x')
    localprtlphase_list.append('pz-x')
    localprtlphase_list.append('ene-x')
    localprtlphase_list.append('py-px')
    localprtlphase_list.append('pz-px')
    localprtlphase_list.append('pz-py')
elif warp:
    localprtlphase_list.append('px-z')
    localprtlphase_list.append('py-z')
    localprtlphase_list.append('pz-z')
    localprtlphase_list.append('ene-z')
    localprtlphase_list.append('px-pz')
    localprtlphase_list.append('py-pz')
    localprtlphase_list.append('py-px')
localprtlphase_list.append('ene-theta')
#localprtlphase_list.append('MaxE')
#localprtlphase_list.append('totalE')
#localprtlphase_list.append('getdata')

visitmenu_list = []
visitmenu_list.append('isocontour')
visitmenu_list.append('volume')

x1min_zoom = 0; x1max_zoom = 100
x2min_zoom = 0; x2max_zoom = 100
# default values of global variables
#if check_field:
#    field = field_list[0]
#species = species_list[0]
#phase = phase_list[0]
#localfield= localfldphase_list[0]
#localparticle = localprtlphase_list[0]

animation = False
oned = False
aspect = 'auto'
#count = 0
#xL_loc=xR_loc=yL_loc=yR_loc=0
lineselect = False
tempfit = 0; temp = 0.; norm = 1.0
p1min = 0; p2min = 0
p1max = 100; p2max = 100
contrast = 100
# mesh refinement level; 0,1,. ..
amrlevel = 0
#amrgrid = 0

#amredges = [0,0,0,0]
saveimg = 0

if warpx:
    if dim == 3:
        sliceplane = 'xz'
    else:
        sliceplane = 'xy'
elif warp:
    sliceplane = 'xz'
xsliceloc = 50
ysliceloc = 50
zsliceloc = 50

i_press = 0
i_release = 0

#number of panels
nrows = 1; ncolumns = 1
#select panel
selectpanel = 1
numpanel = nrows*ncolumns
numrowpanel = 1; numcolumnpanel =1

# field data dictionary
# eg. fdata = {'panel_1': [....], 'panel_2': [...], ...}
# The dictionary keyword is a data name in each window panel
global fdata
fdata = {}
# field name dictionary
global field_dic
field_dic = {}
# particle data dictionary
global xdata, ydata, zdata, uxdata, uydata, uzdata, wtdata
xdata = {}; ydata = {}; zdata = {}
uxdata = {}; uydata = {}; uzdata = {}; wtdata = {}
# species name dictionary
global species_dic
species_dic = {}
# particle phase name dictionary
global phase_dic
phase_dic = {}
# local field name dictionary
global localfldphase_dic
localfldphase_dic = {}
for ind in np.arange(10):
    localfldphase_dic["localfldphase{0}".format(ind+1)] = localfldphase_list[0]
# local particle phase name dictionary
global localprtlphase_dic
localprtlphase_dic = {}

global count_dic
count_dic = {}
for ind in np.arange(10):
    count_dic["count{0}".format(ind+1)] = 0
    
global lineselect_dic
lineselect_dic = {}
for ind in np.arange(10):
    lineselect_dic["lineselect{0}".format(ind+1)] = False
global rectselect_dic
rectselect_dic = {}
for ind in np.arange(10):
    rectselect_dic["rectselect{0}".format(ind+1)] = False
global xL_loc_dic, xR_loc_dic, yL_loc_dic, yR_loc_dic
xL_loc_dic = {}; xR_loc_dic = {}
yL_loc_dic = {}; yR_loc_dic = {}
for ind in np.arange(10):
    xL_loc_dic["xL_loc{0}".format(ind+1)] = 0.
    xR_loc_dic["xR_loc{0}".format(ind+1)] = 0.
    yL_loc_dic["yL_loc{0}".format(ind+1)] = 0.
    yR_loc_dic["yR_loc{0}".format(ind+1)] = 0.

global amrlevel_dic
amrlevel_dic={}
for ind in np.arange(10):
    amrlevel_dic["amrlevel{0}".format(ind+1)] = 0

# check whether a window panel is plotting field or particle
global fieldpanel_dic
fieldpanel_dic = {}
for ind in np.arange(10):
    fieldpanel_dic['fieldpanel{0}'.format(ind+1)] = True

xsize0 = 550 #804
ysize0 = 350 #396
xmargin1 = 10; xmargin2 = 10
ymargin1 = 5; ymargin2 = 6

newpanel = False
syncpanel = False

# control multipanel
doload = True
doload2 = True
doload3 = True
doplot = True
doplot2 = True
doplot3 = True

visit_ind = 0
#visit_processors = 1
#plotwincount = 0 

# This QmainWindow embeds matplotlib widgets
class MyPlot(QtGui.QMainWindow):
    def __init__(self, parent=None):
                
        super(MyPlot, self).__init__(parent)
        self.ui = basewindow.Ui_MainWindow()
        self.ui.setupUi(self)
        
        # set mainwindow title as the current directory
        self.setWindowTitle(os.getcwd())
        
        plotwidget = self.geometry()
        # original window panel size
        #xsize0 = plotwidget.width()
        #ysize0 = plotwidget.height()
        
        # resize the window panel
        xsize1 = ncolumns*(xsize0/ncolumns**0.)
        ysize1 = nrows*(ysize0/nrows**0.)
        self.resize(xsize1, ysize1)

        # default margins for the plot panels
        #<---       xsize0       ------->
        #-------------------------------| ^
        #            y1                 | |
        #       |---------------|       | |
        #       |               |       |
        #<-x1-> |               |<- x2->|ysize0	
        #       |   plot panels |       |	
        #       |               |       | |	
        #       |---------------|       | |
        #               y2              | |
        #-------------------------------| v
        # size of each main plot panel
        mainwidth = xsize1/ncolumns-(xmargin1+xmargin2)
        mainheight = ysize1/nrows-(ymargin1+ymargin2)
        npanels = nrows*ncolumns
        print('npanels=',npanels)
        if npanels >= 1:
            panel = 0
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget0 = MatplotlibWidget(self.ui.centralwidget)
            self.widget0.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget0.setObjectName("widget")

        if npanels >= 2:
            panel = 1
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget1 = MatplotlibWidget(self.ui.centralwidget)
            self.widget1.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget1.setObjectName("widget")
        if npanels >= 3:
            panel = 2
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget2 = MatplotlibWidget(self.ui.centralwidget)
            self.widget2.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget2.setObjectName("widget")
        if npanels >= 4:
            panel = 3
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget3 = MatplotlibWidget(self.ui.centralwidget)
            self.widget3.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget3.setObjectName("widget")
        if npanels >= 5:
            panel = 4
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget4 = MatplotlibWidget(self.ui.centralwidget)
            self.widget4.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget4.setObjectName("widget")
        if npanels >= 6:
            panel = 5
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget5 = MatplotlibWidget(self.ui.centralwidget)
            self.widget5.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget5.setObjectName("widget")
        if npanels >= 7:
            panel = 6
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget6 = MatplotlibWidget(self.ui.centralwidget)
            self.widget6.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget6.setObjectName("widget")
        if npanels >= 8:
            panel = 7
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget7 = MatplotlibWidget(self.ui.centralwidget)
            self.widget7.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget7.setObjectName("widget")
        if npanels >= 9:
            panel = 8
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget8 = MatplotlibWidget(self.ui.centralwidget)
            self.widget8.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget8.setObjectName("widget")
        if npanels >= 10:
            panel = 9
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget9 = MatplotlibWidget(self.ui.centralwidget)
            self.widget9.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                ymargin1+ypos*mainheight, mainwidth, mainheight))
            self.widget9.setObjectName("widget")
             
    def resizeEvent(self,  event):
        
        width = event.size().width()
        height = event.size().height()
        mainwidth = width/ncolumns-(xmargin1+xmargin2)
        mainheight = height/nrows-(ymargin1+ymargin2)
        
        if nrows*ncolumns >= 1:
            panel = 0
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget0.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
        if nrows*ncolumns >= 2:
            panel = 1
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget1.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
        if nrows*ncolumns >= 3:
            panel = 2
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget2.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))       
        if nrows*ncolumns >= 4:
            panel = 3
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget3.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
        if nrows*ncolumns >= 5:
            panel = 4
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget4.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
        if nrows*ncolumns >= 6:
            panel = 5
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget5.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
        if nrows*ncolumns >= 7:
            panel = 6
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget6.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
        if nrows*ncolumns >= 8:
            panel = 7
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget7.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
        if nrows*ncolumns >= 9:
            panel = 8
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget8.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
        if nrows*ncolumns >= 10:
            panel = 9
            xpos = np.mod(panel,ncolumns)
            ypos = panel/ncolumns
            self.widget9.setGeometry(QtCore.QRect(xmargin1+xpos*mainwidth, 
                    ymargin1+ypos*mainheight, mainwidth, mainheight))
                  
# This QmainWindow is for the control panel which contains function keys
class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyForm, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.setWindowTitle('Control panel')
        
        widget = self.geometry()
        width = widget.width()
        height = widget.height()
        # re-position the control panel
        self.setGeometry(200, 100, width, height)
        
        # show the plot window
        self.myplot = MyPlot()
        self.myplot.show()
        
        # display simulation info to labels
        if warpx:
            self.ui.simuLabel.setText('%dD WarpX'%dim)
        elif warp:
            self.ui.simuLabel.setText('%dD Warp'%dim)
        if coord_system == 'cylindrical':
            self.ui.coordinateLabel.setText('cylindrical')
        elif coord_system == 'cartesian':
            self.ui.coordinateLabel.setText('Cartesian')
        
        # emit signal to open directory
        self.connect(self.ui.actionOpen, QtCore.SIGNAL('triggered()' ), self.opendir)
        self.connect(self.ui.actionClose, QtCore.SIGNAL('triggered()' ), self.close)	
        self.ui.quitpushButton.clicked.connect(self.quitpushbutton)
        
        # (x,y) coordinate labeli
        self.ui.coordLabel.setText(str('(0,0)'))

        # row panel number spin box
        self.ui.rowpanelSpinBox.setValue(1)
        self.ui.rowpanelSpinBox.setMinimum(1)
        self.ui.rowpanelSpinBox.setMaximum(5)
        
        # column panel number spin box
        self.ui.columnpanelSpinBox.setValue(1)
        self.ui.columnpanelSpinBox.setMinimum(1)
        self.ui.columnpanelSpinBox.setMaximum(2)
        
        # select panel check box
        self.ui.panelButton1.setChecked(True)
        QtCore.QObject.connect(self.ui.panelButton1, QtCore.SIGNAL('clicked()'),self.panelbutton)
        QtCore.QObject.connect(self.ui.panelButton2, QtCore.SIGNAL('clicked()'),self.panelbutton)
        QtCore.QObject.connect(self.ui.panelButton3, QtCore.SIGNAL('clicked()'),self.panelbutton)
        QtCore.QObject.connect(self.ui.panelButton4, QtCore.SIGNAL('clicked()'),self.panelbutton)
        QtCore.QObject.connect(self.ui.panelButton5, QtCore.SIGNAL('clicked()'),self.panelbutton)
        QtCore.QObject.connect(self.ui.panelButton6, QtCore.SIGNAL('clicked()'),self.panelbutton)
        QtCore.QObject.connect(self.ui.panelButton7, QtCore.SIGNAL('clicked()'),self.panelbutton)
        QtCore.QObject.connect(self.ui.panelButton8, QtCore.SIGNAL('clicked()'),self.panelbutton)
        QtCore.QObject.connect(self.ui.panelButton9, QtCore.SIGNAL('clicked()'),self.panelbutton)
        QtCore.QObject.connect(self.ui.panelButton10, QtCore.SIGNAL('clicked()'),self.panelbutton)
        
        self.ui.panelButton1.setStyleSheet("background-color: rgb(255, 140, 0);")
        
        self.ui.syncCheckBox.setChecked(False)
        self.ui.syncCheckBox.clicked.connect(self.synccheckbox)
        
        self.ui.lowresCheckBox.setChecked(False)
        self.ui.lowresCheckBox.clicked.connect(self.lowrescheckbox)

        self.ui.labframeCheckBox.setChecked(False)
        self.ui.labframeCheckBox.clicked.connect(self.labframecheckbox)

        # time label
        self.ui.tstepLabel.setText("%d" %tstep)
        self.ui.timeLabel.setText("%6.1f fs" % time)

        # timestep stride spin box
        self.ui.stepSpinBox.setValue(1)
        self.ui.stepSpinBox.setMinimum(1)
        
        # particle subsampling
        #self.ui.subsampleSpinBox.setMinimum(1)
        #self.ui.subsampleSpinBox.setMaximum(1000)
        #self.ui.subsampleSpinBox.setValue(subsample)
        
        # backward time button
        self.ui.backwardtimeButton.clicked.connect(self.backwardtimebutton)
        # foward time button
        self.ui.forwardtimeButton.clicked.connect(self.forwardtimebutton)
        
        # timestep slider
        self.ui.timeSlider.setRange(1,tnum)
        self.ui.timeSlider.setSingleStep(1)
        self.ui.timeSlider.setValue(tnum)
        self.ui.timeSlider.valueChanged.connect(self.timeslider)
        
        if warpx and dim == 2:
            self.ui.x1min.setText("xmin")
        elif warp:
            self.ui.x1min.setText("zmin")
        self.ui.x1minLabel.setText(str("%d" %0 )+"%")
        self.ui.x1minSlider.setRange(0,100)
        self.ui.x1maxSlider.setValue(0)
        self.ui.x1minSlider.valueChanged.connect(self.x1minslider)
        if warpx and dim == 2:
            self.ui.x1max.setText("xmax")
        else:
            self.ui.x1max.setText("zmax")
        self.ui.x1maxLabel.setText(str("%d" %100)+"%")
        self.ui.x1maxSlider.setRange(0,100)
        self.ui.x1maxSlider.setValue(100)
        self.ui.x1maxSlider.valueChanged.connect(self.x1maxslider)
        
        if warpx and dim == 2:
            self.ui.x2min.setText("ymin")
        else:
            self.ui.x2min.setText("xmin")
        self.ui.x2minLabel.setText(str("%d" %0)+"%")
        self.ui.x2minSlider.setRange(0,100)
        self.ui.x2maxSlider.setValue(0)
        if warpx and dim == 2:
            self.ui.x2max.setText("ymax")
        else:
            self.ui.x2max.setText("xmax")
        self.ui.x2minSlider.valueChanged.connect(self.x2minslider)
        self.ui.x2maxLabel.setText(str("%d" %100)+"%")
        self.ui.x2maxSlider.setRange(0,100)
        self.ui.x2maxSlider.setValue(100)
        self.ui.x2maxSlider.valueChanged.connect(self.x2maxslider)

        # 2D slices
        if warpx:
            if dim == 3:
                self.ui.xzButton.setChecked(True)
            else:
                self.ui.xyButton.setChecked(True)
        else:
            self.ui.xzButton.setChecked(True)
        QtCore.QObject.connect(self.ui.xyButton, QtCore.SIGNAL('clicked()'),self.slicebutton)
        QtCore.QObject.connect(self.ui.xzButton, QtCore.SIGNAL('clicked()'),self.slicebutton)
        QtCore.QObject.connect(self.ui.yzButton, QtCore.SIGNAL('clicked()'),self.slicebutton)
        
        # slice value slider
        self.ui.slicevalueSlider.setRange(0,99)
        self.ui.slicevalueSlider.setValue(50)
        self.ui.slicevalueSlider.valueChanged.connect(self.slicevalueslider)
                     
        self.ui.centerpushButton.clicked.connect(self.centerpushbutton)
   
        self.ui.strideSlider.setRange(1,100)
        self.ui.strideSlider.setValue(stride)
        self.ui.strideLabel.setText(str("%2d%%" %stride))
        self.ui.strideSlider.valueChanged.connect(self.strideslider)
   
        # field and particle select buttons for three plot panels
        self.ui.fieldButton.setChecked(True)
        QtCore.QObject.connect(self.ui.fieldButton, QtCore.SIGNAL('clicked()'),self.fieldbutton)
        QtCore.QObject.connect(self.ui.particleButton, QtCore.SIGNAL('clicked()'),self.particlebutton)
                
        # fieldcombo-boxes
        for i in np.arange(len(field_list)):
            self.ui.fieldsComboBox.addItem(field_list[i], i)
        self.ui.fieldsComboBox.setCurrentIndex(0)
        #self.ui.fieldsComboBox.currentIndexChanged.connect(self.fieldcombobox)
        self.ui.fieldsComboBox.activated.connect(self.fieldcombobox)
		# particle species combo-boxes
        for i in np.arange(len(species_list)):
            self.ui.particlesComboBox.addItem(species_list[i], i)
        self.ui.particlesComboBox.currentIndexChanged.connect(self.particlecombobox)
  	
        # particle phase space combo-boxes
        for i in np.arange(len(phase_list)):
            self.ui.phaseComboBox.addItem(phase_list[i], i)
        self.ui.phaseComboBox.currentIndexChanged.connect(self.phasecombobox)
        
        # local field combo-boxes
        for i in np.arange(len(localfldphase_list)):
            self.ui.localfieldComboBox.addItem(localfldphase_list[i], i)
        self.ui.localfieldComboBox.currentIndexChanged.connect(self.localfieldcombobox)
        # local particle combo-boxes       
        for i in np.arange(len(localprtlphase_list)):
            self.ui.localparticleComboBox.addItem(localprtlphase_list[i], i)    
        self.ui.localparticleComboBox.currentIndexChanged.connect(self.localparticlecombobox)
               
        # 1D checkbox
        #self.ui.onedCheckBox.setChecked(False)
        #self.ui.onedCheckBox.clicked.connect(self.onedcheckbox)
            
        # aspect ratio checkbox
        self.ui.aspectCheckBox.setChecked(False)
        self.ui.aspectCheckBox.clicked.connect(self.aspectcheckbox)
        
        # line selection checkbox
        self.ui.lineCheckBox.setChecked(False)
        self.ui.lineCheckBox.clicked.connect(self.linecheckbox)
        
        # rectangle selection checkbox
        self.ui.rectangleCheckBox.setChecked(False)
        self.ui.rectangleCheckBox.clicked.connect(self.rectanglecheckbox)
                        
        ## plot PushButton
        self.ui.plotButton.clicked.connect(self.plotbutton)
        
        # VisIt
        if dim == 3 and visitloaded:
            # 3D volume rendering checkbox
            #self.ui.yt3DCheckBox1.setChecked(False)
            #self.ui.yt3DCheckBox1.clicked.connect(self.yt3dcheckbox1)
            self.ui.yt3DPushButton.clicked.connect(self.yt3dpushbutton)
            self.ui.visitPushButton.clicked.connect(self.visitpushbutton)
            self.ui.visitclosePushButton.clicked.connect(self.visitclosepushbutton)
            
            # VisIt combo-boxe       
            for i in np.arange(len(visitmenu_list)):
                self.ui.visitComboBox.addItem(visitmenu_list[i], i)    
                
            self.ui.visitComboBox.currentIndexChanged.connect(self.visitcombobox)
            
            #self.ui.processorSpinBox.setMinimum(1)
            #self.ui.processorSpinBox.setMaximum(1024)
            #self.ui.processorSpinBox.setValue(visit_processors)
        
        # mesh refinement level buttons    
        self.ui.amrSpinBox.setValue(0)
        self.ui.amrSpinBox.setMinimum(0)
        #self.ui.amrgridCheckBox.setChecked(False)
        self.ui.amrSpinBox.valueChanged.connect(self.amrspinbox)
        #self.ui.amrgridCheckBox.clicked.connect(self.amrgridcheckbox)
                   
        # animation PushButton
        self.ui.animationButton.clicked.connect(self.animationbutton)
        self.ui.tiniSpinBox.setMinimum(1)
        self.ui.tiniSpinBox.setMaximum(tnum)
        self.ui.tiniSpinBox.setValue(1)
        self.ui.tmaxSpinBox.setMinimum(1)
        self.ui.tmaxSpinBox.setMaximum(tnum)
        self.ui.tmaxSpinBox.setValue(tnum)
                
        # select image CheckBox
        self.ui.pngCheckBox.setChecked(False)
        self.ui.pngCheckBox.clicked.connect(self.pngcheckbox)
        self.ui.epsCheckBox.setChecked(False)
        self.ui.epsCheckBox.clicked.connect(self.epscheckbox)
        self.ui.movieCheckBox.setChecked(False)
        self.ui.movieCheckBox.clicked.connect(self.moviecheckbox)
            
        # contrast sliders
        self.ui.contrastSlider.setRange(1,100)
        self.ui.contrastSlider.setValue(100)
        self.ui.contrastLabel.setText(str("%d" %100 )+"%")
        self.ui.contrastSlider.valueChanged.connect(self.contrastslider)
        
        # return original contrast button
        #self.ui.returncontrastButton.clicked.connect(self.returncontrastbutton)
        
        # pzmin and pzmax sliders
        #self.ui.p1minSlider.setRange(0,100)
        #self.ui.p1minSlider.setValue(0)
        #self.ui.p1minLabel.setText(str("%d" %0 )+"%")
        #self.ui.p1minSlider.valueChanged.connect(self.p1minslider)
        #self.ui.p1maxSlider.setRange(0,100)
        #self.ui.p1maxSlider.setValue(100)
        #self.ui.p1maxLabel.setText(str("%d" %100 )+"%")
        #self.ui.p1maxSlider.valueChanged.connect(self.p1maxslider)
        # pxmin and pxmax sliders
        #self.ui.p2minSlider.setRange(0,100)
        #self.ui.p2minSlider.setValue(0)
        #self.ui.p2minLabel.setText(str("%d" %0 )+"%")
        #self.ui.p2minSlider.valueChanged.connect(self.p2minslider)
        #self.ui.p2maxSlider.setRange(0,100)
        #self.ui.p2maxSlider.setValue(100)
        #self.ui.p2maxLabel.setText(str("%d" %100 )+"%")
        #self.ui.p2maxSlider.valueChanged.connect(self.p2maxslider)
        
        self.myplot.widget0.mpl_connect('motion_notify_event', self.motion_notify)
        self.myplot.widget0.mpl_connect('button_press_event', self.onclick)
        self.myplot.widget0.mpl_connect('button_release_event', self.release_click)
        
        
        self.load_parameters()
        
        self.editor = QtGui.QTextEdit()
        
        #if dim == 3:
        #    if warpx:
        #        self.ui.slicevalueLabel.setText(str("z=%.2f" %((x3max0-x3min0)*.5+x3min0)))
        #    else:
        #        self.ui.slicevalueLabel.setText(str("y=%.2f" %((x3max0-x3min0)*.5+x3min0)))
                
	# open a new directory and load files for basic information
    def opendir(self):	
        pass
        
    def close(self):
        sys.exit(0)
        self.myplot.close()
        #if mlab in sys.modules:
        #    mlab.close()
        #    self.ui.close()
    
    def quitpushbutton(self):
        sys.exit(0)
        self.myplot.close()
                    
    def plotbutton(self):
        
        global nrows, ncolumns, selectpanel, animation
        global newpanel
        global subsample
        animation = False
         
        #subsample2 = self.ui.subsampleSpinBox.value()
        #print('old subsample=%d, subsample=%d'%(subsample,subsample2))
        
        # read number of panels
        nrows2 = self.ui.rowpanelSpinBox.value()
        ncolumns2 = self.ui.columnpanelSpinBox.value()
        # no change in the number of panels
        if nrows2 == nrows and ncolumns2 == ncolumns:
            newpanel = False
        # change in the number of panels 
        else:    
            newpanel = True
            nrows = nrows2
            ncolumns = ncolumns2
            self.myplot = MyPlot()
            self.myplot.show()
            self.loaddata()
            
            self.ui.panelButton2.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
            self.ui.panelButton3.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
            self.ui.panelButton4.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
            self.ui.panelButton5.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
            self.ui.panelButton6.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
            self.ui.panelButton7.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
            self.ui.panelButton8.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
            self.ui.panelButton9.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
            self.ui.panelButton10.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
            
            if nrows*ncolumns >= 2:
                if ncolumns == 2:
                    self.ui.panelButton2.setStyleSheet("background-color: rgb(255, 140, 0);")    
                else:
                    self.ui.panelButton3.setStyleSheet("background-color: rgb(255, 140, 0);")   
            if nrows*ncolumns >= 3:
                if ncolumns == 2:
                    self.ui.panelButton3.setStyleSheet("background-color: rgb(255, 140, 0);")    
                else:
                    self.ui.panelButton5.setStyleSheet("background-color: rgb(255, 140, 0);") 
            if nrows*ncolumns >= 4:
                if ncolumns == 2:
                    self.ui.panelButton4.setStyleSheet("background-color: rgb(255, 140, 0);")    
                else:
                    self.ui.panelButton7.setStyleSheet("background-color: rgb(255, 140, 0);")
            if nrows*ncolumns >= 5:
                if ncolumns == 2:
                    self.ui.panelButton5.setStyleSheet("background-color: rgb(255, 140, 0);")    
                else:
                    self.ui.panelButton9.setStyleSheet("background-color: rgb(255, 140, 0);")
            if nrows*ncolumns >= 6:
                if ncolumns == 2:
                    self.ui.panelButton6.setStyleSheet("background-color: rgb(255, 140, 0);")    
            if nrows*ncolumns >= 7:
                if ncolumns == 2:
                    self.ui.panelButton7.setStyleSheet("background-color: rgb(255, 140, 0);")
            if nrows*ncolumns >= 8:
                if ncolumns == 2:
                    self.ui.panelButton8.setStyleSheet("background-color: rgb(255, 140, 0);")
            if nrows*ncolumns >= 9:
                if ncolumns == 2:
                    self.ui.panelButton9.setStyleSheet("background-color: rgb(255, 140, 0);")
            if nrows*ncolumns >= 10:
                if ncolumns == 2:
                    self.ui.panelButton10.setStyleSheet("background-color: rgb(255, 140, 0);")
            
        tstep = self.ui.timeSlider.value()
        #if tstep == tstep_previous: # and subsample == subsample2:
        #    self.plotwindow()
        #else:
            #subsample = subsample2
        self.load_parameters()
                      
        self.myplot.widget0.mpl_connect('motion_notify_event', self.motion_notify)
        self.myplot.widget0.mpl_connect('button_press_event', self.onclick)
        self.myplot.widget0.mpl_connect('button_release_event', self.release_click)
        
        if nrows*ncolumns >= 2:
            self.myplot.widget1.mpl_connect('motion_notify_event', self.motion_notify)
            self.myplot.widget1.mpl_connect('button_press_event', self.onclick)
            self.myplot.widget1.mpl_connect('button_release_event', self.release_click)
            
        if nrows*ncolumns >= 3:
            self.myplot.widget2.mpl_connect('motion_notify_event', self.motion_notify)
            self.myplot.widget2.mpl_connect('button_press_event', self.onclick)
            self.myplot.widget2.mpl_connect('button_release_event', self.release_click)
            
        if nrows*ncolumns >= 4:
            self.myplot.widget3.mpl_connect('motion_notify_event', self.motion_notify)
            self.myplot.widget3.mpl_connect('button_press_event', self.onclick)
            self.myplot.widget3.mpl_connect('button_release_event', self.release_click)
            
        if nrows*ncolumns >= 5:
            self.myplot.widget4.mpl_connect('motion_notify_event', self.motion_notify)
            self.myplot.widget4.mpl_connect('button_press_event', self.onclick)
            self.myplot.widget4.mpl_connect('button_release_event', self.release_click)
            
        if nrows*ncolumns >= 6:
            self.myplot.widget5.mpl_connect('motion_notify_event', self.motion_notify)
            self.myplot.widget5.mpl_connect('button_press_event', self.onclick)
            self.myplot.widget5.mpl_connect('button_release_event', self.release_click)
            
        if nrows*ncolumns >= 7:
            self.myplot.widget6.mpl_connect('motion_notify_event', self.motion_notify)
            self.myplot.widget6.mpl_connect('button_press_event', self.onclick)
            self.myplot.widget6.mpl_connect('button_release_event', self.release_click)      
            
        if nrows*ncolumns >= 8:
            self.myplot.widget7.mpl_connect('motion_notify_event', self.motion_notify)
            self.myplot.widget7.mpl_connect('button_press_event', self.onclick)
            self.myplot.widget7.mpl_connect('button_release_event', self.release_click)
            
        if nrows*ncolumns >= 9:
            self.myplot.widget8.mpl_connect('motion_notify_event', self.motion_notify)
            self.myplot.widget8.mpl_connect('button_press_event', self.onclick)
            self.myplot.widget8.mpl_connect('button_release_event', self.release_click)                     
            
        if nrows*ncolumns >= 10:
            self.myplot.widget9.mpl_connect('motion_notify_event', self.motion_notify)
            self.myplot.widget9.mpl_connect('button_press_event', self.onclick)
            self.myplot.widget9.mpl_connect('button_release_event', self.release_click)    
            
        newpanel = False
        self.editor = QtGui.QTextEdit()

    def panelbutton(self):
        
        global selectpanel
        global doload, doload2, doload3
        global doplot, doplot2, doplot3
        
        if self.ui.panelButton1.isChecked():
            selectpanel0 = 1
        elif self.ui.panelButton2.isChecked():
            if ncolumns == 2:
                selectpanel0 = 2
            else:
                selectpanel0 = 0
        elif self.ui.panelButton3.isChecked():
            if ncolumns == 2:
                selectpanel0 = 3
            else:
                selectpanel0 = 2
        elif self.ui.panelButton4.isChecked():
            if ncolumns == 2:
                selectpanel0 = 4
            else:
                selectpanel0 = 0
        elif self.ui.panelButton5.isChecked():
            if ncolumns == 2:
                selectpanel0 = 5
            else:
                selectpanel0 = 3
        elif self.ui.panelButton6.isChecked():
            if ncolumns == 2:
                selectpanel0 = 6
            else:
                selectpanel0 = 0
        elif self.ui.panelButton7.isChecked():
            if ncolumns == 2:
                selectpanel0 = 7
            else:
                selectpanel0 = 4
        elif self.ui.panelButton8.isChecked():
            if ncolumns == 2:
                selectpanel0 = 8
            else:
                selectpanel0 = 0
        elif self.ui.panelButton9.isChecked():
            if ncolumns == 2:
                selectpanel0 = 9
            else:
                selectpanel0 = 5
        elif self.ui.panelButton10.isChecked():
            if ncolumns == 2:
                selectpanel0 = 10
            else:
                selectpanel0 = 0
                
        if selectpanel0 != 0 and selectpanel0 <= nrows*ncolumns:
            selectpanel = selectpanel0
        
        else:
            if selectpanel == 1:
                self.ui.panelButton1.setChecked(True)
            if selectpanel == 2:
                if ncolumns == 2:
                    self.ui.panelButton2.setChecked(True)
                else:
                    self.ui.panelButton3.setChecked(True)
            if selectpanel == 3:
                if ncolumns == 2:
                    self.ui.panelButton3.setChecked(True)
                else:
                    self.ui.panelButton5.setChecked(True)
            if selectpanel == 4:
                if ncolumns == 2:
                    self.ui.panelButton4.setChecked(True)
                else:
                    self.ui.panelButton7.setChecked(True)
            if selectpanel == 5:
                if ncolumns == 2:
                    self.ui.panelButton5.setChecked(True)
                else:
                    self.ui.panelButton9.setChecked(True)
            if selectpanel == 6:
                if ncolumns == 2:
                    self.ui.panelButton6.setChecked(True)
            if selectpanel == 7:
                if ncolumns == 2:
                    self.ui.panelButton7.setChecked(True)
            if selectpanel == 8:
                if ncolumns == 2:
                    self.ui.panelButton8.setChecked(True)
            if selectpanel == 9:
                if ncolumns == 2:
                    self.ui.panelButton9.setChecked(True)
            if selectpanel == 10:
                if ncolumns == 2:
                    self.ui.panelButton10.setChecked(True)
        
        if fieldpanel_dic["fieldpanel{0}".format(selectpanel)]:
            fld = field_dic["fld{0}".format(selectpanel)]
            ind = field_list.index(fld)
            doload2 = False
            self.ui.fieldsComboBox.setCurrentIndex(ind)
            doload2 = True
            
            localfldphase = localfldphase_dic["localfldphase{0}".format(selectpanel)]
            ind = localfldphase_list.index(localfldphase)
            doplot2 = False
            self.ui.localfieldComboBox.setCurrentIndex(ind)
            doplot2 = True
            
            if self.ui.fieldButton.isChecked() == False:
                doload = False
                self.ui.fieldButton.setChecked(True)
                doload = True
        else:
            spec = species_dic["spec{0}".format(selectpanel)]
            ind = species_list.index(spec)
            doload2 = False
            self.ui.particlesComboBox.setCurrentIndex(ind)
            doload2 = True
            
            phase = phase_dic["phase{0}".format(selectpanel)]
            ind = phase_list.index(phase)
            doplot = False
            self.ui.phaseComboBox.setCurrentIndex(ind)
            doplot = True
            
            localprtlphase = localprtlphase_dic["localprtlphase{0}".format(selectpanel)]
            ind = localprtlphase_list.index(localprtlphase)
            doplot2 = False
            self.ui.localparticleComboBox.setCurrentIndex(ind)
            doplot2 = True
            
            if self.ui.particleButton.isChecked() == False:
                doload = False
                self.ui.particleButton.setChecked(True)
                doload = True
    
        lineselect = lineselect_dic["lineselect{0}".format(selectpanel)]
        doplot3 = False
        self.ui.lineCheckBox.setChecked(lineselect)
        doplot3 = True
        
        rectselect = rectselect_dic["rectselect{0}".format(selectpanel)]
        doplot3 = False
        self.ui.rectangleCheckBox.setChecked(rectselect)
        doplot3 = True
        amrlevel = amrlevel_dic["amrlevel{0}".format(selectpanel)]
        doplot4 = False
        self.ui.amrSpinBox.setValue(amrlevel)
        doplot4 = True
        
    def synccheckbox(self):
        global syncpanel
        if self.ui.syncCheckBox.isChecked():
            syncpanel = True
        else:
            syncpanel = False
            
    def lowrescheckbox(self):
        global lowreson
        if self.ui.lowresCheckBox.isChecked():
            lowreson = True
        else:
            lowreson = False
        self.plotwindow()

    def labframecheckbox(self):
        if self.ui.labframeCheckBox.isChecked():
            self.ui.timeSlider.setRange(1,tnum2)
            print(lab_list)
        else:
            self.ui.timeSlider.setRange(1,tnum)

    def load_parameters(self):
        # This function loads the domain sizes at this time step
        # if the time step does not alter, this function will not be called.         
        global tstep, time
        global xmin0, xmax0, ymin0, ymax0, zmin0, zmax0   # full simulation domain
        global nx, ny, nz
        global xaxis, yaxis, zaxis
        global tstep_previous
        
        tstep = self.ui.timeSlider.value()
        tstep_previous = tstep
        iteration = iterations[tstep-1]      
        
        if debug: start_time = tm.time()

        if warpx:
            ds = yt.load(file_path + '/plt%05d/'%(iteration))
            time=ds.current_time.d*1.e15
            xmin0=ds.domain_left_edge[0].d*1.e6
            ymin0=ds.domain_left_edge[1].d*1.e6
            xmax0=ds.domain_right_edge[0].d*1.e6
            ymax0=ds.domain_right_edge[1].d*1.e6
            nx = ds.domain_dimensions[0]
            ny = ds.domain_dimensions[1]
            if dim == 2: zmin0=0.; zmax0=0.; nz = 1
            if dim == 3:
                zmin0=ds.domain_left_edge[2].d*1.e6
                zmax0=ds.domain_right_edge[2].d*1.e6
                nz = ds.domain_dimensions[2]
            
            xaxis=1.*np.arange(nx)*(xmax0-xmin0)/nx+xmin0
            yaxis=1.*np.arange(ny)*(ymax0-ymin0)/ny+ymin0
            zaxis=1.*np.arange(nz)*(zmax0-zmin0)/nz+zmin0                        

        elif warp:
            fi = h5py.File(file_list[tstep-1], 'r')
            dset=fi['/data/'+str(iteration)]
            time=dset.attrs['time']*1.e15
            if dim ==3:
                dset=fi['/data/'+str(iteration)+'/fields/E']
                dx,dy,dz=dset.attrs["gridSpacing"]
                xmin0,ymin0,zmin0 = dset.attrs['gridGlobalOffset']
                dset=fi['/data/'+str(iteration)+'/fields/E/x']
                nx,ny,nz = dset.shape
                xmax0=nx*dx+xmin0
                ymax0=ny*dy+ymin0
                zmax0=nz*dz+zmin0
                xmin0*=1.e6;xmax0*=1.e6
                ymin0*=1.e6;ymax0*=1.e6
                zmin0*=1.e6;zmax0*=1.e6
                
                xaxis=1.*np.arange(nx)*(xmax0-xmin0)/nx+xmin0
                yaxis=1.*np.arange(ny)*(ymax0-ymin0)/ny+ymin0
                zaxis=1.*np.arange(nz)*(zmax0-zmin0)/nz+zmin0
                
            if dim ==2:
                dset=fi['/data/'+str(iteration)+'/fields/E']
                dx,dz=dset.attrs["gridSpacing"]
                xmin0,zmin0 = dset.attrs['gridGlobalOffset']
                if coord_system == 'cartesian':
                    dset=fi['/data/'+str(iteration)+'/fields/E/x']
                    nx,nz = dset.shape
                else:
                    dset=fi['/data/'+str(iteration)+'/fields/E/r']
                    nt,nx,nz = dset.shape
                xmax0=nx*dx+xmin0
                zmax0=nz*dz+zmin0
                xmin0*=1.e6;xmax0*=1.e6
                zmin0*=1.e6;zmax0*=1.e6
            
                xaxis=1.*np.arange(nx)*(xmax0-xmin0)/nx+xmin0
                zaxis=1.*np.arange(nz)*(zmax0-zmin0)/nz+zmin0
                
            if dim ==1:
                dset=fi['/data/'+str(iteration)+'/fields/E']
                dx=dset.attrs["gridSpacing"]
                xmin0 = dset.attrs['gridGlobalOffset']
                dset=fi['/data/'+str(iteration)+'/fields/E/x']
                nx = dset.shape
                xmax0=nx*dx+xmin0
                xmin0*=1.e6;xmax0*=1.e6
            
                xaxis=1.*np.arange(nx)*(xmax0-xmin0)/nx+xmin0
            
                    
        if debug: print("---load parameter %s seconds ---" % (tm.time() - start_time))
        
        if syncpanel == False:      
            self.loaddata()
        else:
            self.loaddata2()
        
    def loaddata(self):  
        if self.ui.fieldButton.isChecked():
            index=self.ui.fieldsComboBox.currentIndex()
            field = field_list[index]
            field_dic["fld{0}".format(selectpanel)] = field
    
            amrlevel = amrlevel_dic["amrlevel{0}".format(selectpanel)]
                       
            index=self.ui.localfieldComboBox.currentIndex()
            localfldphase = localfldphase_list[index]
            localfldphase_dic["localfldphase{0}".format(selectpanel)] = localfldphase
            
            fdata["fdata{0}".format(selectpanel)] = self.loadfield(field, amrlevel)
            fieldpanel_dic["fieldpanel{0}".format(selectpanel)] = True
        else: 
            index=self.ui.particlesComboBox.currentIndex()
            species = species_list[index]
            species_dic["spec{0}".format(selectpanel)] = species
            amrlevel = amrlevel_dic["amrlevel{0}".format(selectpanel)]
            
            xdata["xdata{0}".format(selectpanel)], ydata["ydata{0}".format(selectpanel)], zdata["zdata{0}".format(selectpanel)], \
                uxdata["uxdata{0}".format(selectpanel)], uydata["uydata{0}".format(selectpanel)], \
                uzdata["uzdata{0}".format(selectpanel)], wtdata["wtdata{0}".format(selectpanel)] = self.loadparticle(species, amrlevel)
            fieldpanel_dic["fieldpanel{0}".format(selectpanel)] = False
                        
            index=self.ui.phaseComboBox.currentIndex()
            phase = phase_list[index]
            phase_dic["phase{0}".format(selectpanel)] = phase
            
            index=self.ui.localparticleComboBox.currentIndex()
            localprtlphase = localprtlphase_list[index]
            localprtlphase_dic["localprtlphase{0}".format(selectpanel)] = localprtlphase
                            
        self.plotwindow()
        
    # when panels synchronized
    def loaddata2(self):  
        for index in np.arange(nrows*ncolumns):
            if fieldpanel_dic['fieldpanel'+str(index+1)]:
                field = field_dic["fld{0}".format(index+1)]
                amrlevel = amrlevel_dic["amrlevel{0}".format(index+1)]
                fdata["fdata{0}".format(index+1)] = self.loadfield(field, amrlevel)
            else:                 
                xdata["xdata{0}".format(index+1)], ydata["ydata{0}".format(index+1)], zdata["zdata{0}".format(index+1)], \
                    uxdata["uxdata{0}".format(index+1)], uydata["uydata{0}".format(index+1)], \
                    uzdata["uzdata{0}".format(index+1)], wtdata["wtdata{0}".format(index+1)] = \
                    self.loadparticle(species_dic["spec{0}".format(index+1)], amrlevel)
                    
        self.plotwindow()
        
    def loadfield(self, field, amrlevel):
        
        iteration = iterations[tstep-1]
        
        if debug: start_time = tm.time()
        
        # read data files
        if warpx:
            
            ds = yt.load(file_path + '/plt%05d/'%(iteration))
            dim_factor = 2**amrlevel
            nx1 = dim_factor*nx
            ny1 = dim_factor*ny
            if dim == 3:
                nz1 = dim_factor*nz
            all_data_level = ds.covering_grid(level=amrlevel,
                left_edge=ds.domain_left_edge, dims=dim_factor*ds.domain_dimensions)  
            if dim == 3:
                if sliceplane == 'xy':
                    tempdata = all_data_level[field][:, :, int(zsliceloc/100.*nz1)]
                if sliceplane == 'xz':
                    tempdata = all_data_level[field][:, int(ysliceloc/100.*ny1), :]
                    tempdata = tempdata.T
                if sliceplane == 'yz':
                    tempdata = all_data_level[field][int(xsliceloc/100.*nx1), :, :]
                    tempdata = tempdata.T
            else:
                tempdata = all_data_level[field][:, :, 0]
            
            tempdata = np.array(tempdata)
            tempdata = np.transpose(tempdata) 
        
        elif warp:  # warp data
            
            fi = h5py.File(file_list[tstep-1], 'r')
            nx1 = nx
            if dim ==2: nz1 = nz
            if dim == 3:
                ny1 = ny
                nz1 = nz
            dset = fi['/data/'+str(iteration)+'/fields/'+field[0]+'/'+field[1]]
            
            if dim == 3:
                if sliceplane == 'xy':
                    tempdata = dset[:, :, int(zsliceloc/100.*nz1)]
                if sliceplane == 'xz':
                    tempdata = dset[:, int(ysliceloc/100.*ny1),:]
                    #tempdata = tempdata.T
                if sliceplane == 'yz':
                    tempdata = dset[int(zsliceloc/100.*nx1),:,:]
                    #tempdata = tempdata.T
            else:
                
                if coord_system == 'cartesian':
                    tempdata = dset[()]
                else:
                    tempdata = dset[1,:,:]
           
        if debug: print("---field data loading %s seconds ---" % (tm.time() - start_time))
           
        return tempdata
        
    def loadparticle(self, species, amrlevel):
        
        iteration = iterations[tstep-1]  
        # read data files
        if debug: start_time = tm.time()
        
        if warpx:
            ds = yt.load(file_path + '/plt%05d/'%(iteration))
            ad = ds.all_data()
            if dim == 3:
                dim_factor = 2**amrlevel
                nx1 = dim_factor*nx
                ny1 = dim_factor*ny
                nz1 = dim_factor*nz

                if sliceplane == 'xy':
                    k1 = int(zsliceloc/100.*nz1-(stride/100.*.5)*nz1)
                    if k1 < 0: k1 = 0
                    k2 = int(zsliceloc/100.*nz1+(stride/100.*.5)*nz1)
                    if k2 >= nz1: k2 = nz1-1
                    zl = zaxis[k1]
                    zr = zaxis[k2]
                    tempz = ad[(species,'particle_position_z')].d*1.e6
                    loc = np.where((tempz >zl ) & (tempz < zr))[0]
                    
                    tempx = ad[(species,'particle_position_x')][loc].d*1.e6
                    tempy = ad[(species,'particle_position_y')][loc].d*1.e6
                    tempux = ad[(species,'particle_momentum_x')][loc].d/C
                    tempuy = ad[(species,'particle_momentum_y')][loc].d/C
                    tempuz = ad[(species,'particle_momentum_z')][loc].d/C
                    tempwt = ad[(species,'particle_weight')][loc].d
                    tempz = [0]
                    
                    tempx=np.float32(tempx)
                    tempy=np.float32(tempy)
                    tempux=np.float32(tempux)
                    tempuy=np.float32(tempuy)
                    tempuz=np.float32(tempuz)
                    tempwt=np.float32(tempwt)
                    
                if sliceplane == 'xz':
                    j1 = int(ysliceloc/100.*ny1-(stride/100.*.5)*ny1)
                    if j1 < 0: j1 = 0
                    j2 = int(ysliceloc/100.*ny1+(stride/100.*.5)*ny1)
                    if j2 >= ny1: j2 = ny1-1
                    yl = yaxis[j1]
                    yr = yaxis[j2]
                    tempy = ad[(species,'particle_position_y')].d*1.e6
                    loc = np.where((tempy >yl ) & (tempy < yr))[0]
                    
                    tempx = ad[(species,'particle_position_x')][loc].d*1.e6
                    tempz = ad[(species,'particle_position_z')][loc].d*1.e6
                    tempux = ad[(species,'particle_momentum_x')][loc].d/C
                    tempuy = ad[(species,'particle_momentum_y')][loc].d/C
                    tempuz = ad[(species,'particle_momentum_z')][loc].d/C
                    tempwt = ad[(species,'particle_weight')][loc].d
                    tempy = [0]
                    
                    tempx=np.float32(tempx)
                    tempz=np.float32(tempz)
                    tempux=np.float32(tempux)
                    tempuy=np.float32(tempuy)
                    tempuz=np.float32(tempuz)
                    tempwt=np.float32(tempwt)
                    
                if sliceplane == 'yz':
                    i1 = int(xsliceloc/100.*nx1-(stride/100.*.5)*nx1)
                    if i1 < 0: i1 = 0
                    i2 = int(xsliceloc/100.*nx1+(stride/100.*.5)*nx1)
                    if i2 >= nx1: i2 = nx1-1
                    xl = xaxis[i1]
                    xr = xaxis[i2]
                    tempx = ad[(species,'particle_position_x')].d*1.e6
                    loc = np.where((tempx >xl ) & (tempx < xr))[0]
                    
                    tempy = ad[(species,'particle_position_y')][loc].d*1.e6
                    tempz = ad[(species,'particle_position_z')][loc].d*1.e6
                    tempux = ad[(species,'particle_momentum_x')][loc].d/C
                    tempuy = ad[(species,'particle_momentum_y')][loc].d/C
                    tempuz = ad[(species,'particle_momentum_z')][loc].d/C
                    tempwt = ad[(species,'particle_weight')][loc].d
                    tempx = [0]
                    
                    tempy=np.float32(tempy)
                    tempz=np.float32(tempz)
                    tempux=np.float32(tempux)
                    tempuy=np.float32(tempuy)
                    tempuz=np.float32(tempuz)
                    tempwt=np.float32(tempwt)
            else:

                tempx = ad[(species,'particle_position_x')].d*1.e6
                tempy = ad[(species,'particle_position_y')].d*1.e6
                tempuz = ad[(species,'particle_momentum_z')].d/C
                tempux = ad[(species,'particle_momentum_y')].d/C
                tempuy = ad[(species,'particle_momentum_z')].d/C
                tempwt = ad[(species,'particle_weight')].d
                tempz = [0]
                
                tempx=np.float32(tempx)
                tempy=np.float32(tempy)
                tempux=np.float32(tempux)
                tempuy=np.float32(tempuy)
                tempuz=np.float32(tempuz)
                tempwt=np.float32(tempwt)
            
        elif warp:  # warp data
            fi = h5py.File(file_list[tstep-1], 'r')
            m = mass_dic[species]*me
            if dim == 3:
                nx1 = nx; ny1 = ny; nz1 = nz
                if sliceplane == 'xy':
                    k1 = int(zsliceloc/100.*nz1-(stride/100.*.5)*nz1)
                    if k1 < 0: k1 = 0
                    k2 = int(zsliceloc/100.*nz1+(stride/100.*.5)*nz1)
                    if k2 >= nz1: k2 = nz1-1
                    zl = zaxis[k1]
                    zr = zaxis[k2]
                    tempz = fi['data/'+str(iteration)+'/particles/'+species+'/position/z'][()]*1.e6
                    loc = np.where((tempz >zl ) & (tempz < zr))[0]

                    tempx = fi['data/'+str(iteration)+'/particles/'+species+'/position/x'][()]*1.e6
                    tempy = fi['data/'+str(iteration)+'/particles/'+species+'/position/y'][()]*1.e6
                    tempux = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/x'][()]/(m*C)
                    tempuy = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/y'][()]/(m*C)
                    tempuz = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/z'][()]/(m*C)
                    tempwt = fi['data/'+str(iteration)+'/particles/'+species+'/weighting'][()]    
                    tempx=np.float32(tempx)
                    tempy=np.float32(tempz)
                    tempwt=np.float32(tempwt)
                    tempux=np.float32(tempux)
                    tempuy=np.float32(tempuy)
                    tempuz=np.float32(tempuz)
                    tempz = [0]
                    tempx = tempx[loc]
                    tempy = tempx[loc]
                    tempux = tempux[loc]
                    tempuy = tempuy[loc]
                    tempuz = tempuz[loc]
                    
                if sliceplane == 'xz':
                    j1 = int(zsliceloc/100.*ny1-(stride/100.*.5)*ny1)
                    if j1 < 0: j1 = 0
                    j2 = int(ysliceloc/100.*ny1+(stride/100.*.5)*ny1)
                    if j2 >= ny1: j2 = ny1-1
                    yl = yaxis[j1]
                    yr = yaxis[j2]
                    tempy = fi['data/'+str(iteration)+'/particles/'+species+'/position/y'][()]*1.e6
                    loc = np.where((tempy >yl ) & (tempy < yr))[0]
    
                    tempx = fi['data/'+str(iteration)+'/particles/'+species+'/position/x'][()]*1.e6
                    tempz = fi['data/'+str(iteration)+'/particles/'+species+'/position/z'][()]*1.e6
                    tempux = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/x'][()]/(m*C)
                    tempuy = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/y'][()]/(m*C)
                    tempuz = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/z'][()]/(m*C)
                    tempwt = fi['data/'+str(iteration)+'/particles/'+species+'/weighting'][()]  
                    tempx=np.float32(tempx)
                    tempz=np.float32(tempz)
                    tempwt=np.float32(tempwt)
                    tempux=np.float32(tempux)
                    tempuy=np.float32(tempuy)
                    tempuz=np.float32(tempuz)
                    tempx = tempx[loc]
                    tempz = tempz[loc]
                    tempux = tempux[loc]
                    tempuy = tempuy[loc]
                    tempuz = tempuz[loc]
                    tempwt = tempwt[loc]
                    tempy = [0]
                    
                if sliceplane == 'yz':
                    i1 = int(xsliceloc/100.*nx1-(stride/100.*.5)*nx1)
                    if j1 < 0: j1 = 0
                    i2 = int(xsliceloc/100.*nx1+(stride/100.*.5)*nx1)
                    if i2 >= nx1: i2 = nx1-1
                    xl = xaxis[i1]
                    xr = xaxis[i2]
                    tempx = fi['data/'+str(iteration)+'/particles/'+species+'/position/x'][()]*1.e6
                    loc = np.where((tempx >xl ) & (tempx < xr))[0]
                    
                    tempy = fi['data/'+str(iteration)+'/particles/'+species+'/position/y'][()]*1.e6
                    tempz = fi['data/'+str(iteration)+'/particles/'+species+'/position/z'][()]*1.e6
                    tempux = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/x'][()]/(m*C)
                    tempuy = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/y'][()]/(m*C)
                    tempuz = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/z'][()]/(m*C)
                    tempwt = fi['data/'+str(iteration)+'/particles/'+species+'/weighting'][()]
                    tempy=np.float32(tempx)
                    tempz=np.float32(tempz)
                    tempwt=np.float32(tempwt)
                    tempux=np.float32(tempux)
                    tempuy=np.float32(tempuy)
                    tempuz=np.float32(tempuz)
                    tempx = [0]
                    tempy = tempy[loc]
                    tempz = tempz[loc]
                    tempux = tempux[loc]
                    tempuy = tempuy[loc]
                    tempuz = tempuz[loc]
            else:
                tempz = fi['data/'+str(iteration)+'/particles/'+species+'/position/z'][()]*1.e6
                tempx = fi['data/'+str(iteration)+'/particles/'+species+'/position/x'][()]*1.e6
                tempux = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/x'][()]/(m*C)
                tempuy = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/y'][()]/(m*C)
                tempuz = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/z'][()]/(m*C)
                tempwt = fi['data/'+str(iteration)+'/particles/'+species+'/weighting'][()]    
                tempy = [0]
                tempx=np.float32(tempx)
                tempz=np.float32(tempz)
                tempwt=np.float32(tempwt)
                tempux=np.float32(tempux)
                tempuy=np.float32(tempuy)
                tempuz=np.float32(tempuz)
             
            if debug: print("---particle data loading %s seconds ---" % (tm.time() - start_time))
            
            
        return tempx, tempy, tempz, tempux, tempuy, tempuz, tempwt
           
    def plotwindow(self):
        #global plotwincount
        #plotwincount +=1
        #print('plotwindow=',plotwincount)
        
        #global amredges
        global x1min0, x2min0, x1max0, x2max0
        global x1axis, x2axis
        global nx1, nx2
        
        #amrlevel = amrlevel_dic["amrlevel"+str(selectpanel)]
        
        if sliceplane == 'xy':
            x1min0 = xmin0; x1max0 = xmax0
            x2min0 = ymin0; x2max0 = ymax0
            x1axis = xaxis; x2axis = yaxis
            nx1 = nx; nx2 = ny
        if sliceplane == 'xz':
        
            x1min0 = zmin0; x1max0 = zmax0
            x2min0 = xmin0; x2max0 = xmax0
            x1axis = zaxis; x2axis = xaxis
            nx1 = nz; nx2 = nx
        if sliceplane == 'yz':
            
            x1min0 = zmin0; x1max0 = zmax0
            x2min0 = ymin0; x2max0 = ymax0
            x1axis = zaxis; x2axis = yaxis
            nx1 = nz; nx2 = ny
       
        if warpx and amrlevel >0:
            xleftedge=[]
            xrightedge=[]
            yleftedge=[]
            yrightedge=[]
            if dim == 3:
                zleftedge=[]
                zrightedge=[]
            for grids in ds.index.grids:
                if grids.Level == 1:
                    xleftedge.append(grids.LeftEdge[0].d*1.e6)
                    xrightedge.append(grids.RightEdge[0].d*1.e6)
                    yleftedge.append(grids.LeftEdge[1].d*1.e6)
                    yrightedge.append(grids.RightEdge[1].d*1.e6)
                    if dim == 3:
                        zleftedge.append(grids.LeftEdge[2].d*1.e6)
                        zrightedge.append(grids.RightEdge[2].d*1.e6)
        
            if sliceplane == 'xy':
                xleftedge0 = np.min(xleftedge)
                yleftedge0 = np.min(yleftedge)
                xrightedge0 = np.max(xrightedge)
                yrightedge0 = np.max(yrightedge)
            if sliceplane == 'xz':
                xleftedge0 = np.min(xleftedge)
                yleftedge0 = np.min(zleftedge)
                xrightedge0 = np.max(xrightedge)
                yrightedge0 = np.max(zrightedge)
            if sliceplane == 'yz':
                xleftedge0 = np.min(yleftedge)
                yleftedge0 = np.min(zleftedge)
                xrightedge0 = np.max(yrightedge)
                yrightedge0 = np.max(zrightedge)
                
            amredges = [xleftedge0, xrightedge0, yleftedge0, yrightedge0]
            
            print('amredges=',amredges)
               
        if debug: start_time = tm.time()    
                     
        if nrows*ncolumns >= 1:
            ind = 1
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    self.myplot.widget0.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                else:
                                        
                    self.myplot.widget0.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                          
        if nrows*ncolumns >= 2:
            ind = 2
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    try:
                        field_dic["fld"+str(ind)]
                    except KeyError:
                        fdata["fdata"+str(ind)] = fdata["fdata"+str(1)]
                        field_dic["fld"+str(ind)] = field_dic["fld1"]
                    
                    self.myplot.widget1.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                    
                else:
                    self.myplot.widget1.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                        
                        
        if nrows*ncolumns >= 3:
            ind = 3
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    try:
                        field_dic["fld"+str(ind)]
                    except KeyError:
                        fdata["fdata"+str(ind)] = fdata["fdata"+str(1)]
                        field_dic["fld"+str(ind)] = field_dic["fld1"]
                    
                    self.myplot.widget2.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                    
                else:
                    self.myplot.widget2.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                        
        if nrows*ncolumns >= 4:
            ind = 4
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    try:
                        field_dic["fld"+str(ind)]
                    except KeyError:
                        fdata["fdata"+str(ind)] = fdata["fdata"+str(1)]
                        field_dic["fld"+str(ind)] = field_dic["fld1"]
                    
                    self.myplot.widget3.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                    
                else:
                    self.myplot.widget3.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                        
        if nrows*ncolumns >= 5:
            ind = 5
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    try:
                        field_dic["fld"+str(ind)]
                    except KeyError:
                        fdata["fdata"+str(ind)] = fdata["fdata"+str(1)]
                        field_dic["fld"+str(ind)] = field_dic["fld1"]
                    
                    self.myplot.widget4.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                    
                else:
                    self.myplot.widget4.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                        
        if nrows*ncolumns >= 6:
            ind = 6
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    try:
                        field_dic["fld"+str(ind)]
                    except KeyError:
                        fdata["fdata"+str(ind)] = fdata["fdata"+str(1)]
                        field_dic["fld"+str(ind)] = field_dic["fld1"]
                    
                    self.myplot.widget5.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                    
                else:
                    self.myplot.widget5.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                            
        if nrows*ncolumns >= 7:
            ind = 7
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    try:
                        field_dic["fld"+str(ind)]
                    except KeyError:
                        fdata["fdata"+str(ind)] = fdata["fdata"+str(1)]
                        field_dic["fld"+str(ind)] = field_dic["fld1"]
                    
                    self.myplot.widget6.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                    
                else:
                    self.myplot.widget6.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                            
        if nrows*ncolumns >= 8:
            ind = 8
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    try:
                        field_dic["fld"+str(ind)]
                    except KeyError:
                        fdata["fdata"+str(ind)] = fdata["fdata"+str(1)]
                        field_dic["fld"+str(ind)] = field_dic["fld1"]
                    
                    self.myplot.widget7.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                    
                else:
                    self.myplot.widget7.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                            
        if nrows*ncolumns >= 9:
            ind = 9
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    try:
                        field_dic["fld"+str(ind)]
                    except KeyError:
                        fdata["fdata"+str(ind)] = fdata["fdata"+str(1)]
                        field_dic["fld"+str(ind)] = field_dic["fld1"]
                    
                    self.myplot.widget8.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                    
                else:
                    self.myplot.widget8.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                            
        if nrows*ncolumns >= 10:
            ind = 10
            if selectpanel == ind or newpanel or syncpanel:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    try:
                        field_dic["fld"+str(ind)]
                    except KeyError:
                        fdata["fdata"+str(ind)] = fdata["fdata"+str(1)]
                        field_dic["fld"+str(ind)] = field_dic["fld1"]
                    
                    self.myplot.widget9.field(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, fdata["fdata"+str(ind)], field_dic["fld"+str(ind)], oned, aspect, 
                            localfldphase_dic["localfldphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, x1axis, x2axis,
                            amrlevel_dic["amrlevel"+str(ind)],
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg)
                    
                else:
                    self.myplot.widget9.particle(warpx, dim, coord_system, mass_dic, amass_dic, time, taxis,
                            tstep, xdata["xdata"+str(ind)], ydata["ydata"+str(ind)], zdata["zdata"+str(ind)], 
                            uxdata["uxdata"+str(ind)], uydata["uydata"+str(ind)], uzdata["uzdata"+str(ind)], wtdata["wtdata"+str(ind)],
                            species_dic["spec"+str(ind)], phase_dic["phase"+str(ind)], aspect,
                            localprtlphase_dic["localprtlphase"+str(ind)], count_dic["count"+str(ind)],
                            x1min0, x1max0, x2min0, x2max0, nx1, nx2,
                            x1min_zoom, x1max_zoom, x2min_zoom, x2max_zoom,
                            xL_loc_dic["xL_loc"+str(ind)], xR_loc_dic["xR_loc"+str(ind)],
                            yL_loc_dic["yL_loc"+str(ind)], yR_loc_dic["yR_loc"+str(ind)],
                            lineselect_dic["lineselect"+str(ind)], rectselect_dic["rectselect"+str(ind)], contrast,
                            tempfit, temp, norm, 
                            p1min, p1max, p2min, p2max, sliceplane, animation, saveimg, lowreson)
                                                 
        if debug: print("---plotting %s seconds ---" % (tm.time() - start_time))
        
                
    def animationbutton(self):
        # while animation is running, this function is skipped.
        count = count_dic["count"+str(selectpanel)]
        if count != 0 or count !=2: pass
        if animation == False:
            self.c_thread=threading.Thread(target=self.myEvenListener)
            self.c_thread.start()
        else:
            print('**** If animation gets stuck due to a data loading problem, ****')
            print('**** press "plot" button and then "animation" button likely with different fields/particles ****')
            
    def myEvenListener(self):
        
        global animation
        animation = True
        count = count_dic["count"+str(selectpanel)]
        if self.ui.movieCheckBox.isChecked(): 
            if warpx:
                image_dir = '.'
            elif warp:
                image_dir = '..'
            if self.ui.fieldButton.isChecked():
                if count == 0:
                    os.system('mkdir '+image_dir+'/movie/')
                else:
                    index=self.ui.localphaseComboBox.currentIndex()
                    localphase = localfldphase_list[index]
                    os.system('mkdir '+image_dir+'/movie/')
            else:
                if count == 0:
                    os.system('mkdir '+image_dir+'/movie/')
                else:
                    index=self.ui.localphaseComboBox.currentIndex()
                    localphase = localprtlphase_list[index]
                    os.system('mkdir '+image_dir+'/movie/')
            
        global tini, tmax
        tini = self.ui.tiniSpinBox.value()
        tmax = self.ui.tmaxSpinBox.value()
        step = self.ui.stepSpinBox.value()
        # number of iterations in animation
        num = (tmax-tini)/step+1
        if num<0: num=1
        tarr = np.zeros(num, dtype=np.int)
        t = tmax
        for i in np.arange(num)[::-1]:
            tarr[i] = t
            t = t - step
        for tstep in tarr:
            tm.sleep(0.005)
            self.ui.tstepLabel.setText("%d" %tstep)
            self.ui.timeLabel.setText("%6.1f fs" % time)
            self.ui.timeSlider.setValue(tstep)
            
            self.load_parameters()
        # When animation is done, animation = False
        animation = False
        
             
    def forwardtimebutton(self):
        step = self.ui.stepSpinBox.value()
        tstep=self.ui.timeSlider.value()
        tstep = tstep + step
        if tstep > tnum:
            tstep = tstep - step
        self.ui.tstepLabel.setText("%d" %tstep)
        time = taxis[tstep-1]    
        self.ui.timeLabel.setText("%6.1f fs" % time)
        self.ui.timeSlider.setValue(tstep)
        self.load_parameters()
        
    def backwardtimebutton(self):
        step = self.ui.stepSpinBox.value()
        tstep = self.ui.timeSlider.value()
        tstep = tstep - step
        if tstep < 1:
            tstep = tstep + step
        self.ui.tstepLabel.setText("%d" %tstep)
        time = taxis[tstep-1]
        self.ui.timeLabel.setText("%6.1f fs" % time)
        self.ui.timeSlider.setValue(tstep)
        self.load_parameters()
       
    def timeslider(self):
        tstep=self.ui.timeSlider.value()
        time = taxis[tstep-1]
        self.ui.tstepLabel.setText("%d" %tstep)
        self.ui.timeLabel.setText("%6.1f fs" % time)
        
    def x1minslider(self):
        global x1min_zoom
        x1min_zoom = self.ui.x1minSlider.value()
        x1max_zoom = self.ui.x1maxSlider.value()
        if x1min_zoom >= x1max_zoom: 
            x1min_zoom = x1max_zoom - 1
        self.ui.x1minSlider.setValue(x1min_zoom)
        if sliceplane == 'xy':
            xmin = xmin0+(xmax0-xmin0)*x1min_zoom/100 
            self.ui.x1minLabel.setText(str("%d" %x1min_zoom)+"%"
                    +" = "+str("%.1f" %xmin))
        if sliceplane == 'xz':
            
            zmin = zmin0+(zmax0-zmin0)*x1min_zoom/100 
            self.ui.x1minLabel.setText(str("%d" %x1min_zoom)+"%"
                +" = "+str("%.1f" %zmin))
        if sliceplane == 'yz':
            
            zmin = zmin0+(zmax0-zmin0)*x1min_zoom/100 
            self.ui.x1minLabel.setText(str("%d" %x1min_zoom)+"%"
                +" = "+str("%.1f" %zmin))
            
    def x1maxslider(self):
        global x1max_zoom
        x1min_zoom = self.ui.x1minSlider.value()
        x1max_zoom = self.ui.x1maxSlider.value()
        if x1max_zoom <= x1min_zoom: 
            x1max_zoom = x1min_zoom + 1
        self.ui.x1maxSlider.setValue(x1max_zoom)
        if sliceplane == 'xy':
            xmax = xmin0+(xmax0-xmin0)*x1max_zoom/100 
            self.ui.x1maxLabel.setText(str("%d" %x1max_zoom)+"%"
                    +" = "+str("%.1f" %xmax))
        if sliceplane == 'xz':
            
            zmax = zmin0+(zmax0-zmin0)*x1max_zoom/100 
            self.ui.x1maxLabel.setText(str("%d" %x1max_zoom)+"%"
                +" = "+str("%.1f" %zmax))
        if sliceplane == 'yz':
            
            zmax = zmin0+(zmax0-zmin0)*x1max_zoom/100 
            self.ui.x1maxLabel.setText(str("%d" %x1max_zoom)+"%"
                +" = "+str("%.1f" %zmax))
            
    def x2minslider(self):
        global x2min_zoom
        x2min_zoom = self.ui.x2minSlider.value()
        x2max_zoom = self.ui.x2maxSlider.value()
        if x2min_zoom >= x2max_zoom: 
            x2min_zoom = x2max_zoom - 1
        self.ui.x2minSlider.setValue(x2min_zoom)
        if sliceplane == 'xy':
            ymin = ymin0+(ymax0-ymin0)*x2min_zoom/100 
            self.ui.x2minLabel.setText(str("%d" %x2min_zoom)+"%"
                    +" = "+str("%.1f" %ymin))
        if sliceplane == 'xz':
            
            xmin = xmin0+(xmax0-xmin0)*x2min_zoom/100 
            self.ui.x2minLabel.setText(str("%d" %x2min_zoom)+"%"
                +" = "+str("%.1f" %xmin))
        if sliceplane == 'yz':
            
            ymin = ymin0+(ymax0-ymin0)*x2min_zoom/100 
            self.ui.x2minLabel.setText(str("%d" %x2min_zoom)+"%"
                +" = "+str("%.1f" %ymin))
            
    def x2maxslider(self):
        global x2max_zoom
        x2min_zoom = self.ui.x2minSlider.value()
        x2max_zoom = self.ui.x2maxSlider.value()
        if x2max_zoom <= x2min_zoom: 
            x2max_zoom = x2min_zoom + 1
        self.ui.x2maxSlider.setValue(x2max_zoom)
        if sliceplane == 'xy':
            ymax = ymin0+(ymax0-ymin0)*x2max_zoom/100 
            self.ui.x2maxLabel.setText(str("%d" %x2max_zoom)+"%"
                    +" = "+str("%.1f" %ymax)) 
        if sliceplane == 'xz':
            
            xmax = xmin0+(xmax0-xmin0)*x2max_zoom/100 
            self.ui.x2maxLabel.setText(str("%d" %x2max_zoom)+"%"
                +" = "+str("%.1f" %xmax))
        if sliceplane == 'yz':
            
            ymax = ymin0+(ymax0-ymin0)*x2max_zoom/100 
            self.ui.x2maxLabel.setText(str("%d" %x2max_zoom)+"%"
                +" = "+str("%.1f" %ymax))
            
    def slicebutton(self):
        global sliceplane
        if dim == 3:
            if self.ui.xyButton.isChecked():
                sliceplane = 'xy'
                self.ui.x1min.setText("xmin")
                self.ui.x1max.setText("xmax")
                self.ui.x2min.setText("ymin")
                self.ui.x2max.setText("ymax")
                self.ui.slicevalueSlider.setValue(zsliceloc)
            if self.ui.xzButton.isChecked():
                sliceplane = 'xz'    
                self.ui.x1min.setText("zmin")
                self.ui.x1max.setText("zmax")
                self.ui.x2min.setText("xmin")
                self.ui.x2max.setText("xmax")
                self.ui.slicevalueSlider.setValue(ysliceloc)
            if self.ui.yzButton.isChecked():        
                sliceplane = 'yz'   
                self.ui.x1min.setText("zmin")
                self.ui.x1max.setText("zmax")
                self.ui.x2min.setText("ymin")
                self.ui.x2max.setText("ymax")
                self.ui.slicevalueSlider.setValue(xsliceloc)
            #if self.ui.fieldButton.isChecked():
            self.loaddata()
            #else:
            #    self.plotwindow()
        else:
            if warpx:
                self.ui.xyButton.setChecked(True)
            elif warp:
                self.ui.xzButton.setChecked(True)
         
    def centerpushbutton(self):
        global xsliceloc, ysliceloc, zsliceloc
        if dim == 3:
            slicevalue = 50
            if sliceplane == 'xy':
                zsliceloc = slicevalue
                zslicevalue = zmin0+(zmax0-zmin0)*slicevalue/100.
                self.ui.slicevalueLabel.setText(str("z=%.2f" %zslicevalue))
            if sliceplane == 'xz':
                ysliceloc= slicevalue
                yslicevalue = ymin0+(ymax0-ymin0)*slicevalue/100.
                self.ui.slicevalueLabel.setText(str("y=%.2f" %yslicevalue))
            if sliceplane == 'yz':
                xsliceloc= slicevalue
                xslicevalue = xmin0+(xmax0-xmin0)*slicevalue/100.
                self.ui.slicevalueLabel.setText(str("x=%.2f" %xslicevalue))
            self.ui.slicevalueSlider.setValue(50)
            self.loaddata()
        else:
            self.ui.slicevalueSlider.setValue(50)
            
    def strideslider(self):
        global stride
        if dim ==3 and self.ui.particleButton.isChecked():
            stride = self.ui.strideSlider.value()
            self.ui.strideLabel.setText(str("%2d%%" %stride))
        else:
            self.ui.strideSlider.setValue(1)
                
    def slicevalueslider(self):
        global xsliceloc, ysliceloc, zsliceloc
        if dim == 3:
            slicevalue = self.ui.slicevalueSlider.value()
            if sliceplane == 'xy':
                zsliceloc = slicevalue
                zslicevalue = zmin0+(zmax0-zmin0)*slicevalue/100.
                self.ui.slicevalueLabel.setText(str("z=%.2f" %zslicevalue))
            if sliceplane == 'xz':
                ysliceloc= slicevalue
                yslicevalue = ymin0+(ymax0-ymin0)*slicevalue/100.
                self.ui.slicevalueLabel.setText(str("y=%.2f" %yslicevalue))
            if sliceplane == 'yz':
                xsliceloc= slicevalue
                xslicevalue = xmin0+(xmax0-xmin0)*slicevalue/100.
                self.ui.slicevalueLabel.setText(str("x=%.2f" %xslicevalue))
            if self.ui.fieldButton.isChecked():
                self.loaddata()
        else:
            self.ui.slicevalueSlider.setValue(50)
                              
    def fieldbutton(self):
        global doload
        if doload: 
            self.loaddata()
        doload = True
                
    def particlebutton(self):
        global doload
        if doload: 
            self.loaddata()
        doload = True                
                        
    def fieldcombobox(self):
        global doload, doload2
        doload = False
        self.ui.fieldButton.setChecked(True)        
        doload = True
        if doload2:
            self.loaddata()
        doload2 = True
        
    def particlecombobox(self):
        global doload, doload2
        doload = False
        self.ui.particleButton.setChecked(True)
        doload = True
        if doload2:
            self.loaddata()
        doload2 = True
                       
    def phasecombobox(self):
        global doplot
        if doplot:
            index = self.ui.phaseComboBox.currentIndex()
            phase = phase_list[index]
            phase_dic["phase{0}".format(selectpanel)] = phase
            self.plotwindow()
        doplot = True
        
    def localfieldcombobox(self):
        global doplot2
        if doplot2:
            index=self.ui.localfieldComboBox.currentIndex()
            localfldphase = localfldphase_list[index]
            localfldphase_dic["localfldphase{0}".format(selectpanel)] = localfldphase
            self.plotwindow()
        doplot2 = True    
            
    def localparticlecombobox(self):
        global doplot2
        if doplot2:
            index=self.ui.localparticleComboBox.currentIndex()
            localprtlphase = localprtlphase_list[index]
            localprtlphase_dic["localprtlphase{0}".format(selectpanel)] = localprtlphase
            self.plotwindow()
        doplot2 = True
        
    def pngcheckbox(self):
        global saveimg
        if self.ui.pngCheckBox.isChecked():
            saveimg = 1
            self.ui.epsCheckBox.setChecked(False)
            self.ui.movieCheckBox.setChecked(False)
        else:
            saveimg = 0
            
    def epscheckbox(self):
        global saveimg
        if self.ui.epsCheckBox.isChecked():
            saveimg = 2
            self.ui.pngCheckBox.setChecked(False)
            self.ui.movieCheckBox.setChecked(False)
        else:
            saveimg = 0
            
    def moviecheckbox(self):
        global saveimg
        if self.ui.movieCheckBox.isChecked():
            saveimg = 3
            self.ui.pngCheckBox.setChecked(False)
            self.ui.epsCheckBox.setChecked(False)
            if(warpx == True):
                os.system('mkdir ./movie')
            else:
                os.system('mkdir ../movie')
        else:
            saveimg = 0
                
    def visitcombobox(self):        
        global visit_ind
        visit_ind = self.ui.visitComboBox.currentIndex()
 
    # 3D volume rendering using visit
    def visitpushbutton(self):
        #global visit_processors
        global fname
        if fieldpanel_dic['fieldpanel'+str(selectpanel)]:
            tstep = self.ui.timeSlider.value()
            fld = field_dic["fld"+str(selectpanel)]
            iteration = iterations[tstep-1]
            #visit_processors = self.ui.processorSpinBox.value()
            try:
                visit.Launch()
                #InvertBackgroundColor()
            except visit.VisItException:
                visit.DeleteAllPlots()
                
            if warpx:        
                fname=file_list[tstep-1]+"/Header"
                visit.OpenDatabase(fname)
            elif warp:
                fname=file_list[tstep-1]
                visit.OpenDatabase(fname)
                
            if visit_ind == 0: # iso-surface
                visit.AddPlot("Pseudocolor", fld)
                visit.AddOperator("Isosurface") 
                visit.DrawPlots()
                
            if visit_ind == 1: # volume rendering
                visit.AddPlot("Volume", fld)
                c = visit.GaussianControlPointList()
                p = visit.GaussianControlPoint()
                factor = 4
                p.x      = 0.1
                p.height = 0.2*factor
                p.width  = 0.05
                c.AddControlPoints(p)
                p.x      = 0.9
                p.height = 0.2*factor
                p.width  = 0.05
                c.AddControlPoints(p)
                p.x      = 0.3
                p.height = 0.1*factor
                p.width  = 0.05
                c.AddControlPoints(p)
                p.x      = 0.7
                p.height = 0.1*factor
                p.width  = 0.05
                c.AddControlPoints(p)
                p.x      = 0.5
                p.height = 0.003*factor
                p.width  = 0.1
                c.AddControlPoints(p)
                v=visit.VolumeAttributes()
                v.opacityMode = v.GaussianMode
                v.opacityAttenuation=0.5
                v.SetOpacityControlPoints(c)
                visit.SetPlotOptions(v)
                visit.DrawPlots()
                   
                
            #elif visit_processors > 1:  # parallel rendering
                            
                #dir = './'
                #file = open('visit_temp.py','w')
                #file.write('# visit temporary script\n')
                #file.write('OpenDatabase("'+file_list[tstep-1]+'/Header")\n')
                ##file.write('InvertBackgroundColor()\n')
                    
                #if visit_ind == 1: # volume rendering
                         
                #file.write(
                #'AddPlot("Pseudocolor", '+'"'+fld+'")\n'+
                #'AddOperator("Isosurface")\n'+
                #'DrawPlots()\n')
                        
                #file.close()
                #os.system('visit -np '+str(visit_processors)+' -cli  -s visit_temp.py')
                   
    def visitclosepushbutton(self):
        try:
            visit.Close()
            visit.CloseDatabase(fname)
        except visit.VisItException:
            pass
            
    def yt3dpushbutton(self):
        
        if nrows*ncolumns >= 1:
            ind = 1
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget0.yt_volume(tstep,yt_volumedata, fld)
                        
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget0.yt_volume(tstep,yt_volumedata, fld)
                          
        if nrows*ncolumns >= 2:
            ind = 2
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget1.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget1.yt_volume(tstep,yt_volumedata, fld)
                        
        if nrows*ncolumns >= 3:
            ind = 3
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget2.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget2.yt_volume(tstep,yt_volumedata, fld)
        if nrows*ncolumns >= 3:
            ind = 3
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget2.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget2.yt_volume(tstep,yt_volumedata, fld)
                        
        if nrows*ncolumns >= 4:
            ind = 4
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget3.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget3.yt_volume(tstep,yt_volumedata, fld)
                        
        if nrows*ncolumns >= 5:
            ind = 5
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget4.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget4.yt_volume(tstep,yt_volumedata, fld)
                        
        if nrows*ncolumns >= 6:
            ind = 6
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget5.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget5.yt_volume(tstep,yt_volumedata, fld)
                        
        if nrows*ncolumns >= 7:
            ind = 7
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget6.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget6.yt_volume(tstep,yt_volumedata, fld)
                        
        if nrows*ncolumns >= 8:
            ind = 8
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget7.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget7.yt_volume(tstep,yt_volumedata, fld)
                        
        if nrows*ncolumns >= 9:
            ind = 9
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget8.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget8.yt_volume(tstep,yt_volumedata, fld)
                        
        if nrows*ncolumns >= 10:
            ind = 10
            if selectpanel == ind:
                if fieldpanel_dic['fieldpanel'+str(ind)]:
                    tstep = self.ui.timeSlider.value()
                    fld = field_dic["fld"+str(ind)]
                    if warpx:
                        amrlevel = amrlevel_dic["amrlevel"+str(ind)]            
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = amrlevel
                        sc = yt.create_scene(reg, fld)
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((0.01, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.0001, 100))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget9.yt_volume(tstep,yt_volumedata, fld)
                    elif warp:
                        
                        ds = yt.load(file_list[tstep-1]) 
                        azimuth=20
                        elevation=40
                        reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
                        reg.max_level = 0
                        sc = yt.create_scene(reg, fld[0]+'_'+fld[1])
                        sc.camera.focus = ds.domain_center
                        sc.camera.resolution = 256
                        sc.camera.north_vector = [0, 0, 1]
                        sc.camera.zoom(1.1)
                        sc.camera.rotate(azimuth*np.pi/180)
                        sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
                        sc.annotate_axes(alpha=1)
                        source = sc[0]
                        if fld[0] == 'E':
                            source.tfh.set_bounds((1, 1e13))
                        elif fld[0] == 'B':
                            source.tfh.set_bounds((0.001, 1e5))
                        source.tfh.grey_opacity = False
                        sc.render()
                        yt_volumedata = np.array(sc._last_render)
                        self.myplot.widget9.yt_volume(tstep,yt_volumedata, fld)
                    
                 
                                                  
    def motion_notify(self,event):
        imx, imy = event.xdata, event.ydata
        if imx != None and imy != None:
            self.ui.coordLabel.setText("(x1,x2)=(%4.2f, %4.2f)" %(imx, imy))
            
    #def onedcheckbox(self):
    #    global oned1
    #    if self.ui.onedCheckBox.isChecked():
    #        oned = True
    #    else:
    #        oned = False
    #    self.plotwindow()

    def aspectcheckbox(self):
        global aspect
        if self.ui.aspectCheckBox.isChecked():
            aspect='equal'
        else:
            aspect='auto'
        self.plotwindow()
                        
    def linecheckbox(self):
        global doplot3
        if doplot3:
            if self.ui.lineCheckBox.isChecked() == False:
                count = 0
                count_dic["count{0}".format(selectpanel)] = count
                lineselect_dic["lineselect{0}".format(selectpanel)] = False
                self.plotwindow()
            else:
                lineselect_dic["lineselect{0}".format(selectpanel)] = True
                rectselect_dic["rectselect{0}".format(selectpanel)] = False
                doplot3=False
                self.ui.rectangleCheckBox.setChecked(False)
        doplot3 = True
                                      
    def rectanglecheckbox(self):
        global doplot3
        if doplot3:
            if self.ui.rectangleCheckBox.isChecked() == False:
                count = 0
                count_dic["count{0}".format(selectpanel)] = count
                rectselect_dic["rectselect{0}".format(selectpanel)] = False
                self.plotwindow()
            else:
                lineselect_dic["lineselect{0}".format(selectpanel)] = False
                rectselect_dic["rectselect{0}".format(selectpanel)] = True
                doplot3 = False
                self.ui.lineCheckBox.setChecked(False)
        doplot3 = True
        
    def onclick(self, event):
        imx, imy = event.xdata, event.ydata
        global i_press, j_press
        imx, imy = event.xdata, event.ydata
        i_press = imx
        j_press = imy
        #if self.ui.fieldButton.isChecked():
        #    if oned == True: pass
        if self.ui.particleButton.isChecked():
            index=self.ui.phaseComboBox.currentIndex()
            phase = phase_list[index]
            #if phase1 != 'x-z': pass
        if self.ui.rectangleCheckBox.isChecked() or self.ui.lineCheckBox.isChecked():
            #global count
            #global xL_loc, yL_loc, xR_loc, yR_loc
            count = count_dic["count"+str(selectpanel)]
            if count == 2:
                count = 0
                count_dic["count{0}".format(selectpanel)] = count
            if sliceplane == 'xy':
                x1min = xmin0+(xmax0-xmin0)*x1min_zoom/100  
                x1max = xmin0+(xmax0-xmin0)*x1max_zoom/100
                x2min = ymin0+(ymax0-ymin0)*x2min_zoom/100  
                x2max = ymin0+(ymax0-ymin0)*x2max_zoom/100
            if sliceplane == 'xz':
                
                x1min = zmin0+(zmax0-zmin0)*x1min_zoom/100  
                x1max = zmin0+(zmax0-zmin0)*x1max_zoom/100
                x2min = xmin0+(xmax0-xmin0)*x2min_zoom/100   
                x2max = xmin0+(xmax0-xmin0)*x2max_zoom/100
            if sliceplane == 'yz':
                
                x1min = zmin0+(zmax0-zmin0)*x1min_zoom/100  
                x1max = zmin0+(zmax0-zmin0)*x1max_zoom/100
                x2min = ymin0+(ymax0-ymin0)*x2min_zoom/100  
                x2max = ymin0+(ymax0-ymin0)*x2max_zoom/100
            if imx != None and imy != None:
                count += 1
                count_dic["count{0}".format(selectpanel)] = count
            if count == 1:
                xL_loc = 1.*(imx - x1min)/(x1max-x1min)
                yL_loc = 1.*(imy - x2min)/(x2max-x2min)
                xL_loc_dic["xL_loc{0}".format(selectpanel)] = xL_loc
                yL_loc_dic["yL_loc{0}".format(selectpanel)] = yL_loc
                self.plotwindow()
            if count == 2:
                xR_loc = 1.*(imx - x1min)/(x1max-x1min)
                yR_loc = 1.*(imy - x2min)/(x2max-x2min)
                xR_loc_dic["xR_loc{0}".format(selectpanel)] = xR_loc
                yR_loc_dic["yR_loc{0}".format(selectpanel)] = yR_loc
                self.plotwindow()
                
                 
    def release_click(self, event):
        global i_release, j_release
        i_release, j_release, = event.xdata, event.ydata
     
    """
    def t2dcheckbox(self):
        global tempfit, temp, norm
        temp = float(self.ui.TempEdit.text())
        norm = float(self.ui.NormEdit.text())
        if self.ui.T2dCheckBox.isChecked() == True:
            self.ui.T3dCheckBox.setChecked(False)
            tempfit = 1
        else: 
            tempfit = 0

    def t3dcheckbox(self):
        global tempfit, temp, norm
        temp = float(self.ui.TempEdit.text())
        norm = float(self.ui.NormEdit.text())
        if self.ui.T3dCheckBox.isChecked() == True:
            self.ui.T2dCheckBox.setChecked(False)
            tempfit = 2            
        else:
            tempfit = 0
    """
            
    def contrastslider(self):
        global contrast
        contrast = self.ui.contrastSlider.value()
        self.ui.contrastLabel.setText(str("%d" %contrast)+"%")
            
        
    def returncontrastbutton(self):
        global contrast
        contrast = 100
        self.ui.contrastSlider.setValue(contrast)
        self.ui.contrastLabel.setText(str("%d" %contrast)+"%")    
        #self.plotbutton()
        
    def amrspinbox(self):
        #global amrlevel 
        global doload3
        if doload3:
            amrlevel_dic["amrlevel{0}".format(selectpanel)] = self.ui.amrSpinBox.value()  
            self.loaddata()
        doload3 = True
        
    #def amrgridcheckbox(self):
    #    global amrgrid
    #    if self.ui.amrgridCheckBox.isChecked():
    #        amrgrid = 1
    #    else:
    #        amrgrid = 0
                      
    #def p1minslider(self):
    #    global p1min
    #    p1min = self.ui.p1minSlider.value()
    #    p1max = self.ui.p1maxSlider.value()
    #    if p1min >= p1max: 
    #        p1min = p1max - 1
    #    self.ui.p1minSlider.setValue(p1min)
    #    self.ui.p1minLabel.setText(str("%d" %p1min)+"%")
                                   
    #def p1maxslider(self):
    #    global p1max
    #    p1min = self.ui.p1minSlider.value()
    #    p1max = self.ui.p1maxSlider.value()
    #    if p1max <= p1min: 
    #        p1max = p1min + 1
    #    self.ui.p1maxSlider.setValue(p1max)
    #    self.ui.p1maxLabel.setText(str("%d" %p1max)+"%")
            
    #def p2minslider(self):
    #    p2min = self.ui.p2minSlider.value()
    #    p2max = self.ui.p2maxSlider.value()
    #    if p2min >= p2max: 
    #        p2min = p2max - 1
    #    self.ui.p2minSlider.setValue(p2min)
    #    self.ui.p2minLabel.setText(str("%d" %p2min)+"%")
                
    #def p2maxslider(self):
    #    global p2max
    #    p2min = self.ui.p2minSlider.value()
    #    p2max = self.ui.p2maxSlider.value()
    #    if p2max <= p2min: 
    #        p2max = p2min + 1
    #    self.ui.p2maxSlider.setValue(p2max)
    #    self.ui.p2maxLabel.setText(str("%d" %p2max)+"%")

        
if __name__ == "__main__":
    
    try:
        app = QtGui.QApplication(sys.argv)
    except RuntimeError:
        app = QtCore.QCoreApplication.instance()
        
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())
