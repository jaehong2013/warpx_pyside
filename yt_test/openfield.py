import h5py
import numpy as np
import time as tm
import glob
from mpi4py import MPI
from cic_histogram import histogram_cic_1d, histogram_cic_2d, particle_energy
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.colors import LogNorm

# get MPI size and rank
size = MPI.COMM_WORLD.Get_size()
rank = MPI.COMM_WORLD.Get_rank()

# collect warp data file names
file_list = glob.glob('./data????????.h5')
iterations = [ int(file_name[len(file_name)-11:len(file_name)-3]) for file_name in file_list ]
tstep = len(iterations)
iteration = iterations[tstep-1]
filename = file_list[tstep-1]

# clock starts
start_time = tm.time()

# read h5
fi = h5py.File(filename, 'r')    
# get data dimension
item = fi['data/'+str(iteration)+'/fields/E'].attrs['axisLabels']
dim = len(item)

start_time1 = tm.time()
# get data volume info
if dim == 2:
    nx, nz = fi['data/'+str(iteration)+'/fields/E/x'][:].shape
    dx, dz =  fi['data/'+str(iteration)+'/fields/E'].attrs['gridSpacing']
    Lz = dz*nz*1.e6
    Lx = dx*nx*1.e6
    zmin = 0.; zmax = Lz
    xmin = -Lx/2; xmax = Lx/2
else:
    nx, ny, nz = fi['data/'+str(iteration)+'/fields/E/x'][:].shape
    dx, dy, dz =  fi['data/'+str(iteration)+'/fields/E'].attrs['gridSpacing']
    Lz = dz*nz*1.e6
    Lx = dx*nx*1.e6
    Ly = dy*ny*1.e6
    zmin = 0.; zmax = Lz
    xmin = -Lx/2; xmax = Lx/2
    ymin = -Ly/2; ymax = Ly/2

print(nx,nz)

nzpart = nz#/size
nxpart = nx/size
if np.mod(nzpart,2) == 1: nzpart -=1
if np.mod(nxpart,2) == 1: nxpart -=1
#if rank == size-1:
#    i1 = rank*nzpart
#    i2 = nz
#else:
#    i1 = rank*nzpart
#    i2 = (rank+1)*nzpart

j1 = rank*nxpart
j2 = (rank+1)*nxpart

i1 = 0
i2 = nzpart

C = 2.99792458e8 # light speed
me = 9.10938291e-31 # electron mass
    
start_time2= tm.time()
# read particle data in each rank
tempez = fi['data/'+str(iteration)+'/fields/E/z'][j1:j2,i1:i2]
print(tempez.shape, rank)

print("---data loading %f3.3 seconds from rank %d ---" % (tm.time() - start_time2, rank))

start_time4= tm.time()

field_total = np.zeros([nxpart*size,nzpart])
# collect histogram data into rank 0
MPI.COMM_WORLD.Allgather(tempez, field_total)
print("---MPI Gather %f3.3 seconds from rank %d ---" % (tm.time() - start_time4, rank))

# plot imshow in rank 0
if rank == 0:
    start_time5= tm.time()
#    histogram_total /= size
#    logthresh = 2                      
    im = plt.imshow(field_total , interpolation='nearest', cmap='jet',
            origin='lower', extent=[ zmin,zmax,xmin,xmax ], aspect='auto')
       
    print("---imshow %f3.3 seconds  ---" %(tm.time() - start_time5))        
    print("---total time %f3.3 seconds  ---" %(tm.time() - start_time))
            
            
    plt.show()
