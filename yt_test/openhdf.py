import h5py
import numpy as np
import time as tm
import glob
from mpi4py import MPI
from cic_histogram import histogram_cic_1d, histogram_cic_2d, particle_energy, velocity_2d
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.colors import LogNorm
from pylab import *
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import ticker
from matplotlib.ticker import MaxNLocator

# get MPI size and rank
size = MPI.COMM_WORLD.Get_size()
rank = MPI.COMM_WORLD.Get_rank()

# collect warp data file names
file_list = glob.glob('./data????????.h5')
iterations = [ int(file_name[len(file_name)-11:len(file_name)-3]) for file_name in file_list ]
tstep = len(iterations)
iteration = iterations[tstep-1]
filename = file_list[tstep-1]

# clock starts
if rank == 0: start_time = tm.time()

# read h5
fi = h5py.File(filename, 'r')    
# get particle species
item = fi['data/'+str(iteration)+'/particles']
species_group = item.items()
species = species_group[1][0]
#species = 'elec_Ti'
# get data dimension
item = fi['data/'+str(iteration)+'/fields/E'].attrs['axisLabels']
dim = len(item)

start_time1 = tm.time()
# get data volume info
if dim == 2:
    nx, nz = fi['data/'+str(iteration)+'/fields/E/x'][:].shape
    dx, dz =  fi['data/'+str(iteration)+'/fields/E'].attrs['gridSpacing']
    Lz = dz*nz*1.e6
    Lx = dx*nx*1.e6
    zmin = 0.; zmax = Lz
    xmin = -Lx/2; xmax = Lx/2
else:
    nx, ny, nz = fi['data/'+str(iteration)+'/fields/E/x'][:].shape
    dx, dy, dz =  fi['data/'+str(iteration)+'/fields/E'].attrs['gridSpacing']
    Lz = dz*nz*1.e6
    Lx = dx*nx*1.e6
    Ly = dy*ny*1.e6
    zmin = 0.; zmax = Lz
    xmin = -Lx/2; xmax = Lx/2
    ymin = -Ly/2; ymax = Ly/2
    
# get total particle number info
ntotpart = fi['data/'+str(iteration)+'/particles/'+species+'/mass'].attrs['shape'][0] 
# divide the total particles by the mpi size.
npart = int(ntotpart/size)
i1 = rank*npart
i2 = (rank+1)*npart

C = 2.99792458e8 # light speed
me = 9.10938291e-31 # electron mass
    
start_time2= tm.time()
# read particle data in each rank
tempz = fi['data/'+str(iteration)+'/particles/'+species+'/position/z'][i1:i2]*1.e6
tempx = fi['data/'+str(iteration)+'/particles/'+species+'/position/x'][i1:i2]*1.e6
tempux = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/x'][i1:i2]/(me*C)
tempuy = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/y'][i1:i2]/(me*C)
tempuz = fi['data/'+str(iteration)+'/particles/'+species+'/momentum/z'][i1:i2]/(me*C)
tempwt = fi['data/'+str(iteration)+'/particles/'+species+'/weighting'][i1:i2]    
tempwt = tempwt/np.max(tempwt)
    

#tempz = np.array(tempz)*1e6
#tempx = np.array(tempx)*1e6
#tempux = np.array(tempux)/(me*C)
#tempuy = np.array(tempuy)/(me*C)
#tempuz = np.array(tempuz)/(me*C)
print("---%d particles loading %f3.3 seconds from rank %d ---" % (npart, tm.time() - start_time2, rank))

start_time3= tm.time()
nbin = 400
# histogram by calling a Numba routine
#histogram = histogram_cic_2d( tempz, tempx, tempwt, nbin, zmin, zmax, nbin, xmin, xmax)
velocity, histogram= velocity_2d( tempz, tempx, tempux, tempuy, tempuz, tempwt, nbin, zmin, zmax, nbin, xmin, xmax)
print("---histogram %f3.3 seconds from rank %d ---" % (tm.time() - start_time3, rank))

velocity = velocity/histogram
velocity[velocity ==0] =0  

start_time4= tm.time()
if rank == 0: 
    velocity_total = np.zeros_like(velocity)
else:
    velocity_total = None    
# collect histogram data into rank 0
MPI.COMM_WORLD.Reduce(velocity, velocity_total, op = MPI.SUM, root =0)
print("---MPI reduce %f3.3 seconds from rank %d ---" % (tm.time() - start_time4, rank))


# plot imshow in rank 0
if rank == 0:
#    histogram_total /= size
    #logthresh = 2                      
    im = plt.imshow( velocity_total.T, 
            origin='lower', extent=[ zmin,zmax,xmin,xmax ], cmap='jet', aspect='auto')
            #norm=matplotlib.colors.LogNorm(10**-logthresh) )
    ax = plt.gca()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size=0.15, pad=0.0)
    plt.colorbar(im, cax=cax, orientation='vertical')
            
    print("---total time %f3.3 seconds  ---" %(tm.time() - start_time))
            
            
    plt.show()
