import yt
import numpy as np
import time as tm
import glob
from mpi4py import MPI
from cic_histogram import histogram_cic_1d, histogram_cic_2d, particle_energy
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.colors import LogNorm

yt.enable_parallelism()

# get MPI size and rank
size = MPI.COMM_WORLD.Get_size()
rank = MPI.COMM_WORLD.Get_rank()

#C = 2.99792458e8 # light speed
#me = 9.10938291e-31 # electron mass
#qe = 1.60217657e-19 # electron charge

# collect warpx data file names
file_list = glob.glob('./plt?????')
iterations = [ int(file_name[len(file_name)-5:]) for file_name in file_list ]
tstep = len(iterations)
iteration = iterations[tstep-1]
filename = file_list[tstep-1]

#mass_dic = {}   # mass dictionary
#charge_dic = {} # charge dictionary
#amass_dic = {} # atomic mass number dictionary

# clock starts
start_time = tm.time()

# open file
ds = yt.load('./plt%05d/'%(iteration))


#ds.index
#species_list_all=ds.particle_types
#species_list = []
#for species in species_list_all:
#    if species[0] != 'a':
#        try:
#            q = ds.parameters[species+'_charge'].d
#            m = ds.parameters[species+'_mass'].d
#        except AttributeError:
#            q = ds.parameters[species+'_charge']
#            m = ds.parameters[species+'_mass']
#        species_list.append(species)
#        mass_dic[species] = m/me     # mass ratio
#        charge_dic[species] = q/qe   # charge ratio
#        amass_dic[species] = m/(me*1836)  # atomic mass number
#        if rank ==0 : print('species,q,m=',species,q,m)
         
#nspecies = len(species_list)

#species = species_list[2]

#if ds.domain_dimensions[2] > 1:
#    dim = 3
#elif ds.domain_dimensions[1] == 1:
#    dim = 1
#else: 
#    dim = 2

#time=ds.current_time.d*1.e15
#xmin0=ds.domain_left_edge[0].d*1.e6
#ymin0=ds.domain_left_edge[1].d*1.e6
#xmax0=ds.domain_right_edge[0].d*1.e6
#ymax0=ds.domain_right_edge[1].d*1.e6
#nx = ds.domain_dimensions[0]
#ny = ds.domain_dimensions[1]
#if dim == 2: zmin0=0.; zmax0=0.; nz = 1
#if dim == 3:
#    zmin0=ds.domain_left_edge[2].d*1.e6
#    zmax0=ds.domain_right_edge[2].d*1.e6
#    nz = ds.domain_dimensions[2]
#            
#xaxis=1.*np.arange(nx)*(xmax0-xmin0)/nx+xmin0
#yaxis=1.*np.arange(ny)*(ymax0-ymin0)/ny+ymin0
#zaxis=1.*np.arange(nz)*(zmax0-zmin0)/nz+zmin0             
    
#s=ds.particle_type_counts
#totpart = s[species]
#npart = int(totpart/size)
#i1 = rank*npart
#i2 = (rank+1)*npart

#print('i1:i2, rank=',i1,i2,rank)

start_time2= tm.time()

azimuth=20
elevation=40
                
reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
reg.max_level = 0
sc = yt.create_scene(reg, 'Ez')                
sc.camera.focus = ds.domain_center
sc.camera.resolution = 256
sc.camera.north_vector = [0, 0, 1]

sc.camera.zoom(1.1)
sc.camera.rotate(azimuth*np.pi/180)
sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
sc.annotate_axes(alpha=1)
                
source = sc[0]
source.tfh.set_bounds((1, 1e10))
source.tfh.grey_opacity = False
#sc.save('yt-rendering.png')
#sc.save('yt-rendering'+str(rank)+'.png')
#all_data_level = ds.covering_grid(level=0,
#        left_edge=ds.domain_left_edge, dims=ds.domain_dimensions)  

#fdata=all_data_level['Ez']

#ad = ds.all_data()
#tempx = ad[(species,'particle_position_x')].d*1.e6
#tempy = ad[(species,'particle_position_y')].d*1.e6

#npart = len(tempx)
#tempuz = ad[(species,'particle_momentum_z')].d/C
#tempux = ad[(species,'particle_momentum_y')].d/C
#tempuy = ad[(species,'particle_momentum_z')].d/C
#tempwt = ad[(species,'particle_weight')][i1:i2]
#tempwt = tempwt/np.max(tempwt)

#if dim == 3:
#    tempz = ad[(species,'particle_position_z')][i1:i2].d*1.e6
#else:
#    tempz = [0]

print("-- loading %f3.3 seconds from rank %d ---" %(tm.time() - start_time2, rank))
    
    
    
    
    
    
    
    
