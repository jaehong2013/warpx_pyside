import yt
import time as tm
import glob
import numpy as np
from mpi4py import MPI
from scipy import ndimage
import matplotlib.pyplot as plt

yt.enable_parallelism()

# get MPI size and rank
size = MPI.COMM_WORLD.Get_size()
rank = MPI.COMM_WORLD.Get_rank()

filename = 'plt00010'

start_time = tm.time()
# open file
ds = yt.load(filename)

azimuth=20
elevation=40
reg = ds.region(ds.domain_center, ds.domain_left_edge, ds.domain_right_edge)
reg.max_level = 0
sc = yt.create_scene(reg, 'Ez')                
sc.camera.focus = ds.domain_center
sc.camera.resolution = 256
sc.camera.north_vector = [0, 0, 1]

sc.camera.zoom(1.1)
sc.camera.rotate(azimuth*np.pi/180)
sc.camera.yaw(np.sign(azimuth)*(90.-elevation)*np.pi/180, rot_center=sc.camera.focus)            
sc.annotate_axes(alpha=1)
                
source = sc[0]
source.tfh.set_bounds((1, 1e10))
source.tfh.grey_opacity = False
print("-- %f3.3 seconds from rank %d ---" %(tm.time() - start_time, rank))


# sc.save('yt-rendering.png')
sc.render()
yt_volumedata = ndimage.rotate(sc._last_render,270)
print("-- total %f3.3 seconds from rank %d ---" %(tm.time() - start_time, rank))

if rank ==0:
    plt.imshow(yt_volumedata)
    plt.show()
    
    
    
    
    
    
    
    
